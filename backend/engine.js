var gameLogic = require('./gameLogic/gameLogic.js');

module.exports = {

	initUserData: function(mode, modeData){
		return modeData;
	},

	initGame: function (room) {
		return {
			inviteData: room.inviteData
		}
	},

	doTurn: function(room, user, turn, type){
		if (type == 'timeout'){
			return false;
		}

		console.log(turn);

		var isCorrectTurn = gameLogic.isCorrectTurn(room.game.history, turn);
		console.log('isCorrectTurn:' + isCorrectTurn);

		if (!isCorrectTurn) return false;
		room.game.isCorrectTurn = isCorrectTurn;
		turn.isCorrectTurn = isCorrectTurn;
		return turn;
        },



	switchPlayer: function(room, user, turn, type) {
            if (type != 'turn') {return false;}
	    if (!room.game.isCorrectTurn) {return user;}
            if (room.players[0] == user) return room.players[1];
            else return room.players[0];
        },

	getGameResult: function(room, user, turn, type){

		if (type == 'timeout'){

                        if (room.data[user.userId].timeouts == room.maxTimeouts) {
				return {
					winner: room.getOpponent(user),
					action: 'timeout'
				};
			} else return false;
                  } else {
			if (type == 'event'){
				return false;
			}
		}

		var gameOver = gameLogic.isGameOver(room.game.history, turn);
		if (gameOver === false) return false;

        	switch (gameOver.gameResult){
            		case 0: // win first player, white
				return {
					winner: user,
					gameComment: gameOver.gameComment
				};
			break;
			case 1: // win second player, black
				for (var i = 0; i < room.players.length; i++) {
					if (room.players[i] != room.game.first) {
						return {
							winner: room.players[i],
							gameComment: gameOver.gameComment
						};
					}
				}
			break;
			case 2: // draw
				return {
					winner: null,
					gameComment: gameOver.gameComment
				};
	                break;
			default: return false;
		}
		throw new Error('can not compute winner! room:' + room.id + ' result: ' + turn.result);
	},

	checkSign: function(user){
		return (user.sign === user.userId + user.userName);
	}
};