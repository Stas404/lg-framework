var ChessModule = require('./chessjs/chess.js');

module.exports = {


	isTimeoutDrawPosition: function (history) {
//		console.log('isNotDrawPosition');
		var chess = new ChessModule.Chess;

		for (var i=0; i<history.length; i++) {
			chess.move(history[i].newMove);
		}

			var history = chess.history({verbose: true});

			var rslt = {
				w: [],
				b: [],
			};
			for (var i=0; i<history.length; i++) {
				if (history[i].captured) {
					if(history[i].color === 'w') rslt['b'].push(history[i].captured.toUpperCase())
					else rslt['w'].push(history[i].captured.toUpperCase())
				}
			}
			captured = rslt;

//		console.log('captured');
//		console.log(captured);

		var opp = (chess.turn() == 'w') ? 'b' : 'w';
//		console.log('opp:' + opp);

//		console.log('captured[chess.turn()]');
//		console.log(captured[chess.turn()]);

//		console.log('captured[opp]');
//		console.log(captured[opp]);

		var isOneKing		= (captured[chess.turn()].length == 15);
		var isOneKingOpp	= (captured[opp].length == 15);
//		console.log('isOneKing: ' + isOneKing);
//		console.log('isOneKingOpp: ' + isOneKingOpp);

		// it's draw
		if (isOneKingOpp) return true;
		return false;
	},

	isGameOver: function (history, turn) {
		var chess = new ChessModule.Chess;
		for (var i=0; i<history.length; i++) {
			chess.move(history[i].newMove);
		}
//		console.log(chess.ascii());

		if (!chess.game_over()) {
			chess = null;
			return false;
		}

		if (chess.in_checkmate()) {
//			console.log('chess.in_checkmate()');
			var winner = (chess.turn() === 'b') ? 0 : 1;
			return {
				gameResult: winner,
				gameComment: ((winner === 0) ? 'Выиграли белые: мат.' : 'Выиграли черные: мат.')
			}
		}

		if ((chess.in_stalemate()) || (chess.in_draw()) || (chess.in_threefold_repetition())) {
//			console.log('draw @123');
			var gameComment = '';
			if (chess.in_stalemate()) gameComment = 'Пат.';
			if (chess.in_threefold_repetition()) gameComment = 'Трехкратное повторение позиции.';
			if (chess.insufficient_material()) gameComment = 'Недостаточно фигур для мата.';
			return {
				gameResult: 2,
				gameComment: gameComment
			}
		}
	},

	isCorrectMove: function (history, turn) {
/*
		console.log('-=-=-=-=-=-=-===-=-=-=-=-=-');
		console.log(history);
		console.log(turn);
		console.log('-=-=-=-=-=-=-===-=-=-=-=-=-');
*/
		var chess = new ChessModule.Chess;

		for (var i=0; i<history.length; i++) {
			chess.move(history[i].newMove);
		}

//		console.log(chess.ascii());
//		turn.newMove.promotion = "q";
		var move = chess.move(turn.newMove);
//		console.log(chess.ascii());

		if (move === null) {
//			console.info('ILLEGAL MOVE !!!');
		  	return false;
		}


//		console.log('@@isCorrectMove');
//		console.log(turn);
		chess = null;
		return true;
	}
}