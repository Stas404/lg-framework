var UCIchessEngine = require('uci');
var chessEngine = new UCIchessEngine('stockfish-6-64.exe');
chessEngine.runProcess().then(function () {
//    console.log('Started');
    return chessEngine.uciCommand();
}).then(function (idAndOptions) {
//    console.log('chessEngine name - ' + idAndOptions.id.name);
    return chessEngine.isReadyCommand();
}).then(function () {
//    console.log('Ready');
    return chessEngine.uciNewGameCommand();
}).then(function () {
//    console.log('New game started');
    return chessEngine.positionCommand('startpos', 'e2e4 e7e5');
}).then(function () {
//    console.log('Starting position set');
//    console.log('Starting analysis');
    return chessEngine.goInfiniteCommand(function infoHandler(info) {
//        console.log(info);
    });
}).delay(1000).then(function () {
//    console.log('Stopping analysis');
    return chessEngine.stopCommand();
}).then(function (bestmove) {
//    console.log('Bestmove: ');
//    console.log(bestmove);
    return chessEngine.quitCommand();
}).then(function () {
//    console.log('Stopped');
}).fail(function (error) {
//    console.log(error);
    process.exit();
}).done();