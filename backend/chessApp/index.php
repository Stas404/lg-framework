<?php
require_once("../sharedAPI/LogicGameSessionManager.php");
require_once("../sharedAPI/LogicGameLocalization.php");
require_once("../sharedAPI/LogicGameVkAuth.php");
require_once("../sharedAPI/LogicGameResourceManager.php");

$v .= '10';
$sm = new LogicGameSessionManager(CHESS_ID);
$s = $sm->getAuthServerInstance();
$s->updateActivity();

// Vk auth
$isVk = false;
$vkAuth = new LogicGameVkAuth($s, $sm);
$vkAuth->tryVkAuth();
$isVk = $vkAuth->hasVkAuth();

$isFreshUser = $sm->isFreshUser();
$i18n = $s->getI18n();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>logic-games.spb.ru</title>
	<link rel="icon" type="image/x-icon" href="index/img/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript">
        var _sessionId = "<?php echo $s->getSessionId(); ?>";
        var _userId = <?php echo $s->getUserId(); ?>;
        var _username = "<?php echo $s->getUsername(); ?>";
        var _sign = "<?php echo $s->getSign('test_salt');  ?>";
        var _isGuest = <?php if ($s->isGuest()) echo "true"; else echo "false"; ?>;
        var _gameVariationId = <?php echo $s->getGameVariationId(); ?>;
        var _isFreshUser = <?php echo $isFreshUser ? 'true' : 'false'; ?>;
        var _isVk = <?php echo $isVk ? 'true' : 'false'; ?>;
    </script>


<?php
	if(!$isVk) {
		echo '<script src="//vk.com/js/api/openapi.js" type="text/javascript"/></script>';
	} else {
		echo '<script src="//vk.com/js/api/xd_connection.js?2" type="text/javascript"></script>';
		echo '<link rel="stylesheet" href="vk/vk.css">';
	}
?>


    <?php
    echo "<script type='text/javascript' src='/js/build/public-main.min.js?v=$v'></script> \n\r";
    echo "<script type='text/javascript' src='/js/build/shared-main.js?v=$v'></script> \n\r";
    echo "<script type='text/javascript' src='/js/lang/lang.".$s->getI18n()->get("locale", "id").".js?v=$v'></script> \n\r";
    echo "<link media='screen' href='/css/build/shared.css?v=$v' rel='stylesheet' type='text/css'>\n\r";
    ?>

	<script src="//logic-games.spb.ru/v6-game-client/app/lib/underscore-min.js"></script>
	<script src="//logic-games.spb.ru/v6-game-client/app/lib/backbone-min.js"></script>
	<script src="//logic-games.spb.ru/v6-game-client/build/v6-game-client.js"></script>
            <link rel="stylesheet" href="//logic-games.spb.ru/v6-game-client/build/v6-game-client.css"/>


<!-- // V6-Game-Client -->
	<link href="css/index.css" rel="stylesheet">
	<link id="gameCSS" href="" rel="stylesheet">
</head>
<body>
<div id="main-wrapper">
<div id="root">
<!--
	<section id="allGames">
		<ul class="allGames-list">
			<li class="allGames-chess">
				<h2><a onclick='app.eventManager.trigger("gameEnter", {name: "chess"});'>Шахматы</a></h2>
				<ul>
					<li><a onclick='app.eventManager.trigger("gameEnter", {name: "chess", variation: "chessClassic"});'>Классические шахматы</a></li>
					<li><a onclick='app.eventManager.trigger("gameEnter", {name: "chess", variation: "chess960"});'>Шахматы Фишера</a></li>
					<li><a onclick='app.eventManager.trigger("gameEnter", {name: "chess", variation: "chessMini"});'>Мини-шахматы</a></li>
				</ul>
			</li>
			<li class="allGames-checkers">
				<h2><a onclick='app.eventManager.trigger("gameEnter", {name: "checkers"});'>Шашки</a></h2>
				<ul>
					<li><a onclick='app.eventManager.trigger("gameEnter", {name: "checkers", variation: "checkersClassic"});'>Классические шашки</a></li>
					<li><a onclick='app.eventManager.trigger("gameEnter", {name: "checkers", variation: "checkersGiveaway"});'>Шашки-поддавки</a></li>
				</ul>
			</li>
		</ul>
	</section>
-->
<!--
	<section id="game">
		<aside class="gameRelations">
			<div id="userListDiv"></div>
			<div id="chatDiv"></div>
		</aside>

		<div id="gamePanel">
			<header id="gameMenu">gameMenu</header>
			<main id="gameWorkspace">gameWorkspace</main>
			<footer id="accountPanel">accountPanel</footer>
		</div>

		<aside id="gameInfo">gameInfo</aside>
	</section>
-->
	<section id="game">
		<div class="layout">
			<aside class="gameRelations">
				<div id="userListDiv"></div>
				<div id="chatDiv"></div>
			</aside>

			<div class="chessApp">
				<section id="gamePanel">

					<div id="field">

                                                                    <?php include '../snippets/lg-login-register.htm';  ?>





					<header id="gameMenu"></header>
					<main id="gameWorkspace"></main>
					<footer class="menu-user">
						<ul class="menu-inline">
							<li><a href="#" onclick="window._client.viewsManager.showSettings(); return false;"><span>Параметры</span></a></li>
							<li><a href="#" onclick="app.gameManager.game.showDescription(); return false;"><span>Описание</span></a></li>
							<li><a href="#" onclick="LogicGame.showGuestBook(); return false;" ><span>Вопросы<br>и отзывы</span></a></li>
							<li><a href="#" onclick="window._client.historyManager.getHistory(false, false, false); return false;"><span>История</span></a></li>
							<li><a href="#" onclick="window._client.ratingManager.getRatings(); return false;"><span>Рейтинг</span></a></li>
							<li>
								<em id="bbLoginRegister"  style="display: none;">Авторизация</em>
								<em id="bbProfile">
									<span id="bbProfileLabel">Личный кабинет</span>
									<span id="bbUnreadMsgCount"></span>
								</em>
							</li>
						</ul>
					<footer>
                                                        </div>
				</section>

				<aside id="gameInfoPanel">
					<div id="historyMoves"></div>
					<div id="chessBalance"></div>
				</aside>

				<section id="gameAccountPanel">
					<div id="ratingDiv"></div>
					<div id="historyDiv"></div>
<?php
        include '../snippets/lg-profile.htm';
        include '../snippets/lg-guestbook.htm';
        ?>
				</section>


			</div>

			<div class="siteInfo">
        <?php
        include '../snippets/lg-activity.htm';
        ?>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" id="gameJS" src=""></script>
<!-- 13.8.2015  -->
</div>

</body>
</html>