var Server = require('v6-game-server');

console.log('@v6-game-server');
console.log(Server);

var engine = require('./engine.js');
var conf = require('./conf.js');

server = new Server(conf, engine);
server.start();