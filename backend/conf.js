module.exports = {
    game: 'newgame',
    port: 8103,
    pingTimeout: 100000,
    pingInterval: 25000,
    logLevel: 1,
    turnTime: 300,
    maxTimeouts: 1,
    minTurns: 4,
    takeBacks: 0,
    mode: 'develop',
    gameModes: ['mode1', 'mode2'], 
    modesAlias:{'mode1':'Game (mode1)', 'mode2': 'Game (mode2)'},
    adminList: ['85505', '460981', '448039', '40', '144'],
    adminPass: '1',
	mongo:{
        host: '192.168.250.40', port: '27001'
    },
    httpsKey: '/etc/apache2/ssl/serv.key',
    httpsCert: '/etc/apache2/ssl/serv.crt',
    httpsCa: ["/etc/apache2/ssl/sub.class1.server.ca.pem", "/etc/apache2/ssl/ca.pem"],
    https: false,
};