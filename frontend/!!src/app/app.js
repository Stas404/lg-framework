﻿app.state.dispach({
	type:'USER_LOGIN',
	data: data
});

app.state.getState();

app.game.menuModes = new Menu({
    items: [{
        title: 'Классические',
        action: 'appActions.SET_MODE',
        data: 'mode1',
        disabled: false,
        active: true
    }, {
        title: 'Блиц',
        action: 'appActions.SET_MODE',
        data: 'mode2',
        disabled: false,
        active: false
    }, {
        title: 'Задачи',
        action: 'appActions.SET_MODE',
        data: 'mode3',
        disabled: true,
        active: false
    }, {
        title: 'Этюды',
        action: 'appActions.SET_MODE',
        data: 'mode4',
        disabled: true,
        active: false
    }, {
        title: 'Другие игры',
        action: 'appActions.OTHER_GAMES',
        disabled: false,
        active: false,
        class: "other-games-item"
    }]
});

app.game.menuGameActions = new Menu({
    items: [{
        title: 'Отменить ход',
        action: 'gameActions.GAME_TAKEBACK',
        disabled: false,
        active: false
    }, {
        title: 'Предложить ничью',
        action: 'gameActions.GAME_OFFER_DRAW',
        disabled: true,
        active: false
    }, {
        title: 'Сдаться',
        action: 'gameActions.GAME_ADMIT_DEFEAT',
        disabled: true,
        active: false
    }, {
        title: 'Новая игра',
        action: 'gameActions.NEW_GAME',
        disabled: true,
        active: false
    }
]
});

app.game.history = new GameHistory();
app.game.balance = new GameBalance();
app.game.chessBoard = new ChessBoard();