// Debug panel.
import DebugPanel from './debug/debugPanel.js';
let debagPanel = new DebugPanel({
	rootElementId: 'game-field'
});

// Import Redux
import {createStore, combineReducers} from 'redux';

// Import Actions
import {appActions} from './game/actions/appActions.js';
import {userActions} from './game/actions/userActions.js';
import {gameActions} from './game/actions/gameActions.js';
import {optionsActions} from './game/actions/optionsActions.js';

// Import reducers
import {app} from './game/reducers/app.js';
import {user} from './game/reducers/user.js';
import {game} from './game/reducers/game.js';
import {options} from './game/reducers/options.js';

// Createing Store
window.app = {
	game: {
		store : {}
	}
};

window.app.game.store = createStore(combineReducers({
  				app,
  				user,
  				game,
  				options
			})			
		)





// State change (callback: log, update views, ...)
let unsubscribe = window.app.game.store.subscribe(() => {
	if (debagPanel)	document.getElementById('debugOutput').innerHTML = JSON.stringify(window.app.game.store.getState(), null, 4);
	console.log(window.app.game.store.getState());
});

// Demo usecase (array of actions)
// let demoUsecase = require('./demoUsecase.js');
// demoUsecase(1000);

require('./index.css');

// Stop listening changes of State
// unsubscribe();



require('./layout.scss');
_isGuest = false;

window.LogicGame.init(function() {	
	console.log('LG init');
	require('./client/init.js');

//	app.game.store.dispatch({
//		type: appActions.APP_INIT,
//		data: _client
//	});

});