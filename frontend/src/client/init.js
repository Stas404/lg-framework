// Import Actions
import {appActions} from './../game/actions/appActions.js';
import {userActions} from './../game/actions/userActions.js';
import {gameActions} from './../game/actions/gameActions.js';
import {optionsActions} from './../game/actions/optionsActions.js';

console.log('app start');
console.log(Client);
//var settingsTemplate = '<div><p>Цвет</p> <div> <label><input type="radio" name="color" value="red" >красный</label> <label><input type="radio" name="color" value="black" >черный</label> </div> </div> <p>Настройки игры</p> <div> <div class="option"> <label><input type="checkbox" name="sounds"> Включить звук</label> </div> <div class="option"> <label><input type="checkbox" name="disableInvite"> Запретить приглашать меня в игру</label> </div></div>';
var settingsTemplate = '<div>	<p>Цвет фигур</p>	<div>		<div class="option">			<label><input type="radio" name="singlePlayerColor" value="white" > Белые</label>		</div>		<div class="option">			<label><input type="radio" name="singlePlayerColor" value="black" > Черные</label>		</div>	</div>		<p>Уровень сложности</p>	<div>		<div class="option">			<label><input type="radio" name="singlePlayerEngineSkill" value="0"> Простой</label>		</div>		<div class="option">			<label><input type="radio" name="singlePlayerEngineSkill" value="1"> Средний</label>		</div>		<div class="option">			<label><input type="radio" name="singlePlayerEngineSkill" value="2"> Сложный</label>		</div>			</div>	 <p>Настройки игры</p>	<div>		<div class="option">			<label><input type="checkbox" name="sounds"> Включить звук</label>		</div>		<div class="option">			<label><input type="checkbox" name="disableInvite"> Запретить приглашать меня в игру</label>		</div>	</div></div>';
// var settingsTemplate = '@@@@';

/* ---- TODO: remove for production  */

    if (navigator.userAgent.indexOf('Firefox/') > 0) {
        window._userId = 'Firefox';
        window._username = 'Firefox';
    }
    if (navigator.userAgent.indexOf('Chrome/') > 0) {
        window._userId = 'Chrome';
        window._username = 'Chrome';
    }
    if (navigator.userAgent.indexOf('OPR/') > 0) {
        window._userId = 'Opera';
        window._username = 'Opera';
    }

/* ---- */
var user = {
	userId: window._userId,
	userName: window._username,
	sign: window._userId + window._username
};

window._client = new Client({
	game: 'v6ChessGame',
	port: 8103,
	resultDialogDelay: 1000,
	reload: true,
//	https: true,
//	domain: 'logic-games.spb.ru',
//	domain: 'v6chess.local',
	autoShowProfile: true,
	newGameFormat: true,
	generateInviteText: function (invite) {
//		console.log('@@@@ invite');
//		console.log(invite);
		var  inviteStringClassic = 'Вас пригласил пользователь ' + invite.from.userName + ' (' + invite.from.getRank(invite.data.mode) + ' место в рейтинге)'
			+ ' в  <strong>классические шахматы</strong>.';
		var  inviteStringBlitz = 'Вас пригласил пользователь ' + invite.from.userName + ' (' + invite.from.getRank(invite.data.mode) + ' место в рейтинге)'
			+ ' в  <strong>блиц</strong>.';

		if (invite.data.mode == 'classic') {
			return inviteStringClassic;
		} else {
			return inviteStringBlitz;
		}
/*
		return 'Вас пригласил пользователь ' + invite.from.userName + '(' + invite.from.getRank(invite.data.mode) + ' место в рейтинге)'
			+ ' в  '+_client.getModeAlias(invite.data.mode);
*/
	},
	blocks: {
		userListId: 'userListDiv',
		chatId: 'chatDiv',
		ratingId: 'ratingDiv',
		historyId: 'ratingDiv',
		profileId: 'ratingDiv'
	},
	settings: {
		singlePlayerColor: 'white',
		singlePlayerEngineSkill: "0",
		chessClockView: "0",
	},
	settingsTemplate: settingsTemplate
}).init(user);

var _client = window._client;

_client.on('login', function (data) {
	app.game.store.dispatch({
		type: appActions.APP_INIT,
		data: {
			modes: _client.modes,
			currentMode: _client.currentMode
		}
	});

	const  currentUser = {
		id: data.userId,
		name: data.userName,
		isGuest: null,
		status: null,
		tags: [],
		img: null,
	};
	for (let gameMode in  _client.modes) {
		currentUser[_client.modes[gameMode]] = data[_client.modes[gameMode]]
	}

	app.game.store.dispatch({
		type: userActions.USER_LOGIN,
		data: currentUser
	});

});

_client.on('mode_switch', function (data) {
	app.game.store.dispatch({
		type: appActions.MODE_SWITCH,
		data: data
	});
});

_client.gameManager.on('game_start', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_START,
		data: data
	});
});

_client.gameManager.on('round_start', function (data) {
	app.game.store.dispatch({
		type: gameActions.ROUND_START,
		data: data
	});
});

_client.gameManager.on('game_load', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_LOAD,
		data: data
	});
});

_client.gameManager.on('turn', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_TURN,
		data: {
			from: data.turn.newMove.from,
			to: data.turn.newMove.to,
			correct:  data.turn.isCorect
		}
	});
});

_client.gameManager.on('error', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_ERROR,
		data: data
	});
});

_client.gameManager.on('switch_player', function (data) {
	app.game.store.dispatch({
		type: gameActions.SWITCH_PLAYER,
		data: data
	});
});

_client.gameManager.on('event', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_EVENT,
		data: data
	});
});
_client.gameManager.on('timeout', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_TIMEOUT,
		data: data
	});
});
_client.gameManager.on('round_end', function (data) {
	app.game.store.dispatch({
		type: gameActions.ROUND_END,
		data: data
	});
});

_client.gameManager.on('game_leave', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_LEAVE,
		data: data
	});
});

_client.gameManager.on('take_back', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_TAKEBACK,
		data: data
	});
});

_client.gameManager.on('time', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_TIMETICK,
		data: data
	});
});

_client.gameManager.on('focus', function (data) {
//	app.game.store.dispatch({
//		type: userActions.USER_FOCUS,
//		data: data
//	});
});

_client.on('show_profile', function (data) {
//	console.log('main;', 'show_profile user:', data);
});

_client.on('settings_changed', function (data) {
	app.game.store.dispatch({
		type: optionsActions.OPTIONS_CHENGED,
		data: data
	});
});

_client.on('settings_saved', function (data) {
	app.game.store.dispatch({
		type: optionsActions.OPTIONS_SAVED,
		data: data
	});
});
// send events buttons example

_client.historyManager.on('game_load', function (data) {
	app.game.store.dispatch({
		type: gameActions.GAME_HISTORY_LOAD,
		data: data
	});
});


_generateEndGameBtn();
function _generateEndGameBtn() {
	var bdiv = $('<div>');
	bdiv.addClass('v6-buttons');
	$('body').append(bdiv);
	var div = $('<div>');
	div.attr('id', 'endGameButton');
	div.html('<span>Выйти из игры</span>');
	div.on('click', function () {
		window._client.gameManager.leaveGame();
	});
	bdiv.append(div);
	div = $('<div>');
	div.attr('id', 'drawButton');
	div.html('<span>Предложить ничью</span>');
	div.on('click', function () {
		window._client.gameManager.sendDraw();
	});
	bdiv.append(div);
	div = $('<div>');
	div.attr('id', 'winButton');
	div.html('<span>Победный ход</span>');
	div.on('click', function () {
		window._client.gameManager.sendTurn({result: 1});
	});
	bdiv.append(div);
	div = $('<div>');
	div.attr('id', 'ratingButton');
	div.html('<span>Показать рейтинг</span>');
	div.on('click', function () {
		window._client.ratingManager.getRatings();
	});
	bdiv.append(div);
	div = $('<div>');
	div.attr('id', 'historyButton');
	div.html('<span>Показать историю</span>');
	div.on('click', function () {
		window._client.historyManager.getHistory(false, false, false);
	});
	bdiv.append(div);
	div = $('<div>');
	div.html('<span>Передать ход</span>');
	div.on('click', function () {
		window._client.gameManager.sendTurn({'switch': true});
	});
	bdiv.append(div);
	div = $('<div>');
	div.html('<span>Сделать ход</span>');
	div.on('click', function () {
			var newMove = {
				'newMove': {
			    		from: 'e2',
			    		to: 'e4',
			    		promotion: 'q',
			    	},
			    	fen: 'FEN'
			}
		window._client.gameManager.sendTurn(newMove);
	});
	bdiv.append(div);
	div = $('<div>');
	div.html('<span>ход назад</span>');
	div.on('click', function () {
		window._client.gameManager.sendTakeBack();
	});
	bdiv.append(div);
}

function getCookie(c_name) {
	if (document.cookie.length > 0) {
		var c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			var c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return "";
}

//	if (window._username == 'Chrome')
//		require('./../bot/bot.js');