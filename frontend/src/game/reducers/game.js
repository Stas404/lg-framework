﻿// Import actions
import {gameActions} from './../actions/gameActions';

export function game (state = {turns: [], activeTurn:0}, action) {

	if (action.type != gameActions.GAME_TIMETICK)  console.debug(action);

	switch (action.type) {
		case gameActions.GAME_START:  return state
		case gameActions.ROUND_START:  return state
		case gameActions.GAME_LOAD:  return state
		case gameActions.GAME_TURN:
			return {
				turns: [...state.turns, action.data]
			}
		case gameActions.GAME_TIMETICK:  return state
		case gameActions.GAME_TIMEOUT:  return state
		case gameActions.SWITCH_PLAYER:  return state
		case gameActions.ROUND_END:  return state
		case gameActions.GAME_LEAVE:  return state
		case gameActions.GAME_EVENT:  return state
		case gameActions.GAME_ERROR:  return state
		default: return state;
	}
};