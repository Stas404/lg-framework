﻿// Import actions
import {appActions} from './../actions/appActions';
import AppClass from './../classes/app.class';

export function app (state = null , action) {
	switch (action.type) {

		case appActions.APP_INIT:
			return new AppClass (state, action).getState();

		case appActions.MODE_SWITCH:
			return Object.assign({}, state, {
				currentMode: action.data
			});
		case appActions.APP_STATUS: return state;
		case appActions.APP_DEBUG: return state;
		case appActions.APP_LOG: return state;

		default: return state;
	}
};
