﻿// Import actions
import {optionsActions} from './../actions/optionsActions';

export function options (state = null , action) {
	
//	console.debug(action);

	switch (action.type) {

		case optionsActions.OPTIONS_SAVED: return state;
		case optionsActions.OPTIONS_CHENGED: return state;
		case optionsActions.OPTIONS_RESET: return state

		default: return state;
	}
};             
