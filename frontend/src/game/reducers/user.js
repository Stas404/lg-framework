﻿// Import actions
import {userActions} from './../actions/userActions';
//import {userClass} from './../classes/user.class';

export function user (state = null, action) {
	switch (action.type) {
		case userActions.USER_LOGIN: return Object.assign({}, state, action.data);
		case userActions.USER_LOGOUT: return state;
		case userActions.USER_ONLINE: return state;
		case userActions.USER_OFFLINE: return state;
		case userActions.USER_UPDATE: return state
		default: return state;
	}
};
