﻿export const gameActions = {
	GAME_START: 'GAME_START',
	ROUND_START: 'ROUND_START',
	GAME_LOAD: 'GAME_LOAD',
	GAME_HISTORY_LOAD: 'GAME_HISTORY_LOAD',
	GAME_TURN: 'GAME_TURN',
	GAME_TIMETICK: 'GAME_TIMETICK',
	GAME_TIMEOUT: 'GAME_TIMEOUT',
	SWITCH_PLAYER: 'SWITCH_PLAYER',
	ROUND_END: 'ROUND_END',
	GAME_LEAVE: 'GAME_LEAVE',
	GAME_EVENT: 'GAME_EVENT',
	GAME_TAKEBACK: 'GAME_TAKEBACK',
	GAME_ERROR: 'GAME_ERROR'
};