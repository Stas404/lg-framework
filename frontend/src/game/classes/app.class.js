﻿class AppClass {
	constructor (state, action) {
		this.state = {};
		this.state.modes = action.data.modes || [];
		this.state.currentMode = action.data.modes || null;
		this.state.id = null;
		this.state.status = null;
		this.state.debug = true;
		this.state.log = true;
	}
	message () {
		alert('@@');
	}
	getState () {
		return this.state;
	}
}

export default AppClass