﻿class GameTurn {
	constructor (args) {
		this.id = args.id;
		this.time = args.time;
		this.owner = args.owner;
		this.data = args.data;
	}
}