﻿globalState = {
	app:{
		task: 'play' // ['play', 'view-online', 'view-history', 'analize', 'splashscreen']
		currentGameMode = 0,
		gameModes: [
			{
				name: 'mode1',
				title: 'aliase1',
				settings: {
					timing: '05:00',
					timeResetAfterTurn: false
				}
			}, 
			{
				name: 'mode2',
				title: 'aliase2',
				settings: {
					timing: '01:00',
					timeResetAfterTurn: true
				}
			}
		],
		user: {
			authorized: true,

			id: 435633662,
			name: 'Name of player1',
			bio: 'About player 1';
			socialurl = 'http://vk.com/player1id';
			stat: {
				mode1: {
					rating: 1751,
					ratingPosition: 211
				},
				mode2: {
					rating: 1893,
					ratingPosition: 17
				}								
			}
		}
	},

	game: {
		mode: 0,
		players: [{
			id: 435633662,
			name: 'Name of player1',
			bio: 'About player 1';
			socialurl = 'http://vk.com/player1id';
			stat: {
				mode1: {
					rating: 1751,
					ratingPosition: 211
				},
				mode2: {
					rating: 1893,
					ratingPosition: 17
				}								
			}
		}, {
			id: 7426097782,
			name: 'Name of player2',
			bio: 'About player 2';
			socialurl = 'http://vk.com/player2id';
			stat: {
				mode1: {
					rating: 1777,
					ratingPosition: 187
				},
				mode2: {
					rating: 1941,
					ratingPosition: 6
				}								
			}
		}],
		history: {
			activeTurn: 0,
			turns: [
				{
					id: 1,
					time: 1478466355509,
					owner: 0,
					data: {
						from: 'e2',
						to: 'e4',
						notation: 'e4',
						fen: 'fenstring'
						promotion: false,
						check: false,
						mate: false						
					}
				}, {
					id: 2,
					time: 1478466345643,
					owner: 1,
					data: {
						from: 'e7',
						to: 'e5',
						notation: 'e5',
						fen: 'fenstring'
						promotion: false,
						check: false,
						mate: false						
					}
				}, {
					id: 3,
					time: 1478467655445,
					owner: 0,
					data: {
						from: 'g1',
						to: 'f3',
						notation: 'Nf3',
						fen: 'fenstring'
						promotion: false,
						check: true,
						mate: false						
					}
				}
			]
		},
		gameData: [{
			player1: {
				time: '12:25',
				score: 1,
			},
			player2: {
				time: '13:47',
				score: 2
			}
		}],
		status: {
			whoMove: 1,
			gameOver: false,			
			winner: false,
			hint: 'Ход противника',
			resultMessage: 'Победил игрок 1',
			resultDescription: 'У игрок 2 закончилось время',
		}
	}
}