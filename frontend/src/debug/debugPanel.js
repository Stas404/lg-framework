require('./debugPanel.css');

export default class {
	constructor (options) {
//		alert('debug!!');
		this.DOM = document.createElement('div');
		this.DOM.id = "debugPanel";
		this.DOM.innerHTML =  `<form id="debugInput"><textarea name="debugInputTextarea" id="">
window._client.gameManager.sendTurn({
	'newMove': {
    		from: 'e2',
    		to: 'e4',
    		promotion: 'q',
    	}
});
</textarea><button id="debugInputButton">Apply</button></form><div id="debugOutput">DEBUG OUTPUT</div>`;
		document.getElementById(options.rootElementId).appendChild(this.DOM);

		document.getElementById('debugInput').addEventListener('submit', function (e) {
    			e.preventDefault();
    			eval(e.target.elements['debugInputTextarea'].value);
		});
	}
};