﻿import {gameActions} from './game/actions/gameActions';
import {userActions} from './game/actions/userActions';

module.exports = function demoUsecase (delay) {

	const demoActionsArray = [

		function () {
			app.game.store.dispatch({
				type: gameActions.GAME_TURN,
				turn: {
					from: 'e2',
					to:'e4',
					notation: 'e4',
					correct: true,
					fen: 'fen_' + Math.random(),
					time: new Date().toString(),
					extra: 'promotion'
				},
				activeTurn: 13
			});
		},

		function () {
			app.game.store.dispatch({
				type: gameActions.GAME_TURN,
				turn: {
					from: 'e7',
					to:'e5',
					notation: 'e5',
					correct: true,
					fen: 'fen_' + Math.random(),
					time: new Date().toString(),
					extra: 'check',
					hmr: 'hmr'
				},
				activeTurn: 14
			});
		},

		function () {
			app.game.store.dispatch({
				type: userActions.DRAW_OFFER,
				data: 'DRAW, OK?'
			});
		},
	
		function () {		
			app.game.store.dispatch({
				type: gameActions.GAME_TURN,
				turn: {
					from: 'g1',
					to:'f3',
					notation: 'Nf3',
					correct: true,
					fen: 'fen_' + Math.random(),
					time: new Date().toString(),
					extra: ''
				},
				activeTurn: 15
			});
		},
	
		function () {
			app.game.store.dispatch({
				type: userActions.DRAW_ACCEPT,
				data: 'DRAW, OK!'
			});
		}
	];
	

	let demoActionsArrayCounter = 0;
	
	setInterval (function () {

		if (typeof demoActionsArray[demoActionsArrayCounter] === 'function') demoActionsArray[demoActionsArrayCounter]();
		demoActionsArrayCounter++;

		app.game.store.dispatch({
			type: gameActions.GAME_TIMETICK,
			data: Math.floor(Math.random() * 10)
		});
	}, delay);
}