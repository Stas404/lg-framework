/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _debugPanel = __webpack_require__(1);
	
	var _debugPanel2 = _interopRequireDefault(_debugPanel);
	
	var _redux = __webpack_require__(6);
	
	var _appActions = __webpack_require__(16);
	
	var _userActions = __webpack_require__(17);
	
	var _gameActions = __webpack_require__(18);
	
	var _optionsActions = __webpack_require__(19);
	
	var _app = __webpack_require__(20);
	
	var _user = __webpack_require__(22);
	
	var _game = __webpack_require__(23);
	
	var _options = __webpack_require__(24);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var debagPanel = new _debugPanel2.default({
		rootElementId: 'game-field'
	});
	
	// Import Redux
	// Debug panel.
	
	// Import Actions
	
	// Import reducers
	
	// Createing Store
	window.app = {
		game: {
			store: {}
		}
	};
	
	window.app.game.store = (0, _redux.createStore)((0, _redux.combineReducers)({
		app: _app.app,
		user: _user.user,
		game: _game.game,
		options: _options.options
	}));
	
	// State change (callback: log, update views, ...)
	var unsubscribe = window.app.game.store.subscribe(function () {
		if (debagPanel) document.getElementById('debugOutput').innerHTML = JSON.stringify(window.app.game.store.getState(), null, 4);
		console.log(window.app.game.store.getState());
	});
	
	// Demo usecase (array of actions)
	// let demoUsecase = require('./demoUsecase.js');
	// demoUsecase(1000);
	
	__webpack_require__(25);
	
	// Stop listening changes of State
	// unsubscribe();
	
	__webpack_require__(27);
	_isGuest = false;
	
	window.LogicGame.init(function () {
		console.log('LG init');
		__webpack_require__(28);
	
		//	app.game.store.dispatch({
		//		type: appActions.APP_INIT,
		//		data: _client
		//	});
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
			value: true
	});
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	__webpack_require__(2);
	
	var _class = function _class(options) {
			_classCallCheck(this, _class);
	
			//		alert('debug!!');
			this.DOM = document.createElement('div');
			this.DOM.id = "debugPanel";
			this.DOM.innerHTML = '<form id="debugInput"><textarea name="debugInputTextarea" id="">\nwindow._client.gameManager.sendTurn({\n\t\'newMove\': {\n    \t\tfrom: \'e2\',\n    \t\tto: \'e4\',\n    \t\tpromotion: \'q\',\n    \t}\n});\n</textarea><button id="debugInputButton">Apply</button></form><div id="debugOutput">DEBUG OUTPUT</div>';
			document.getElementById(options.rootElementId).appendChild(this.DOM);
	
			document.getElementById('debugInput').addEventListener('submit', function (e) {
					e.preventDefault();
					eval(e.target.elements['debugInputTextarea'].value);
			});
	};
	
	exports.default = _class;
	;

/***/ },
/* 2 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { 'default': obj };
	}
	
	var _createStore = __webpack_require__(7);
	
	var _createStore2 = _interopRequireDefault(_createStore);
	
	var _utilsCombineReducers = __webpack_require__(9);
	
	var _utilsCombineReducers2 = _interopRequireDefault(_utilsCombineReducers);
	
	var _utilsBindActionCreators = __webpack_require__(13);
	
	var _utilsBindActionCreators2 = _interopRequireDefault(_utilsBindActionCreators);
	
	var _utilsApplyMiddleware = __webpack_require__(14);
	
	var _utilsApplyMiddleware2 = _interopRequireDefault(_utilsApplyMiddleware);
	
	var _utilsCompose = __webpack_require__(15);
	
	var _utilsCompose2 = _interopRequireDefault(_utilsCompose);
	
	exports.createStore = _createStore2['default'];
	exports.combineReducers = _utilsCombineReducers2['default'];
	exports.bindActionCreators = _utilsBindActionCreators2['default'];
	exports.applyMiddleware = _utilsApplyMiddleware2['default'];
	exports.compose = _utilsCompose2['default'];

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports['default'] = createStore;
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { 'default': obj };
	}
	
	var _utilsIsPlainObject = __webpack_require__(8);
	
	var _utilsIsPlainObject2 = _interopRequireDefault(_utilsIsPlainObject);
	
	/**
	 * These are private action types reserved by Redux.
	 * For any unknown actions, you must return the current state.
	 * If the current state is undefined, you must return the initial state.
	 * Do not reference these action types directly in your code.
	 */
	var ActionTypes = {
	  INIT: '@@redux/INIT'
	};
	
	exports.ActionTypes = ActionTypes;
	/**
	 * Creates a Redux store that holds the state tree.
	 * The only way to change the data in the store is to call `dispatch()` on it.
	 *
	 * There should only be a single store in your app. To specify how different
	 * parts of the state tree respond to actions, you may combine several reducers
	 * into a single reducer function by using `combineReducers`.
	 *
	 * @param {Function} reducer A function that returns the next state tree, given
	 * the current state tree and the action to handle.
	 *
	 * @param {any} [initialState] The initial state. You may optionally specify it
	 * to hydrate the state from the server in universal apps, or to restore a
	 * previously serialized user session.
	 * If you use `combineReducers` to produce the root reducer function, this must be
	 * an object with the same shape as `combineReducers` keys.
	 *
	 * @returns {Store} A Redux store that lets you read the state, dispatch actions
	 * and subscribe to changes.
	 */
	
	function createStore(reducer, initialState) {
	  if (typeof reducer !== 'function') {
	    throw new Error('Expected the reducer to be a function.');
	  }
	
	  var currentReducer = reducer;
	  var currentState = initialState;
	  var listeners = [];
	  var isDispatching = false;
	
	  /**
	   * Reads the state tree managed by the store.
	   *
	   * @returns {any} The current state tree of your application.
	   */
	  function getState() {
	    return currentState;
	  }
	
	  /**
	   * Adds a change listener. It will be called any time an action is dispatched,
	   * and some part of the state tree may potentially have changed. You may then
	   * call `getState()` to read the current state tree inside the callback.
	   *
	   * @param {Function} listener A callback to be invoked on every dispatch.
	   * @returns {Function} A function to remove this change listener.
	   */
	  function subscribe(listener) {
	    listeners.push(listener);
	    var isSubscribed = true;
	
	    return function unsubscribe() {
	      if (!isSubscribed) {
	        return;
	      }
	
	      isSubscribed = false;
	      var index = listeners.indexOf(listener);
	      listeners.splice(index, 1);
	    };
	  }
	
	  /**
	   * Dispatches an action. It is the only way to trigger a state change.
	   *
	   * The `reducer` function, used to create the store, will be called with the
	   * current state tree and the given `action`. Its return value will
	   * be considered the **next** state of the tree, and the change listeners
	   * will be notified.
	   *
	   * The base implementation only supports plain object actions. If you want to
	   * dispatch a Promise, an Observable, a thunk, or something else, you need to
	   * wrap your store creating function into the corresponding middleware. For
	   * example, see the documentation for the `redux-thunk` package. Even the
	   * middleware will eventually dispatch plain object actions using this method.
	   *
	   * @param {Object} action A plain object representing “what changed”. It is
	   * a good idea to keep actions serializable so you can record and replay user
	   * sessions, or use the time travelling `redux-devtools`. An action must have
	   * a `type` property which may not be `undefined`. It is a good idea to use
	   * string constants for action types.
	   *
	   * @returns {Object} For convenience, the same action object you dispatched.
	   *
	   * Note that, if you use a custom middleware, it may wrap `dispatch()` to
	   * return something else (for example, a Promise you can await).
	   */
	  function dispatch(action) {
	    if (!_utilsIsPlainObject2['default'](action)) {
	      throw new Error('Actions must be plain objects. ' + 'Use custom middleware for async actions.');
	    }
	
	    if (typeof action.type === 'undefined') {
	      throw new Error('Actions may not have an undefined "type" property. ' + 'Have you misspelled a constant?');
	    }
	
	    if (isDispatching) {
	      throw new Error('Reducers may not dispatch actions.');
	    }
	
	    try {
	      isDispatching = true;
	      currentState = currentReducer(currentState, action);
	    } finally {
	      isDispatching = false;
	    }
	
	    listeners.slice().forEach(function (listener) {
	      return listener();
	    });
	    return action;
	  }
	
	  /**
	   * Replaces the reducer currently used by the store to calculate the state.
	   *
	   * You might need this if your app implements code splitting and you want to
	   * load some of the reducers dynamically. You might also need this if you
	   * implement a hot reloading mechanism for Redux.
	   *
	   * @param {Function} nextReducer The reducer for the store to use instead.
	   * @returns {void}
	   */
	  function replaceReducer(nextReducer) {
	    currentReducer = nextReducer;
	    dispatch({ type: ActionTypes.INIT });
	  }
	
	  // When a store is created, an "INIT" action is dispatched so that every
	  // reducer returns their initial state. This effectively populates
	  // the initial state tree.
	  dispatch({ type: ActionTypes.INIT });
	
	  return {
	    dispatch: dispatch,
	    subscribe: subscribe,
	    getState: getState,
	    replaceReducer: replaceReducer
	  };
	}

/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';
	
	function _typeof(obj) { return obj && typeof Symbol !== "undefined" && obj.constructor === Symbol ? "symbol" : typeof obj; }
	
	exports.__esModule = true;
	exports['default'] = isPlainObject;
	var fnToString = function fnToString(fn) {
	  return Function.prototype.toString.call(fn);
	};
	var objStringValue = fnToString(Object);
	
	/**
	 * @param {any} obj The object to inspect.
	 * @returns {boolean} True if the argument appears to be a plain object.
	 */
	
	function isPlainObject(obj) {
	  if (!obj || (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object') {
	    return false;
	  }
	
	  var proto = typeof obj.constructor === 'function' ? Object.getPrototypeOf(obj) : Object.prototype;
	
	  if (proto === null) {
	    return true;
	  }
	
	  var constructor = proto.constructor;
	
	  return typeof constructor === 'function' && constructor instanceof constructor && fnToString(constructor) === objStringValue;
	}
	
	module.exports = exports['default'];

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';
	
	exports.__esModule = true;
	exports['default'] = combineReducers;
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { 'default': obj };
	}
	
	var _createStore = __webpack_require__(7);
	
	var _isPlainObject = __webpack_require__(8);
	
	var _isPlainObject2 = _interopRequireDefault(_isPlainObject);
	
	var _mapValues = __webpack_require__(11);
	
	var _mapValues2 = _interopRequireDefault(_mapValues);
	
	var _pick = __webpack_require__(12);
	
	var _pick2 = _interopRequireDefault(_pick);
	
	/* eslint-disable no-console */
	
	function getUndefinedStateErrorMessage(key, action) {
	  var actionType = action && action.type;
	  var actionName = actionType && '"' + actionType.toString() + '"' || 'an action';
	
	  return 'Reducer "' + key + '" returned undefined handling ' + actionName + '. ' + 'To ignore an action, you must explicitly return the previous state.';
	}
	
	function getUnexpectedStateKeyWarningMessage(inputState, outputState, action) {
	  var reducerKeys = Object.keys(outputState);
	  var argumentName = action && action.type === _createStore.ActionTypes.INIT ? 'initialState argument passed to createStore' : 'previous state received by the reducer';
	
	  if (reducerKeys.length === 0) {
	    return 'Store does not have a valid reducer. Make sure the argument passed ' + 'to combineReducers is an object whose values are reducers.';
	  }
	
	  if (!_isPlainObject2['default'](inputState)) {
	    return 'The ' + argumentName + ' has unexpected type of "' + ({}).toString.call(inputState).match(/\s([a-z|A-Z]+)/)[1] + '". Expected argument to be an object with the following ' + ('keys: "' + reducerKeys.join('", "') + '"');
	  }
	
	  var unexpectedKeys = Object.keys(inputState).filter(function (key) {
	    return reducerKeys.indexOf(key) < 0;
	  });
	
	  if (unexpectedKeys.length > 0) {
	    return 'Unexpected ' + (unexpectedKeys.length > 1 ? 'keys' : 'key') + ' ' + ('"' + unexpectedKeys.join('", "') + '" found in ' + argumentName + '. ') + 'Expected to find one of the known reducer keys instead: ' + ('"' + reducerKeys.join('", "') + '". Unexpected keys will be ignored.');
	  }
	}
	
	function assertReducerSanity(reducers) {
	  Object.keys(reducers).forEach(function (key) {
	    var reducer = reducers[key];
	    var initialState = reducer(undefined, { type: _createStore.ActionTypes.INIT });
	
	    if (typeof initialState === 'undefined') {
	      throw new Error('Reducer "' + key + '" returned undefined during initialization. ' + 'If the state passed to the reducer is undefined, you must ' + 'explicitly return the initial state. The initial state may ' + 'not be undefined.');
	    }
	
	    var type = '@@redux/PROBE_UNKNOWN_ACTION_' + Math.random().toString(36).substring(7).split('').join('.');
	    if (typeof reducer(undefined, { type: type }) === 'undefined') {
	      throw new Error('Reducer "' + key + '" returned undefined when probed with a random type. ' + ('Don\'t try to handle ' + _createStore.ActionTypes.INIT + ' or other actions in "redux/*" ') + 'namespace. They are considered private. Instead, you must return the ' + 'current state for any unknown actions, unless it is undefined, ' + 'in which case you must return the initial state, regardless of the ' + 'action type. The initial state may not be undefined.');
	    }
	  });
	}
	
	/**
	 * Turns an object whose values are different reducer functions, into a single
	 * reducer function. It will call every child reducer, and gather their results
	 * into a single state object, whose keys correspond to the keys of the passed
	 * reducer functions.
	 *
	 * @param {Object} reducers An object whose values correspond to different
	 * reducer functions that need to be combined into one. One handy way to obtain
	 * it is to use ES6 `import * as reducers` syntax. The reducers may never return
	 * undefined for any action. Instead, they should return their initial state
	 * if the state passed to them was undefined, and the current state for any
	 * unrecognized action.
	 *
	 * @returns {Function} A reducer function that invokes every reducer inside the
	 * passed object, and builds a state object with the same shape.
	 */
	
	function combineReducers(reducers) {
	  var finalReducers = _pick2['default'](reducers, function (val) {
	    return typeof val === 'function';
	  });
	  var sanityError;
	
	  try {
	    assertReducerSanity(finalReducers);
	  } catch (e) {
	    sanityError = e;
	  }
	
	  var defaultState = _mapValues2['default'](finalReducers, function () {
	    return undefined;
	  });
	
	  return function combination(state, action) {
	    if (state === undefined) state = defaultState;
	
	    if (sanityError) {
	      throw sanityError;
	    }
	
	    var hasChanged = false;
	    var finalState = _mapValues2['default'](finalReducers, function (reducer, key) {
	      var previousStateForKey = state[key];
	      var nextStateForKey = reducer(previousStateForKey, action);
	      if (typeof nextStateForKey === 'undefined') {
	        var errorMessage = getUndefinedStateErrorMessage(key, action);
	        throw new Error(errorMessage);
	      }
	      hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
	      return nextStateForKey;
	    });
	
	    if (process.env.NODE_ENV !== 'production') {
	      var warningMessage = getUnexpectedStateKeyWarningMessage(state, finalState, action);
	      if (warningMessage) {
	        console.error(warningMessage);
	      }
	    }
	
	    return hasChanged ? finalState : state;
	  };
	}
	
	module.exports = exports['default'];
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(10)))

/***/ },
/* 10 */
/***/ function(module, exports) {

	'use strict';
	
	// shim for using process in browser
	
	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while (len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () {
	    return '/';
	};
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function () {
	    return 0;
	};

/***/ },
/* 11 */
/***/ function(module, exports) {

	/**
	 * Applies a function to every key-value pair inside an object.
	 *
	 * @param {Object} obj The source object.
	 * @param {Function} fn The mapper function that receives the value and the key.
	 * @returns {Object} A new object that contains the mapped values for the keys.
	 */
	"use strict";
	
	exports.__esModule = true;
	exports["default"] = mapValues;
	
	function mapValues(obj, fn) {
	  return Object.keys(obj).reduce(function (result, key) {
	    result[key] = fn(obj[key], key);
	    return result;
	  }, {});
	}
	
	module.exports = exports["default"];

/***/ },
/* 12 */
/***/ function(module, exports) {

	/**
	 * Picks key-value pairs from an object where values satisfy a predicate.
	 *
	 * @param {Object} obj The object to pick from.
	 * @param {Function} fn The predicate the values must satisfy to be copied.
	 * @returns {Object} The object with the values that satisfied the predicate.
	 */
	"use strict";
	
	exports.__esModule = true;
	exports["default"] = pick;
	
	function pick(obj, fn) {
	  return Object.keys(obj).reduce(function (result, key) {
	    if (fn(obj[key])) {
	      result[key] = obj[key];
	    }
	    return result;
	  }, {});
	}
	
	module.exports = exports["default"];

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _typeof(obj) { return obj && typeof Symbol !== "undefined" && obj.constructor === Symbol ? "symbol" : typeof obj; }
	
	exports.__esModule = true;
	exports['default'] = bindActionCreators;
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { 'default': obj };
	}
	
	var _mapValues = __webpack_require__(11);
	
	var _mapValues2 = _interopRequireDefault(_mapValues);
	
	function bindActionCreator(actionCreator, dispatch) {
	  return function () {
	    return dispatch(actionCreator.apply(undefined, arguments));
	  };
	}
	
	/**
	 * Turns an object whose values are action creators, into an object with the
	 * same keys, but with every function wrapped into a `dispatch` call so they
	 * may be invoked directly. This is just a convenience method, as you can call
	 * `store.dispatch(MyActionCreators.doSomething())` yourself just fine.
	 *
	 * For convenience, you can also pass a single function as the first argument,
	 * and get a function in return.
	 *
	 * @param {Function|Object} actionCreators An object whose values are action
	 * creator functions. One handy way to obtain it is to use ES6 `import * as`
	 * syntax. You may also pass a single function.
	 *
	 * @param {Function} dispatch The `dispatch` function available on your Redux
	 * store.
	 *
	 * @returns {Function|Object} The object mimicking the original object, but with
	 * every action creator wrapped into the `dispatch` call. If you passed a
	 * function as `actionCreators`, the return value will also be a single
	 * function.
	 */
	
	function bindActionCreators(actionCreators, dispatch) {
	  if (typeof actionCreators === 'function') {
	    return bindActionCreator(actionCreators, dispatch);
	  }
	
	  if ((typeof actionCreators === 'undefined' ? 'undefined' : _typeof(actionCreators)) !== 'object' || actionCreators === null || actionCreators === undefined) {
	    throw new Error('bindActionCreators expected an object or a function, instead received ' + (actionCreators === null ? 'null' : typeof actionCreators === 'undefined' ? 'undefined' : _typeof(actionCreators)) + '. ' + 'Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?');
	  }
	
	  return _mapValues2['default'](actionCreators, function (actionCreator) {
	    return bindActionCreator(actionCreator, dispatch);
	  });
	}
	
	module.exports = exports['default'];

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	var _extends = Object.assign || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }return target;
	};
	
	exports['default'] = applyMiddleware;
	
	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { 'default': obj };
	}
	
	var _compose = __webpack_require__(15);
	
	var _compose2 = _interopRequireDefault(_compose);
	
	/**
	 * Creates a store enhancer that applies middleware to the dispatch method
	 * of the Redux store. This is handy for a variety of tasks, such as expressing
	 * asynchronous actions in a concise manner, or logging every action payload.
	 *
	 * See `redux-thunk` package as an example of the Redux middleware.
	 *
	 * Because middleware is potentially asynchronous, this should be the first
	 * store enhancer in the composition chain.
	 *
	 * Note that each middleware will be given the `dispatch` and `getState` functions
	 * as named arguments.
	 *
	 * @param {...Function} middlewares The middleware chain to be applied.
	 * @returns {Function} A store enhancer applying the middleware.
	 */
	
	function applyMiddleware() {
	  for (var _len = arguments.length, middlewares = Array(_len), _key = 0; _key < _len; _key++) {
	    middlewares[_key] = arguments[_key];
	  }
	
	  return function (next) {
	    return function (reducer, initialState) {
	      var store = next(reducer, initialState);
	      var _dispatch = store.dispatch;
	      var chain = [];
	
	      var middlewareAPI = {
	        getState: store.getState,
	        dispatch: function dispatch(action) {
	          return _dispatch(action);
	        }
	      };
	      chain = middlewares.map(function (middleware) {
	        return middleware(middlewareAPI);
	      });
	      _dispatch = _compose2['default'].apply(undefined, chain)(store.dispatch);
	
	      return _extends({}, store, {
	        dispatch: _dispatch
	      });
	    };
	  };
	}
	
	module.exports = exports['default'];

/***/ },
/* 15 */
/***/ function(module, exports) {

	/**
	 * Composes single-argument functions from right to left.
	 *
	 * @param {...Function} funcs The functions to compose.
	 * @returns {Function} A function obtained by composing functions from right to
	 * left. For example, compose(f, g, h) is identical to arg => f(g(h(arg))).
	 */
	"use strict";
	
	exports.__esModule = true;
	exports["default"] = compose;
	
	function compose() {
	  for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) {
	    funcs[_key] = arguments[_key];
	  }
	
	  return function (arg) {
	    return funcs.reduceRight(function (composed, f) {
	      return f(composed);
	    }, arg);
	  };
	}
	
	module.exports = exports["default"];

/***/ },
/* 16 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var appActions = exports.appActions = {
		APP_INIT: 'APP_INIT',
		APP_STATUS: 'APP_STATUS',
		APP_DEBUG: 'APP_DEBUG',
		APP_LOG: 'APP_LOG',
		APP_ERROR: 'APP_ERROR',
		MODE_SWITCH: 'MODE_SWITCH'
	};

/***/ },
/* 17 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var userActions = exports.userActions = {
		USER_LOGIN: 'USER_LOGIN',
		USER_LOGOUT: 'USER_LOGIN',
		USER_ONLINE: 'USER_OFFLINE',
		USER_OFFLINE: 'USER_OFFLINE',
		USER_UPDATE: 'USER_UPDATE',
		USER_TURN: 'USER_TURN',
		USER_HISTORY_NAVIGATE: 'USER_HISTORY_NAVIGATE',
		USER_ADMIT_DEFEAT: 'USER_ADMIT_DEFEAT',
		USER_DRAW_OFFER: 'USER_DRAW_OFFER',
		USER_DRAW_ACCEPT: 'USER_DRAW_ACCEPT',
		USER_TAKEBACK_OFFER: 'USER_TAKEBACK_OFFER',
		USER_TAKEBACK_ACCEPT: 'USER_TAKEBACK_ACCEPT',
		USER_FOCUS: 'USER_FOCUS'
	};

/***/ },
/* 18 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var gameActions = exports.gameActions = {
		GAME_START: 'GAME_START',
		ROUND_START: 'ROUND_START',
		GAME_LOAD: 'GAME_LOAD',
		GAME_HISTORY_LOAD: 'GAME_HISTORY_LOAD',
		GAME_TURN: 'GAME_TURN',
		GAME_TIMETICK: 'GAME_TIMETICK',
		GAME_TIMEOUT: 'GAME_TIMEOUT',
		SWITCH_PLAYER: 'SWITCH_PLAYER',
		ROUND_END: 'ROUND_END',
		GAME_LEAVE: 'GAME_LEAVE',
		GAME_EVENT: 'GAME_EVENT',
		GAME_TAKEBACK: 'GAME_TAKEBACK',
		GAME_ERROR: 'GAME_ERROR'
	};

/***/ },
/* 19 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var optionsActions = exports.optionsActions = {
		OPTIONS_CHANGED: 'OPTIONS_CHANGED',
		OPTIONS_SAVED: 'OPTIONS_SAVED',
		OPTIONS_RESET: 'OPTIONS_RESET'
	};

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.app = app;
	
	var _appActions = __webpack_require__(16);
	
	var _app = __webpack_require__(21);
	
	var _app2 = _interopRequireDefault(_app);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	// Import actions
	function app() {
		var state = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
		var action = arguments[1];
	
		switch (action.type) {
	
			case _appActions.appActions.APP_INIT:
				return new _app2.default(state, action).getState();
	
			case _appActions.appActions.MODE_SWITCH:
				return Object.assign({}, state, {
					currentMode: action.data
				});
			case _appActions.appActions.APP_STATUS:
				return state;
			case _appActions.appActions.APP_DEBUG:
				return state;
			case _appActions.appActions.APP_LOG:
				return state;
	
			default:
				return state;
		}
	};

/***/ },
/* 21 */
/***/ function(module, exports) {

	'use strict';
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var AppClass = (function () {
		function AppClass(state, action) {
			_classCallCheck(this, AppClass);
	
			this.state = {};
			this.state.modes = action.data.modes || [];
			this.state.currentMode = action.data.modes || null;
			this.state.id = null;
			this.state.status = null;
			this.state.debug = true;
			this.state.log = true;
		}
	
		_createClass(AppClass, [{
			key: 'message',
			value: function message() {
				alert('@@');
			}
		}, {
			key: 'getState',
			value: function getState() {
				return this.state;
			}
		}]);
	
		return AppClass;
	})();
	
	exports.default = AppClass;

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.user = user;
	
	var _userActions = __webpack_require__(17);
	
	//import {userClass} from './../classes/user.class';
	
	function user() {
		var state = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
		var action = arguments[1];
	
		switch (action.type) {
			case _userActions.userActions.USER_LOGIN:
				return Object.assign({}, state, action.data);
			case _userActions.userActions.USER_LOGOUT:
				return state;
			case _userActions.userActions.USER_ONLINE:
				return state;
			case _userActions.userActions.USER_OFFLINE:
				return state;
			case _userActions.userActions.USER_UPDATE:
				return state;
			default:
				return state;
		}
	} // Import actions
	;

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.game = game;
	
	var _gameActions = __webpack_require__(18);
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } } // Import actions
	
	function game() {
		var state = arguments.length <= 0 || arguments[0] === undefined ? { turns: [], activeTurn: 0 } : arguments[0];
		var action = arguments[1];
	
		if (action.type != _gameActions.gameActions.GAME_TIMETICK) console.debug(action);
	
		switch (action.type) {
			case _gameActions.gameActions.GAME_START:
				return state;
			case _gameActions.gameActions.ROUND_START:
				return state;
			case _gameActions.gameActions.GAME_LOAD:
				return state;
			case _gameActions.gameActions.GAME_TURN:
				return {
					turns: [].concat(_toConsumableArray(state.turns), [action.data])
				};
			case _gameActions.gameActions.GAME_TIMETICK:
				return state;
			case _gameActions.gameActions.GAME_TIMEOUT:
				return state;
			case _gameActions.gameActions.SWITCH_PLAYER:
				return state;
			case _gameActions.gameActions.ROUND_END:
				return state;
			case _gameActions.gameActions.GAME_LEAVE:
				return state;
			case _gameActions.gameActions.GAME_EVENT:
				return state;
			case _gameActions.gameActions.GAME_ERROR:
				return state;
			default:
				return state;
		}
	};

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.options = options;
	
	var _optionsActions = __webpack_require__(19);
	
	function options() {
		var state = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
		var action = arguments[1];
	
		//	console.debug(action);
	
		switch (action.type) {
	
			case _optionsActions.optionsActions.OPTIONS_SAVED:
				return state;
			case _optionsActions.optionsActions.OPTIONS_CHENGED:
				return state;
			case _optionsActions.optionsActions.OPTIONS_RESET:
				return state;
	
			default:
				return state;
		}
	} // Import actions
	;

/***/ },
/* 25 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 26 */,
/* 27 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _appActions = __webpack_require__(16);
	
	var _userActions = __webpack_require__(17);
	
	var _gameActions = __webpack_require__(18);
	
	var _optionsActions = __webpack_require__(19);
	
	// Import Actions
	
	console.log('app start');
	console.log(Client);
	//var settingsTemplate = '<div><p>Цвет</p> <div> <label><input type="radio" name="color" value="red" >красный</label> <label><input type="radio" name="color" value="black" >черный</label> </div> </div> <p>Настройки игры</p> <div> <div class="option"> <label><input type="checkbox" name="sounds"> Включить звук</label> </div> <div class="option"> <label><input type="checkbox" name="disableInvite"> Запретить приглашать меня в игру</label> </div></div>';
	var settingsTemplate = '<div>	<p>Цвет фигур</p>	<div>		<div class="option">			<label><input type="radio" name="singlePlayerColor" value="white" > Белые</label>		</div>		<div class="option">			<label><input type="radio" name="singlePlayerColor" value="black" > Черные</label>		</div>	</div>		<p>Уровень сложности</p>	<div>		<div class="option">			<label><input type="radio" name="singlePlayerEngineSkill" value="0"> Простой</label>		</div>		<div class="option">			<label><input type="radio" name="singlePlayerEngineSkill" value="1"> Средний</label>		</div>		<div class="option">			<label><input type="radio" name="singlePlayerEngineSkill" value="2"> Сложный</label>		</div>			</div>	 <p>Настройки игры</p>	<div>		<div class="option">			<label><input type="checkbox" name="sounds"> Включить звук</label>		</div>		<div class="option">			<label><input type="checkbox" name="disableInvite"> Запретить приглашать меня в игру</label>		</div>	</div></div>';
	// var settingsTemplate = '@@@@';
	
	/* ---- TODO: remove for production  */
	
	if (navigator.userAgent.indexOf('Firefox/') > 0) {
		window._userId = 'Firefox';
		window._username = 'Firefox';
	}
	if (navigator.userAgent.indexOf('Chrome/') > 0) {
		window._userId = 'Chrome';
		window._username = 'Chrome';
	}
	if (navigator.userAgent.indexOf('OPR/') > 0) {
		window._userId = 'Opera';
		window._username = 'Opera';
	}
	
	/* ---- */
	var user = {
		userId: window._userId,
		userName: window._username,
		sign: window._userId + window._username
	};
	
	window._client = new Client({
		game: 'v6ChessGame',
		port: 8103,
		resultDialogDelay: 1000,
		reload: true,
		//	https: true,
		//	domain: 'logic-games.spb.ru',
		//	domain: 'v6chess.local',
		autoShowProfile: true,
		newGameFormat: true,
		generateInviteText: function generateInviteText(invite) {
			//		console.log('@@@@ invite');
			//		console.log(invite);
			var inviteStringClassic = 'Вас пригласил пользователь ' + invite.from.userName + ' (' + invite.from.getRank(invite.data.mode) + ' место в рейтинге)' + ' в  <strong>классические шахматы</strong>.';
			var inviteStringBlitz = 'Вас пригласил пользователь ' + invite.from.userName + ' (' + invite.from.getRank(invite.data.mode) + ' место в рейтинге)' + ' в  <strong>блиц</strong>.';
	
			if (invite.data.mode == 'classic') {
				return inviteStringClassic;
			} else {
				return inviteStringBlitz;
			}
			/*
	  		return 'Вас пригласил пользователь ' + invite.from.userName + '(' + invite.from.getRank(invite.data.mode) + ' место в рейтинге)'
	  			+ ' в  '+_client.getModeAlias(invite.data.mode);
	  */
		},
		blocks: {
			userListId: 'userListDiv',
			chatId: 'chatDiv',
			ratingId: 'ratingDiv',
			historyId: 'ratingDiv',
			profileId: 'ratingDiv'
		},
		settings: {
			singlePlayerColor: 'white',
			singlePlayerEngineSkill: "0",
			chessClockView: "0"
		},
		settingsTemplate: settingsTemplate
	}).init(user);
	
	var _client = window._client;
	
	_client.on('login', function (data) {
		app.game.store.dispatch({
			type: _appActions.appActions.APP_INIT,
			data: {
				modes: _client.modes,
				currentMode: _client.currentMode
			}
		});
	
		var currentUser = {
			id: data.userId,
			name: data.userName,
			isGuest: null,
			status: null,
			tags: [],
			img: null
		};
		for (var gameMode in _client.modes) {
			currentUser[_client.modes[gameMode]] = data[_client.modes[gameMode]];
		}
	
		app.game.store.dispatch({
			type: _userActions.userActions.USER_LOGIN,
			data: currentUser
		});
	});
	
	_client.on('mode_switch', function (data) {
		app.game.store.dispatch({
			type: _appActions.appActions.MODE_SWITCH,
			data: data
		});
	});
	
	_client.gameManager.on('game_start', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_START,
			data: data
		});
	});
	
	_client.gameManager.on('round_start', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.ROUND_START,
			data: data
		});
	});
	
	_client.gameManager.on('game_load', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_LOAD,
			data: data
		});
	});
	
	_client.gameManager.on('turn', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_TURN,
			data: {
				from: data.turn.newMove.from,
				to: data.turn.newMove.to,
				correct: data.turn.isCorect
			}
		});
	});
	
	_client.gameManager.on('error', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_ERROR,
			data: data
		});
	});
	
	_client.gameManager.on('switch_player', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.SWITCH_PLAYER,
			data: data
		});
	});
	
	_client.gameManager.on('event', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_EVENT,
			data: data
		});
	});
	_client.gameManager.on('timeout', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_TIMEOUT,
			data: data
		});
	});
	_client.gameManager.on('round_end', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.ROUND_END,
			data: data
		});
	});
	
	_client.gameManager.on('game_leave', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_LEAVE,
			data: data
		});
	});
	
	_client.gameManager.on('take_back', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_TAKEBACK,
			data: data
		});
	});
	
	_client.gameManager.on('time', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_TIMETICK,
			data: data
		});
	});
	
	_client.gameManager.on('focus', function (data) {
		//	app.game.store.dispatch({
		//		type: userActions.USER_FOCUS,
		//		data: data
		//	});
	});
	
	_client.on('show_profile', function (data) {
		//	console.log('main;', 'show_profile user:', data);
	});
	
	_client.on('settings_changed', function (data) {
		app.game.store.dispatch({
			type: _optionsActions.optionsActions.OPTIONS_CHENGED,
			data: data
		});
	});
	
	_client.on('settings_saved', function (data) {
		app.game.store.dispatch({
			type: _optionsActions.optionsActions.OPTIONS_SAVED,
			data: data
		});
	});
	// send events buttons example
	
	_client.historyManager.on('game_load', function (data) {
		app.game.store.dispatch({
			type: _gameActions.gameActions.GAME_HISTORY_LOAD,
			data: data
		});
	});
	
	_generateEndGameBtn();
	function _generateEndGameBtn() {
		var bdiv = $('<div>');
		bdiv.addClass('v6-buttons');
		$('body').append(bdiv);
		var div = $('<div>');
		div.attr('id', 'endGameButton');
		div.html('<span>Выйти из игры</span>');
		div.on('click', function () {
			window._client.gameManager.leaveGame();
		});
		bdiv.append(div);
		div = $('<div>');
		div.attr('id', 'drawButton');
		div.html('<span>Предложить ничью</span>');
		div.on('click', function () {
			window._client.gameManager.sendDraw();
		});
		bdiv.append(div);
		div = $('<div>');
		div.attr('id', 'winButton');
		div.html('<span>Победный ход</span>');
		div.on('click', function () {
			window._client.gameManager.sendTurn({ result: 1 });
		});
		bdiv.append(div);
		div = $('<div>');
		div.attr('id', 'ratingButton');
		div.html('<span>Показать рейтинг</span>');
		div.on('click', function () {
			window._client.ratingManager.getRatings();
		});
		bdiv.append(div);
		div = $('<div>');
		div.attr('id', 'historyButton');
		div.html('<span>Показать историю</span>');
		div.on('click', function () {
			window._client.historyManager.getHistory(false, false, false);
		});
		bdiv.append(div);
		div = $('<div>');
		div.html('<span>Передать ход</span>');
		div.on('click', function () {
			window._client.gameManager.sendTurn({ 'switch': true });
		});
		bdiv.append(div);
		div = $('<div>');
		div.html('<span>Сделать ход</span>');
		div.on('click', function () {
			var newMove = {
				'newMove': {
					from: 'e2',
					to: 'e4',
					promotion: 'q'
				},
				fen: 'FEN'
			};
			window._client.gameManager.sendTurn(newMove);
		});
		bdiv.append(div);
		div = $('<div>');
		div.html('<span>ход назад</span>');
		div.on('click', function () {
			window._client.gameManager.sendTakeBack();
		});
		bdiv.append(div);
	}
	
	function getCookie(c_name) {
		if (document.cookie.length > 0) {
			var c_start = document.cookie.indexOf(c_name + "=");
			if (c_start != -1) {
				c_start = c_start + c_name.length + 1;
				var c_end = document.cookie.indexOf(";", c_start);
				if (c_end == -1) c_end = document.cookie.length;
				return unescape(document.cookie.substring(c_start, c_end));
			}
		}
		return "";
	}
	
	//	if (window._username == 'Chrome')
	//		require('./../bot/bot.js');

/***/ }
/******/ ]);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZmRkYzA1ZGQ4NWM5MWEyZjU0YmQiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi4vZnJvbnRlbmQvc3JjL2RlYnVnL2RlYnVnUGFuZWwuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9kZWJ1Zy9kZWJ1Z1BhbmVsLmNzcyIsIndlYnBhY2s6Ly8vLi4vfi9yZWR1eC9saWIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgvbGliL2NyZWF0ZVN0b3JlLmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi91dGlscy9pc1BsYWluT2JqZWN0LmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi91dGlscy9jb21iaW5lUmVkdWNlcnMuanMiLCJ3ZWJwYWNrOi8vLy4uL34vcHJvY2Vzcy9icm93c2VyLmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi91dGlscy9tYXBWYWx1ZXMuanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgvbGliL3V0aWxzL3BpY2suanMiLCJ3ZWJwYWNrOi8vLy4uL34vcmVkdXgvbGliL3V0aWxzL2JpbmRBY3Rpb25DcmVhdG9ycy5qcyIsIndlYnBhY2s6Ly8vLi4vfi9yZWR1eC9saWIvdXRpbHMvYXBwbHlNaWRkbGV3YXJlLmpzIiwid2VicGFjazovLy8uLi9+L3JlZHV4L2xpYi91dGlscy9jb21wb3NlLmpzIiwid2VicGFjazovLy8uLi9mcm9udGVuZC9zcmMvZ2FtZS9hY3Rpb25zL2FwcEFjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL2FjdGlvbnMvdXNlckFjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL2FjdGlvbnMvZ2FtZUFjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL2FjdGlvbnMvb3B0aW9uc0FjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL3JlZHVjZXJzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi4vZnJvbnRlbmQvc3JjL2dhbWUvY2xhc3Nlcy9hcHAuY2xhc3MuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL3JlZHVjZXJzL3VzZXIuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL3JlZHVjZXJzL2dhbWUuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9nYW1lL3JlZHVjZXJzL29wdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9pbmRleC5jc3MiLCJ3ZWJwYWNrOi8vLy4uL2Zyb250ZW5kL3NyYy9sYXlvdXQuc2NzcyIsIndlYnBhY2s6Ly8vLi4vZnJvbnRlbmQvc3JjL2NsaWVudC9pbml0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx1QkFBZTtBQUNmO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQ0EsS0FBSSxVQUFVLEdBQUcseUJBQWU7QUFDL0IsZUFBYSxFQUFFLFlBQVk7RUFDM0IsQ0FBQzs7Ozs7Ozs7QUFBQzs7QUFrQkgsT0FBTSxDQUFDLEdBQUcsR0FBRztBQUNaLE1BQUksRUFBRTtBQUNMLFFBQUssRUFBRyxFQUFFO0dBQ1Y7RUFDRCxDQUFDOztBQUVGLE9BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxXQXJCaEIsV0FBVyxFQXFCaUIsV0FyQmYsZUFBZSxFQXFCZ0I7QUFDOUMsS0FBRyxPQWJELEdBYUM7QUFDSCxNQUFJLFFBYkYsSUFhRTtBQUNKLE1BQUksUUFiRixJQWFFO0FBQ0osU0FBTyxXQWJMLE9BYUs7RUFDVCxDQUFDLENBQ0Y7OztBQU9ILEtBQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsWUFBTTtBQUN2RCxNQUFJLFVBQVUsRUFBRSxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFDN0gsU0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztFQUM5QyxDQUFDOzs7Ozs7QUFNRixvQkFBTyxDQUFDLEVBQWEsQ0FBQzs7Ozs7QUFPdEIsb0JBQU8sQ0FBQyxFQUFlLENBQUMsQ0FBQztBQUN6QixTQUFRLEdBQUcsS0FBSyxDQUFDOztBQUVqQixPQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFXO0FBQ2hDLFNBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDdkIscUJBQU8sQ0FBQyxFQUFrQixDQUFDOzs7Ozs7RUFPM0IsQ0FBQyxDOzs7Ozs7Ozs7Ozs7OztBQ3JFRixvQkFBTyxDQUFDLENBQWtCLENBQUMsQ0FBQzs7Y0FHM0IsZ0JBQWEsT0FBTyxFQUFFOzs7O0FBRXJCLE9BQUksQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUN6QyxPQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxZQUFZLENBQUM7QUFDM0IsT0FBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLDJUQVFtRixDQUFDO0FBQ3RHLFdBQVEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7O0FBRXJFLFdBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxFQUFFO0FBQ3pFLE1BQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztBQUNuQixTQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4RCxDQUFDLENBQUM7RUFDSDs7O0FBQ0QsRTs7Ozs7O0FDdkJELDBDOzs7Ozs7Ozs7QUNBQSxhQUFZLENBQUM7O0FBRWIsUUFBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7O0FBRTFCLFVBQVMsc0JBQXNCLENBQUMsR0FBRyxFQUFFO0FBQUUsVUFBTyxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcsRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLENBQUM7RUFBRTs7QUFFakcsS0FBSSxZQUFZLEdBQUcsbUJBQU8sQ0FBQyxDQUFlLENBQUMsQ0FBQzs7QUFFNUMsS0FBSSxhQUFhLEdBQUcsc0JBQXNCLENBQUMsWUFBWSxDQUFDLENBQUM7O0FBRXpELEtBQUkscUJBQXFCLEdBQUcsbUJBQU8sQ0FBQyxDQUF5QixDQUFDLENBQUM7O0FBRS9ELEtBQUksc0JBQXNCLEdBQUcsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsQ0FBQzs7QUFFM0UsS0FBSSx3QkFBd0IsR0FBRyxtQkFBTyxDQUFDLEVBQTRCLENBQUMsQ0FBQzs7QUFFckUsS0FBSSx5QkFBeUIsR0FBRyxzQkFBc0IsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDOztBQUVqRixLQUFJLHFCQUFxQixHQUFHLG1CQUFPLENBQUMsRUFBeUIsQ0FBQyxDQUFDOztBQUUvRCxLQUFJLHNCQUFzQixHQUFHLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLENBQUM7O0FBRTNFLEtBQUksYUFBYSxHQUFHLG1CQUFPLENBQUMsRUFBaUIsQ0FBQyxDQUFDOztBQUUvQyxLQUFJLGNBQWMsR0FBRyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7QUFFM0QsUUFBTyxDQUFDLFdBQVcsR0FBRyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDL0MsUUFBTyxDQUFDLGVBQWUsR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUM1RCxRQUFPLENBQUMsa0JBQWtCLEdBQUcseUJBQXlCLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDbEUsUUFBTyxDQUFDLGVBQWUsR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUM1RCxRQUFPLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQyxTQUFTLENBQUMsQzs7Ozs7O0FDOUIzQyxhQUFZLENBQUM7O0FBRWIsUUFBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7QUFDMUIsUUFBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLFdBQVcsQ0FBQzs7QUFFakMsVUFBUyxzQkFBc0IsQ0FBQyxHQUFHLEVBQUU7QUFBRSxVQUFPLEdBQUcsSUFBSSxHQUFHLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsQ0FBQztFQUFFOztBQUVqRyxLQUFJLG1CQUFtQixHQUFHLG1CQUFPLENBQUMsQ0FBdUIsQ0FBQyxDQUFDOztBQUUzRCxLQUFJLG9CQUFvQixHQUFHLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDOzs7Ozs7OztBQVF0RSxLQUFJLFdBQVcsR0FBRztBQUNoQixPQUFJLEVBQUUsY0FBYztFQUNyQixDQUFDOztBQUVGLFFBQU8sQ0FBQyxXQUFXLEdBQUcsV0FBVzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCakMsVUFBUyxXQUFXLENBQUMsT0FBTyxFQUFFLFlBQVksRUFBRTtBQUMxQyxPQUFJLE9BQU8sT0FBTyxLQUFLLFVBQVUsRUFBRTtBQUNqQyxXQUFNLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7SUFDM0Q7O0FBRUQsT0FBSSxjQUFjLEdBQUcsT0FBTyxDQUFDO0FBQzdCLE9BQUksWUFBWSxHQUFHLFlBQVksQ0FBQztBQUNoQyxPQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7QUFDbkIsT0FBSSxhQUFhLEdBQUcsS0FBSzs7Ozs7OztBQU96QixZQUFTLFFBQVEsR0FBRztBQUNsQixZQUFPLFlBQVksQ0FBQztJQUNyQjs7Ozs7Ozs7OztBQVVELFlBQVMsU0FBUyxDQUFDLFFBQVEsRUFBRTtBQUMzQixjQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ3pCLFNBQUksWUFBWSxHQUFHLElBQUksQ0FBQzs7QUFFeEIsWUFBTyxTQUFTLFdBQVcsR0FBRztBQUM1QixXQUFJLENBQUMsWUFBWSxFQUFFO0FBQ2pCLGdCQUFPO1FBQ1I7O0FBRUQsbUJBQVksR0FBRyxLQUFLLENBQUM7QUFDckIsV0FBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUN4QyxnQkFBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7TUFDNUIsQ0FBQztJQUNIOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQkQsWUFBUyxRQUFRLENBQUMsTUFBTSxFQUFFO0FBQ3hCLFNBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRTtBQUM1QyxhQUFNLElBQUksS0FBSyxDQUFDLGlDQUFpQyxHQUFHLDBDQUEwQyxDQUFDLENBQUM7TUFDakc7O0FBRUQsU0FBSSxPQUFPLE1BQU0sQ0FBQyxJQUFJLEtBQUssV0FBVyxFQUFFO0FBQ3RDLGFBQU0sSUFBSSxLQUFLLENBQUMscURBQXFELEdBQUcsaUNBQWlDLENBQUMsQ0FBQztNQUM1Rzs7QUFFRCxTQUFJLGFBQWEsRUFBRTtBQUNqQixhQUFNLElBQUksS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7TUFDdkQ7O0FBRUQsU0FBSTtBQUNGLG9CQUFhLEdBQUcsSUFBSSxDQUFDO0FBQ3JCLG1CQUFZLEdBQUcsY0FBYyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsQ0FBQztNQUNyRCxTQUFTO0FBQ1Isb0JBQWEsR0FBRyxLQUFLLENBQUM7TUFDdkI7O0FBRUQsY0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLFFBQVEsRUFBRTtBQUM1QyxjQUFPLFFBQVEsRUFBRSxDQUFDO01BQ25CLENBQUMsQ0FBQztBQUNILFlBQU8sTUFBTSxDQUFDO0lBQ2Y7Ozs7Ozs7Ozs7OztBQVlELFlBQVMsY0FBYyxDQUFDLFdBQVcsRUFBRTtBQUNuQyxtQkFBYyxHQUFHLFdBQVcsQ0FBQztBQUM3QixhQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdEM7Ozs7O0FBS0QsV0FBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDOztBQUVyQyxVQUFPO0FBQ0wsYUFBUSxFQUFFLFFBQVE7QUFDbEIsY0FBUyxFQUFFLFNBQVM7QUFDcEIsYUFBUSxFQUFFLFFBQVE7QUFDbEIsbUJBQWMsRUFBRSxjQUFjO0lBQy9CLENBQUM7Ozs7Ozs7QUNqS0osYUFBWSxDQUFDOzs7O0FBRWIsUUFBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7QUFDMUIsUUFBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLGFBQWEsQ0FBQztBQUNuQyxLQUFJLFVBQVUsR0FBRyxTQUFTLFVBQVUsQ0FBQyxFQUFFLEVBQUU7QUFDdkMsVUFBTyxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7RUFDN0MsQ0FBQztBQUNGLEtBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7Ozs7Ozs7QUFPdkMsVUFBUyxhQUFhLENBQUMsR0FBRyxFQUFFO0FBQzFCLE9BQUksQ0FBQyxHQUFHLElBQUksUUFBTyxHQUFHLHlDQUFILEdBQUcsT0FBSyxRQUFRLEVBQUU7QUFDbkMsWUFBTyxLQUFLLENBQUM7SUFDZDs7QUFFRCxPQUFJLEtBQUssR0FBRyxPQUFPLEdBQUcsQ0FBQyxXQUFXLEtBQUssVUFBVSxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQzs7QUFFbEcsT0FBSSxLQUFLLEtBQUssSUFBSSxFQUFFO0FBQ2xCLFlBQU8sSUFBSSxDQUFDO0lBQ2I7O0FBRUQsT0FBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQzs7QUFFcEMsVUFBTyxPQUFPLFdBQVcsS0FBSyxVQUFVLElBQUksV0FBVyxZQUFZLFdBQVcsSUFBSSxVQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssY0FBYyxDQUFDO0VBQzlIOztBQUVELE9BQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDOzs7Ozs7QUM5Qm5DLDREQUFZLENBQUM7O0FBRWIsUUFBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7QUFDMUIsUUFBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLGVBQWUsQ0FBQzs7QUFFckMsVUFBUyxzQkFBc0IsQ0FBQyxHQUFHLEVBQUU7QUFBRSxVQUFPLEdBQUcsSUFBSSxHQUFHLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsQ0FBQztFQUFFOztBQUVqRyxLQUFJLFlBQVksR0FBRyxtQkFBTyxDQUFDLENBQWdCLENBQUMsQ0FBQzs7QUFFN0MsS0FBSSxjQUFjLEdBQUcsbUJBQU8sQ0FBQyxDQUFpQixDQUFDLENBQUM7O0FBRWhELEtBQUksZUFBZSxHQUFHLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxDQUFDOztBQUU3RCxLQUFJLFVBQVUsR0FBRyxtQkFBTyxDQUFDLEVBQWEsQ0FBQyxDQUFDOztBQUV4QyxLQUFJLFdBQVcsR0FBRyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQzs7QUFFckQsS0FBSSxLQUFLLEdBQUcsbUJBQU8sQ0FBQyxFQUFRLENBQUMsQ0FBQzs7QUFFOUIsS0FBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUMsS0FBSyxDQUFDOzs7O0FBSTFDLFVBQVMsNkJBQTZCLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRTtBQUNsRCxPQUFJLFVBQVUsR0FBRyxNQUFNLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQztBQUN2QyxPQUFJLFVBQVUsR0FBRyxVQUFVLElBQUksR0FBRyxHQUFHLFVBQVUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHLElBQUksV0FBVyxDQUFDOztBQUVoRixVQUFPLFdBQVcsR0FBRyxHQUFHLEdBQUcsZ0NBQWdDLEdBQUcsVUFBVSxHQUFHLElBQUksR0FBRyxxRUFBcUUsQ0FBQztFQUN6Sjs7QUFFRCxVQUFTLG1DQUFtQyxDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFO0FBQzVFLE9BQUksV0FBVyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDM0MsT0FBSSxZQUFZLEdBQUcsTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsNkNBQTZDLEdBQUcsd0NBQXdDLENBQUM7O0FBRXRLLE9BQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7QUFDNUIsWUFBTyxxRUFBcUUsR0FBRyw0REFBNEQsQ0FBQztJQUM3STs7QUFFRCxPQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUFFO0FBQzNDLFlBQU8sTUFBTSxHQUFHLFlBQVksR0FBRywyQkFBMkIsR0FBRyxDQUFDLEdBQUUsQ0FBRSxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLDBEQUEwRCxJQUFJLFNBQVMsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0lBQ3BPOztBQUVELE9BQUksY0FBYyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxFQUFFO0FBQ2pFLFlBQU8sV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDckMsQ0FBQyxDQUFDOztBQUVILE9BQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7QUFDN0IsWUFBTyxhQUFhLElBQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsTUFBTSxHQUFHLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxHQUFHLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxhQUFhLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxHQUFHLDBEQUEwRCxJQUFJLEdBQUcsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLHFDQUFxQyxDQUFDLENBQUM7SUFDL1I7RUFDRjs7QUFFRCxVQUFTLG1CQUFtQixDQUFDLFFBQVEsRUFBRTtBQUNyQyxTQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRTtBQUMzQyxTQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDNUIsU0FBSSxZQUFZLEdBQUcsT0FBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7O0FBRS9FLFNBQUksT0FBTyxZQUFZLEtBQUssV0FBVyxFQUFFO0FBQ3ZDLGFBQU0sSUFBSSxLQUFLLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyw4Q0FBOEMsR0FBRyw0REFBNEQsR0FBRyw2REFBNkQsR0FBRyxtQkFBbUIsQ0FBQyxDQUFDO01BQzFPOztBQUVELFNBQUksSUFBSSxHQUFHLCtCQUErQixHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDekcsU0FBSSxPQUFPLE9BQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxXQUFXLEVBQUU7QUFDN0QsYUFBTSxJQUFJLEtBQUssQ0FBQyxXQUFXLEdBQUcsR0FBRyxHQUFHLHVEQUF1RCxJQUFJLHVCQUF1QixHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLGlDQUFpQyxDQUFDLEdBQUcsdUVBQXVFLEdBQUcsaUVBQWlFLEdBQUcscUVBQXFFLEdBQUcsc0RBQXNELENBQUMsQ0FBQztNQUM3YztJQUNGLENBQUMsQ0FBQztFQUNKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJELFVBQVMsZUFBZSxDQUFDLFFBQVEsRUFBRTtBQUNqQyxPQUFJLGFBQWEsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxFQUFFLFVBQVUsR0FBRyxFQUFFO0FBQzdELFlBQU8sT0FBTyxHQUFHLEtBQUssVUFBVSxDQUFDO0lBQ2xDLENBQUMsQ0FBQztBQUNILE9BQUksV0FBVyxDQUFDOztBQUVoQixPQUFJO0FBQ0Ysd0JBQW1CLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDcEMsQ0FBQyxPQUFPLENBQUMsRUFBRTtBQUNWLGdCQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ2pCOztBQUVELE9BQUksWUFBWSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxhQUFhLEVBQUUsWUFBWTtBQUNuRSxZQUFPLFNBQVMsQ0FBQztJQUNsQixDQUFDLENBQUM7O0FBRUgsVUFBTyxTQUFTLFdBQVcsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFO0FBQ3pDLFNBQUksS0FBSyxLQUFLLFNBQVMsRUFBRSxLQUFLLEdBQUcsWUFBWSxDQUFDOztBQUU5QyxTQUFJLFdBQVcsRUFBRTtBQUNmLGFBQU0sV0FBVyxDQUFDO01BQ25COztBQUVELFNBQUksVUFBVSxHQUFHLEtBQUssQ0FBQztBQUN2QixTQUFJLFVBQVUsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsYUFBYSxFQUFFLFVBQVUsT0FBTyxFQUFFLEdBQUcsRUFBRTtBQUM3RSxXQUFJLG1CQUFtQixHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNyQyxXQUFJLGVBQWUsR0FBRyxPQUFPLENBQUMsbUJBQW1CLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDM0QsV0FBSSxPQUFPLGVBQWUsS0FBSyxXQUFXLEVBQUU7QUFDMUMsYUFBSSxZQUFZLEdBQUcsNkJBQTZCLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzlELGVBQU0sSUFBSSxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDL0I7QUFDRCxpQkFBVSxHQUFHLFVBQVUsSUFBSSxlQUFlLEtBQUssbUJBQW1CLENBQUM7QUFDbkUsY0FBTyxlQUFlLENBQUM7TUFDeEIsQ0FBQyxDQUFDOztBQUVILFNBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEtBQUssWUFBWSxFQUFFO0FBQ3pDLFdBQUksY0FBYyxHQUFHLG1DQUFtQyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDcEYsV0FBSSxjQUFjLEVBQUU7QUFDbEIsZ0JBQU8sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDL0I7TUFDRjs7QUFFRCxZQUFPLFVBQVUsR0FBRyxVQUFVLEdBQUcsS0FBSyxDQUFDO0lBQ3hDLENBQUM7RUFDSDs7QUFFRCxPQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQzs7Ozs7Ozs7Ozs7QUNoSW5DLEtBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ2xDLEtBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztBQUNmLEtBQUksUUFBUSxHQUFHLEtBQUssQ0FBQztBQUNyQixLQUFJLFlBQVksQ0FBQztBQUNqQixLQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQzs7QUFFcEIsVUFBUyxlQUFlLEdBQUc7QUFDdkIsYUFBUSxHQUFHLEtBQUssQ0FBQztBQUNqQixTQUFJLFlBQVksQ0FBQyxNQUFNLEVBQUU7QUFDckIsY0FBSyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7TUFDdEMsTUFBTTtBQUNILG1CQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7TUFDbkI7QUFDRCxTQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7QUFDZCxtQkFBVSxFQUFFLENBQUM7TUFDaEI7RUFDSjs7QUFFRCxVQUFTLFVBQVUsR0FBRztBQUNsQixTQUFJLFFBQVEsRUFBRTtBQUNWLGdCQUFPO01BQ1Y7QUFDRCxTQUFJLE9BQU8sR0FBRyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDMUMsYUFBUSxHQUFHLElBQUksQ0FBQzs7QUFFaEIsU0FBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztBQUN2QixZQUFNLEdBQUcsRUFBRTtBQUNQLHFCQUFZLEdBQUcsS0FBSyxDQUFDO0FBQ3JCLGNBQUssR0FBRyxFQUFFLENBQUM7QUFDWCxnQkFBTyxFQUFFLFVBQVUsR0FBRyxHQUFHLEVBQUU7QUFDdkIsaUJBQUksWUFBWSxFQUFFO0FBQ2QsNkJBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztjQUNsQztVQUNKO0FBQ0QsbUJBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUNoQixZQUFHLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztNQUN0QjtBQUNELGlCQUFZLEdBQUcsSUFBSSxDQUFDO0FBQ3BCLGFBQVEsR0FBRyxLQUFLLENBQUM7QUFDakIsaUJBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztFQUN6Qjs7QUFFRCxRQUFPLENBQUMsUUFBUSxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQzlCLFNBQUksSUFBSSxHQUFHLElBQUksS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDM0MsU0FBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUN0QixjQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUN2QyxpQkFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7VUFDOUI7TUFDSjtBQUNELFVBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7QUFDaEMsU0FBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtBQUNqQyxtQkFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQztNQUM3QjtFQUNKOzs7QUFHRCxVQUFTLElBQUksQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFO0FBQ3RCLFNBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO0FBQ2YsU0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7RUFDdEI7QUFDRCxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsR0FBRyxZQUFZO0FBQzdCLFNBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7RUFDcEMsQ0FBQztBQUNGLFFBQU8sQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO0FBQzFCLFFBQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0FBQ3ZCLFFBQU8sQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO0FBQ2pCLFFBQU8sQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0FBQ2xCLFFBQU8sQ0FBQyxPQUFPLEdBQUcsRUFBRTtBQUNwQixRQUFPLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQzs7QUFFdEIsVUFBUyxJQUFJLEdBQUcsRUFBRTs7QUFFbEIsUUFBTyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUM7QUFDbEIsUUFBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7QUFDM0IsUUFBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7QUFDcEIsUUFBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7QUFDbkIsUUFBTyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7QUFDOUIsUUFBTyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztBQUNsQyxRQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzs7QUFFcEIsUUFBTyxDQUFDLE9BQU8sR0FBRyxVQUFVLElBQUksRUFBRTtBQUM5QixXQUFNLElBQUksS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7RUFDdkQsQ0FBQzs7QUFFRixRQUFPLENBQUMsR0FBRyxHQUFHLFlBQVk7QUFBRSxZQUFPLEdBQUc7RUFBRSxDQUFDO0FBQ3pDLFFBQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxHQUFHLEVBQUU7QUFDM0IsV0FBTSxJQUFJLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO0VBQ3JELENBQUM7QUFDRixRQUFPLENBQUMsS0FBSyxHQUFHLFlBQVc7QUFBRSxZQUFPLENBQUMsQ0FBQztFQUFFLEM7Ozs7Ozs7Ozs7Ozs7QUNuRnhDLGFBQVksQ0FBQzs7QUFFYixRQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztBQUMxQixRQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDOztBQUUvQixVQUFTLFNBQVMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFO0FBQzFCLFVBQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxNQUFNLEVBQUUsR0FBRyxFQUFFO0FBQ3BELFdBQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0FBQ2hDLFlBQU8sTUFBTSxDQUFDO0lBQ2YsRUFBRSxFQUFFLENBQUMsQ0FBQztFQUNSOztBQUVELE9BQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDOzs7Ozs7Ozs7Ozs7O0FDWm5DLGFBQVksQ0FBQzs7QUFFYixRQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztBQUMxQixRQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDOztBQUUxQixVQUFTLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFO0FBQ3JCLFVBQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxNQUFNLEVBQUUsR0FBRyxFQUFFO0FBQ3BELFNBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO0FBQ2hCLGFBQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7TUFDeEI7QUFDRCxZQUFPLE1BQU0sQ0FBQztJQUNmLEVBQUUsRUFBRSxDQUFDLENBQUM7RUFDUjs7QUFFRCxPQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQzs7Ozs7O0FDckJuQyxhQUFZLENBQUM7Ozs7QUFFYixRQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztBQUMxQixRQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsa0JBQWtCLENBQUM7O0FBRXhDLFVBQVMsc0JBQXNCLENBQUMsR0FBRyxFQUFFO0FBQUUsVUFBTyxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcsRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLENBQUM7RUFBRTs7QUFFakcsS0FBSSxVQUFVLEdBQUcsbUJBQU8sQ0FBQyxFQUFhLENBQUMsQ0FBQzs7QUFFeEMsS0FBSSxXQUFXLEdBQUcsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUM7O0FBRXJELFVBQVMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLFFBQVEsRUFBRTtBQUNsRCxVQUFPLFlBQVk7QUFDakIsWUFBTyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUM1RCxDQUFDO0VBQ0g7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdCRCxVQUFTLGtCQUFrQixDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUU7QUFDcEQsT0FBSSxPQUFPLGNBQWMsS0FBSyxVQUFVLEVBQUU7QUFDeEMsWUFBTyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEQ7O0FBRUQsT0FBSSxRQUFPLGNBQWMseUNBQWQsY0FBYyxPQUFLLFFBQVEsSUFBSSxjQUFjLEtBQUssSUFBSSxJQUFJLGNBQWMsS0FBSyxTQUFTLEVBQUU7QUFDakcsV0FBTSxJQUFJLEtBQUssQ0FBQyx3RUFBd0UsSUFBSSxjQUFjLEtBQUssSUFBSSxHQUFHLE1BQU0sVUFBVSxjQUFjLHlDQUFkLGNBQWMsRUFBQyxHQUFHLElBQUksR0FBRywwRkFBMEYsQ0FBQyxDQUFDO0lBQzVQOztBQUVELFVBQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxVQUFVLGFBQWEsRUFBRTtBQUNyRSxZQUFPLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNuRCxDQUFDLENBQUM7RUFDSjs7QUFFRCxPQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQzs7Ozs7O0FDckRuQyxhQUFZLENBQUM7O0FBRWIsUUFBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7O0FBRTFCLEtBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksVUFBVSxNQUFNLEVBQUU7QUFBRSxRQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUFFLFNBQUksTUFBTSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBRSxLQUFLLElBQUksR0FBRyxJQUFJLE1BQU0sRUFBRTtBQUFFLFdBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsRUFBRTtBQUFFLGVBQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFBRTtNQUFFO0lBQUcsT0FBTyxNQUFNLENBQUM7RUFBRSxDQUFDOztBQUVqUSxRQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsZUFBZSxDQUFDOztBQUVyQyxVQUFTLHNCQUFzQixDQUFDLEdBQUcsRUFBRTtBQUFFLFVBQU8sR0FBRyxJQUFJLEdBQUcsQ0FBQyxVQUFVLEdBQUcsR0FBRyxHQUFHLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxDQUFDO0VBQUU7O0FBRWpHLEtBQUksUUFBUSxHQUFHLG1CQUFPLENBQUMsRUFBVyxDQUFDLENBQUM7O0FBRXBDLEtBQUksU0FBUyxHQUFHLHNCQUFzQixDQUFDLFFBQVEsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CaEQsVUFBUyxlQUFlLEdBQUc7QUFDekIsUUFBSyxJQUFJLElBQUksR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO0FBQzFGLGdCQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3JDOztBQUVELFVBQU8sVUFBVSxJQUFJLEVBQUU7QUFDckIsWUFBTyxVQUFVLE9BQU8sRUFBRSxZQUFZLEVBQUU7QUFDdEMsV0FBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQztBQUN4QyxXQUFJLFNBQVMsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQy9CLFdBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQzs7QUFFZixXQUFJLGFBQWEsR0FBRztBQUNsQixpQkFBUSxFQUFFLEtBQUssQ0FBQyxRQUFRO0FBQ3hCLGlCQUFRLEVBQUUsU0FBUyxRQUFRLENBQUMsTUFBTSxFQUFFO0FBQ2xDLGtCQUFPLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztVQUMxQjtRQUNGLENBQUM7QUFDRixZQUFLLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxVQUFVLFVBQVUsRUFBRTtBQUM1QyxnQkFBTyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDO0FBQ0gsZ0JBQVMsR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7O0FBRXpFLGNBQU8sUUFBUSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUU7QUFDekIsaUJBQVEsRUFBRSxTQUFTO1FBQ3BCLENBQUMsQ0FBQztNQUNKLENBQUM7SUFDSCxDQUFDO0VBQ0g7O0FBRUQsT0FBTSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLEM7Ozs7Ozs7Ozs7Ozs7QUNyRG5DLGFBQVksQ0FBQzs7QUFFYixRQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztBQUMxQixRQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsT0FBTyxDQUFDOztBQUU3QixVQUFTLE9BQU8sR0FBRztBQUNqQixRQUFLLElBQUksSUFBSSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUU7QUFDcEYsVUFBSyxDQUFDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMvQjs7QUFFRCxVQUFPLFVBQVUsR0FBRyxFQUFFO0FBQ3BCLFlBQU8sS0FBSyxDQUFDLFdBQVcsQ0FBQyxVQUFVLFFBQVEsRUFBRSxDQUFDLEVBQUU7QUFDOUMsY0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7TUFDcEIsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNULENBQUM7RUFDSDs7QUFFRCxPQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQzs7Ozs7Ozs7Ozs7QUN4QjVCLEtBQU0sVUFBVSxXQUFWLFVBQVUsR0FBRztBQUN6QixVQUFRLEVBQUUsVUFBVTtBQUNwQixZQUFVLEVBQUUsWUFBWTtBQUN4QixXQUFTLEVBQUUsV0FBVztBQUN0QixTQUFPLEVBQUUsU0FBUztBQUNsQixXQUFTLEVBQUUsV0FBVztBQUN0QixhQUFXLEVBQUUsYUFBYTtFQUMxQixDOzs7Ozs7Ozs7OztBQ1BNLEtBQU0sV0FBVyxXQUFYLFdBQVcsR0FBRztBQUMxQixZQUFVLEVBQUUsWUFBWTtBQUN4QixhQUFXLEVBQUUsWUFBWTtBQUN6QixhQUFXLEVBQUUsY0FBYztBQUMzQixjQUFZLEVBQUUsY0FBYztBQUM1QixhQUFXLEVBQUUsYUFBYTtBQUMxQixXQUFTLEVBQUUsV0FBVztBQUN0Qix1QkFBcUIsRUFBRSx1QkFBdUI7QUFDOUMsbUJBQWlCLEVBQUUsbUJBQW1CO0FBQ3RDLGlCQUFlLEVBQUUsaUJBQWlCO0FBQ2xDLGtCQUFnQixFQUFFLGtCQUFrQjtBQUNwQyxxQkFBbUIsRUFBRSxxQkFBcUI7QUFDMUMsc0JBQW9CLEVBQUUsc0JBQXNCO0FBQzVDLFlBQVUsRUFBRSxZQUFZO0VBQ3hCLEM7Ozs7Ozs7Ozs7O0FDZE0sS0FBTSxXQUFXLFdBQVgsV0FBVyxHQUFHO0FBQzFCLFlBQVUsRUFBRSxZQUFZO0FBQ3hCLGFBQVcsRUFBRSxhQUFhO0FBQzFCLFdBQVMsRUFBRSxXQUFXO0FBQ3RCLG1CQUFpQixFQUFFLG1CQUFtQjtBQUN0QyxXQUFTLEVBQUUsV0FBVztBQUN0QixlQUFhLEVBQUUsZUFBZTtBQUM5QixjQUFZLEVBQUUsY0FBYztBQUM1QixlQUFhLEVBQUUsZUFBZTtBQUM5QixXQUFTLEVBQUUsV0FBVztBQUN0QixZQUFVLEVBQUUsWUFBWTtBQUN4QixZQUFVLEVBQUUsWUFBWTtBQUN4QixlQUFhLEVBQUUsZUFBZTtBQUM5QixZQUFVLEVBQUUsWUFBWTtFQUN4QixDOzs7Ozs7Ozs7OztBQ2RNLEtBQU0sY0FBYyxXQUFkLGNBQWMsR0FBRztBQUM3QixpQkFBZSxFQUFFLGlCQUFpQjtBQUNsQyxlQUFhLEVBQUUsZUFBZTtBQUM5QixlQUFhLEVBQUUsZUFBZTtFQUM5QixDOzs7Ozs7Ozs7OztTQ0FlLEdBQUcsR0FBSCxHQUFHOzs7Ozs7Ozs7OztBQUFaLFVBQVMsR0FBRyxHQUF5QjtNQUF2QixLQUFLLHlEQUFHLElBQUk7TUFBRyxNQUFNOztBQUN6QyxVQUFRLE1BQU0sQ0FBQyxJQUFJOztBQUVsQixRQUFLLFlBTkMsVUFBVSxDQU1BLFFBQVE7QUFDdkIsV0FBTyxrQkFBYyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7O0FBRWhELFFBQUssWUFUQyxVQUFVLENBU0EsV0FBVztBQUMxQixXQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRTtBQUMvQixnQkFBVyxFQUFFLE1BQU0sQ0FBQyxJQUFJO0tBQ3hCLENBQUMsQ0FBQztBQUNKLFFBQUssWUFiQyxVQUFVLENBYUEsVUFBVTtBQUFFLFdBQU8sS0FBSyxDQUFDO0FBQ3pDLFFBQUssWUFkQyxVQUFVLENBY0EsU0FBUztBQUFFLFdBQU8sS0FBSyxDQUFDO0FBQ3hDLFFBQUssWUFmQyxVQUFVLENBZUEsT0FBTztBQUFFLFdBQU8sS0FBSyxDQUFDOztBQUV0QztBQUFTLFdBQU8sS0FBSyxDQUFDO0FBQUEsR0FDdEI7RUFDRCxDOzs7Ozs7Ozs7Ozs7Ozs7O0tDcEJLLFFBQVE7QUFDYixXQURLLFFBQVEsQ0FDQSxLQUFLLEVBQUUsTUFBTSxFQUFFO3lCQUR2QixRQUFROztBQUVaLE9BQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0FBQ2hCLE9BQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztBQUMzQyxPQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7QUFDbkQsT0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDO0FBQ3JCLE9BQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztBQUN6QixPQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7QUFDeEIsT0FBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO0dBQ3RCOztlQVRJLFFBQVE7OzZCQVVGO0FBQ1YsU0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ1o7Ozs4QkFDVztBQUNYLFdBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNsQjs7O1NBZkksUUFBUTs7O21CQWtCQyxRQUFRLEM7Ozs7Ozs7Ozs7O1NDZFAsSUFBSSxHQUFKLElBQUk7Ozs7OztBQUFiLFVBQVMsSUFBSSxHQUF3QjtNQUF0QixLQUFLLHlEQUFHLElBQUk7TUFBRSxNQUFNOztBQUN6QyxVQUFRLE1BQU0sQ0FBQyxJQUFJO0FBQ2xCLFFBQUssYUFMQyxXQUFXLENBS0EsVUFBVTtBQUFFLFdBQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUMxRSxRQUFLLGFBTkMsV0FBVyxDQU1BLFdBQVc7QUFBRSxXQUFPLEtBQUssQ0FBQztBQUMzQyxRQUFLLGFBUEMsV0FBVyxDQU9BLFdBQVc7QUFBRSxXQUFPLEtBQUssQ0FBQztBQUMzQyxRQUFLLGFBUkMsV0FBVyxDQVFBLFlBQVk7QUFBRSxXQUFPLEtBQUssQ0FBQztBQUM1QyxRQUFLLGFBVEMsV0FBVyxDQVNBLFdBQVc7QUFBRSxXQUFPLEtBQUs7QUFDMUM7QUFBUyxXQUFPLEtBQUssQ0FBQztBQUFBLEdBQ3RCO0VBQ0Q7QUFBQSxFOzs7Ozs7Ozs7OztTQ1ZlLElBQUksR0FBSixJQUFJOzs7Ozs7QUFBYixVQUFTLElBQUksR0FBNkM7TUFBM0MsS0FBSyx5REFBRyxFQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFDLENBQUMsRUFBQztNQUFFLE1BQU07O0FBRTlELE1BQUksTUFBTSxDQUFDLElBQUksSUFBSSxhQUpaLFdBQVcsQ0FJYSxhQUFhLEVBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzs7QUFFckUsVUFBUSxNQUFNLENBQUMsSUFBSTtBQUNsQixRQUFLLGFBUEMsV0FBVyxDQU9BLFVBQVU7QUFBRyxXQUFPLEtBQUs7QUFDMUMsUUFBSyxhQVJDLFdBQVcsQ0FRQSxXQUFXO0FBQUcsV0FBTyxLQUFLO0FBQzNDLFFBQUssYUFUQyxXQUFXLENBU0EsU0FBUztBQUFHLFdBQU8sS0FBSztBQUN6QyxRQUFLLGFBVkMsV0FBVyxDQVVBLFNBQVM7QUFDekIsV0FBTztBQUNOLFVBQUssK0JBQU0sS0FBSyxDQUFDLEtBQUssSUFBRSxNQUFNLENBQUMsSUFBSSxFQUFDO0tBQ3BDO0FBQ0YsUUFBSyxhQWRDLFdBQVcsQ0FjQSxhQUFhO0FBQUcsV0FBTyxLQUFLO0FBQzdDLFFBQUssYUFmQyxXQUFXLENBZUEsWUFBWTtBQUFHLFdBQU8sS0FBSztBQUM1QyxRQUFLLGFBaEJDLFdBQVcsQ0FnQkEsYUFBYTtBQUFHLFdBQU8sS0FBSztBQUM3QyxRQUFLLGFBakJDLFdBQVcsQ0FpQkEsU0FBUztBQUFHLFdBQU8sS0FBSztBQUN6QyxRQUFLLGFBbEJDLFdBQVcsQ0FrQkEsVUFBVTtBQUFHLFdBQU8sS0FBSztBQUMxQyxRQUFLLGFBbkJDLFdBQVcsQ0FtQkEsVUFBVTtBQUFHLFdBQU8sS0FBSztBQUMxQyxRQUFLLGFBcEJDLFdBQVcsQ0FvQkEsVUFBVTtBQUFHLFdBQU8sS0FBSztBQUMxQztBQUFTLFdBQU8sS0FBSyxDQUFDO0FBQUEsR0FDdEI7RUFDRCxDOzs7Ozs7Ozs7OztTQ3JCZSxPQUFPLEdBQVAsT0FBTzs7OztBQUFoQixVQUFTLE9BQU8sR0FBeUI7TUFBdkIsS0FBSyx5REFBRyxJQUFJO01BQUcsTUFBTTs7OztBQUk3QyxVQUFRLE1BQU0sQ0FBQyxJQUFJOztBQUVsQixRQUFLLGdCQVJDLGNBQWMsQ0FRQSxhQUFhO0FBQUUsV0FBTyxLQUFLLENBQUM7QUFDaEQsUUFBSyxnQkFUQyxjQUFjLENBU0EsZUFBZTtBQUFFLFdBQU8sS0FBSyxDQUFDO0FBQ2xELFFBQUssZ0JBVkMsY0FBYyxDQVVBLGFBQWE7QUFBRSxXQUFPLEtBQUs7O0FBRS9DO0FBQVMsV0FBTyxLQUFLLENBQUM7QUFBQSxHQUN0QjtFQUNEO0FBQUEsRTs7Ozs7O0FDZkQsMEM7Ozs7Ozs7QUNBQSwwQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTUEsUUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUN6QixRQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQzs7QUFFbkIsS0FBSSxnQkFBZ0IsR0FBRyw2NUJBQTY1Qjs7Ozs7QUFLaDdCLEtBQUksU0FBUyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQzdDLFFBQU0sQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDO0FBQzNCLFFBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0VBQ2hDO0FBQ0QsS0FBSSxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7QUFDNUMsUUFBTSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUM7QUFDMUIsUUFBTSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7RUFDL0I7QUFDRCxLQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUN6QyxRQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztBQUN6QixRQUFNLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztFQUM5Qjs7O0FBR0wsS0FBSSxJQUFJLEdBQUc7QUFDVixRQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87QUFDdEIsVUFBUSxFQUFFLE1BQU0sQ0FBQyxTQUFTO0FBQzFCLE1BQUksRUFBRSxNQUFNLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxTQUFTO0VBQ3ZDLENBQUM7O0FBRUYsT0FBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQztBQUMzQixNQUFJLEVBQUUsYUFBYTtBQUNuQixNQUFJLEVBQUUsSUFBSTtBQUNWLG1CQUFpQixFQUFFLElBQUk7QUFDdkIsUUFBTSxFQUFFLElBQUk7Ozs7QUFJWixpQkFBZSxFQUFFLElBQUk7QUFDckIsZUFBYSxFQUFFLElBQUk7QUFDbkIsb0JBQWtCLEVBQUUsNEJBQVUsTUFBTSxFQUFFOzs7QUFHckMsT0FBSyxtQkFBbUIsR0FBRyw2QkFBNkIsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxvQkFBb0IsR0FDbEosNENBQTRDLENBQUM7QUFDaEQsT0FBSyxpQkFBaUIsR0FBRyw2QkFBNkIsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxvQkFBb0IsR0FDaEosNEJBQTRCLENBQUM7O0FBRWhDLE9BQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxFQUFFO0FBQ2xDLFdBQU8sbUJBQW1CLENBQUM7SUFDM0IsTUFBTTtBQUNOLFdBQU8saUJBQWlCLENBQUM7SUFDekI7Ozs7O0FBQUEsR0FLRDtBQUNELFFBQU0sRUFBRTtBQUNQLGFBQVUsRUFBRSxhQUFhO0FBQ3pCLFNBQU0sRUFBRSxTQUFTO0FBQ2pCLFdBQVEsRUFBRSxXQUFXO0FBQ3JCLFlBQVMsRUFBRSxXQUFXO0FBQ3RCLFlBQVMsRUFBRSxXQUFXO0dBQ3RCO0FBQ0QsVUFBUSxFQUFFO0FBQ1Qsb0JBQWlCLEVBQUUsT0FBTztBQUMxQiwwQkFBdUIsRUFBRSxHQUFHO0FBQzVCLGlCQUFjLEVBQUUsR0FBRztHQUNuQjtBQUNELGtCQUFnQixFQUFFLGdCQUFnQjtFQUNsQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztBQUVkLEtBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7O0FBRTdCLFFBQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ25DLEtBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztBQUN2QixPQUFJLEVBQUUsWUFoRkEsVUFBVSxDQWdGQyxRQUFRO0FBQ3pCLE9BQUksRUFBRTtBQUNMLFNBQUssRUFBRSxPQUFPLENBQUMsS0FBSztBQUNwQixlQUFXLEVBQUUsT0FBTyxDQUFDLFdBQVc7SUFDaEM7R0FDRCxDQUFDLENBQUM7O0FBRUgsTUFBTyxXQUFXLEdBQUc7QUFDcEIsS0FBRSxFQUFFLElBQUksQ0FBQyxNQUFNO0FBQ2YsT0FBSSxFQUFFLElBQUksQ0FBQyxRQUFRO0FBQ25CLFVBQU8sRUFBRSxJQUFJO0FBQ2IsU0FBTSxFQUFFLElBQUk7QUFDWixPQUFJLEVBQUUsRUFBRTtBQUNSLE1BQUcsRUFBRSxJQUFJO0dBQ1QsQ0FBQztBQUNGLE9BQUssSUFBSSxRQUFRLElBQUssT0FBTyxDQUFDLEtBQUssRUFBRTtBQUNwQyxjQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0dBQ3BFOztBQUVELEtBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztBQUN2QixPQUFJLEVBQUUsYUFuR0EsV0FBVyxDQW1HQyxVQUFVO0FBQzVCLE9BQUksRUFBRSxXQUFXO0dBQ2pCLENBQUMsQ0FBQztFQUVILENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxVQUFVLElBQUksRUFBRTtBQUN6QyxLQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7QUFDdkIsT0FBSSxFQUFFLFlBNUdBLFVBQVUsQ0E0R0MsV0FBVztBQUM1QixPQUFJLEVBQUUsSUFBSTtHQUNWLENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDcEQsS0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLE9BQUksRUFBRSxhQWpIQSxXQUFXLENBaUhDLFVBQVU7QUFDNUIsT0FBSSxFQUFFLElBQUk7R0FDVixDQUFDLENBQUM7RUFDSCxDQUFDLENBQUM7O0FBRUgsUUFBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ3JELEtBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztBQUN2QixPQUFJLEVBQUUsYUF4SEEsV0FBVyxDQXdIQyxXQUFXO0FBQzdCLE9BQUksRUFBRSxJQUFJO0dBQ1YsQ0FBQyxDQUFDO0VBQ0gsQ0FBQyxDQUFDOztBQUVILFFBQU8sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUksRUFBRTtBQUNuRCxLQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7QUFDdkIsT0FBSSxFQUFFLGFBL0hBLFdBQVcsQ0ErSEMsU0FBUztBQUMzQixPQUFJLEVBQUUsSUFBSTtHQUNWLENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDOUMsS0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLE9BQUksRUFBRSxhQXRJQSxXQUFXLENBc0lDLFNBQVM7QUFDM0IsT0FBSSxFQUFFO0FBQ0wsUUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7QUFDNUIsTUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7QUFDeEIsV0FBTyxFQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtJQUM1QjtHQUNELENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDL0MsS0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLE9BQUksRUFBRSxhQWpKQSxXQUFXLENBaUpDLFVBQVU7QUFDNUIsT0FBSSxFQUFFLElBQUk7R0FDVixDQUFDLENBQUM7RUFDSCxDQUFDLENBQUM7O0FBRUgsUUFBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ3ZELEtBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztBQUN2QixPQUFJLEVBQUUsYUF4SkEsV0FBVyxDQXdKQyxhQUFhO0FBQy9CLE9BQUksRUFBRSxJQUFJO0dBQ1YsQ0FBQyxDQUFDO0VBQ0gsQ0FBQyxDQUFDOztBQUVILFFBQU8sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUksRUFBRTtBQUMvQyxLQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7QUFDdkIsT0FBSSxFQUFFLGFBL0pBLFdBQVcsQ0ErSkMsVUFBVTtBQUM1QixPQUFJLEVBQUUsSUFBSTtHQUNWLENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQztBQUNILFFBQU8sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFVLElBQUksRUFBRTtBQUNqRCxLQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7QUFDdkIsT0FBSSxFQUFFLGFBcktBLFdBQVcsQ0FxS0MsWUFBWTtBQUM5QixPQUFJLEVBQUUsSUFBSTtHQUNWLENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQztBQUNILFFBQU8sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUksRUFBRTtBQUNuRCxLQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7QUFDdkIsT0FBSSxFQUFFLGFBM0tBLFdBQVcsQ0EyS0MsU0FBUztBQUMzQixPQUFJLEVBQUUsSUFBSTtHQUNWLENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDcEQsS0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLE9BQUksRUFBRSxhQWxMQSxXQUFXLENBa0xDLFVBQVU7QUFDNUIsT0FBSSxFQUFFLElBQUk7R0FDVixDQUFDLENBQUM7RUFDSCxDQUFDLENBQUM7O0FBRUgsUUFBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ25ELEtBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztBQUN2QixPQUFJLEVBQUUsYUF6TEEsV0FBVyxDQXlMQyxhQUFhO0FBQy9CLE9BQUksRUFBRSxJQUFJO0dBQ1YsQ0FBQyxDQUFDO0VBQ0gsQ0FBQyxDQUFDOztBQUVILFFBQU8sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFVLElBQUksRUFBRTtBQUM5QyxLQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7QUFDdkIsT0FBSSxFQUFFLGFBaE1BLFdBQVcsQ0FnTUMsYUFBYTtBQUMvQixPQUFJLEVBQUUsSUFBSTtHQUNWLENBQUMsQ0FBQztFQUNILENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxJQUFJLEVBQUU7Ozs7O0VBSy9DLENBQUMsQ0FBQzs7QUFFSCxRQUFPLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUksRUFBRTs7RUFFMUMsQ0FBQyxDQUFDOztBQUVILFFBQU8sQ0FBQyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDOUMsS0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLE9BQUksRUFBRSxnQkFqTkEsY0FBYyxDQWlOQyxlQUFlO0FBQ3BDLE9BQUksRUFBRSxJQUFJO0dBQ1YsQ0FBQyxDQUFDO0VBQ0gsQ0FBQyxDQUFDOztBQUVILFFBQU8sQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDNUMsS0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLE9BQUksRUFBRSxnQkF4TkEsY0FBYyxDQXdOQyxhQUFhO0FBQ2xDLE9BQUksRUFBRSxJQUFJO0dBQ1YsQ0FBQyxDQUFDO0VBQ0gsQ0FBQzs7O0FBR0YsUUFBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ3RELEtBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztBQUN2QixPQUFJLEVBQUUsYUFqT0EsV0FBVyxDQWlPQyxpQkFBaUI7QUFDbkMsT0FBSSxFQUFFLElBQUk7R0FDVixDQUFDLENBQUM7RUFDSCxDQUFDLENBQUM7O0FBR0gsb0JBQW1CLEVBQUUsQ0FBQztBQUN0QixVQUFTLG1CQUFtQixHQUFHO0FBQzlCLE1BQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUN0QixNQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQzVCLEdBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDdkIsTUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ3JCLEtBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDO0FBQ2hDLEtBQUcsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQztBQUN2QyxLQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZO0FBQzNCLFNBQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO0dBQ3ZDLENBQUMsQ0FBQztBQUNILE1BQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDakIsS0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNqQixLQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztBQUM3QixLQUFHLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7QUFDMUMsS0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsWUFBWTtBQUMzQixTQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztHQUN0QyxDQUFDLENBQUM7QUFDSCxNQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2pCLEtBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDakIsS0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7QUFDNUIsS0FBRyxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0FBQ3RDLEtBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFlBQVk7QUFDM0IsU0FBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUMsTUFBTSxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7R0FDakQsQ0FBQyxDQUFDO0FBQ0gsTUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNqQixLQUFHLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ2pCLEtBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO0FBQy9CLEtBQUcsQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsQ0FBQztBQUMxQyxLQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZO0FBQzNCLFNBQU0sQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO0dBQzFDLENBQUMsQ0FBQztBQUNILE1BQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDakIsS0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNqQixLQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztBQUNoQyxLQUFHLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7QUFDMUMsS0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsWUFBWTtBQUMzQixTQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztHQUM5RCxDQUFDLENBQUM7QUFDSCxNQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2pCLEtBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDakIsS0FBRyxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0FBQ3RDLEtBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFlBQVk7QUFDM0IsU0FBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7R0FDdEQsQ0FBQyxDQUFDO0FBQ0gsTUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNqQixLQUFHLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ2pCLEtBQUcsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQztBQUNyQyxLQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZO0FBQzFCLE9BQUksT0FBTyxHQUFHO0FBQ2IsYUFBUyxFQUFFO0FBQ04sU0FBSSxFQUFFLElBQUk7QUFDVixPQUFFLEVBQUUsSUFBSTtBQUNSLGNBQVMsRUFBRSxHQUFHO0tBQ2Q7QUFDRCxPQUFHLEVBQUUsS0FBSztJQUNkO0FBQ0YsU0FBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0dBQzdDLENBQUMsQ0FBQztBQUNILE1BQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDakIsS0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNqQixLQUFHLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7QUFDbkMsS0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsWUFBWTtBQUMzQixTQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztHQUMxQyxDQUFDLENBQUM7QUFDSCxNQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0VBQ2pCOztBQUVELFVBQVMsU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUMxQixNQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUMvQixPQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7QUFDcEQsT0FBSSxPQUFPLElBQUksQ0FBQyxDQUFDLEVBQUU7QUFDbEIsV0FBTyxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztBQUN0QyxRQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDbEQsUUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0FBQ2hELFdBQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQzNEO0dBQ0Q7QUFDRCxTQUFPLEVBQUUsQ0FBQztFQUNWIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRleHBvcnRzOiB7fSxcbiBcdFx0XHRpZDogbW9kdWxlSWQsXG4gXHRcdFx0bG9hZGVkOiBmYWxzZVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sb2FkZWQgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogd2VicGFjay9ib290c3RyYXAgZmRkYzA1ZGQ4NWM5MWEyZjU0YmRcbiAqKi8iLCIvLyBEZWJ1ZyBwYW5lbC5cclxuaW1wb3J0IERlYnVnUGFuZWwgZnJvbSAnLi9kZWJ1Zy9kZWJ1Z1BhbmVsLmpzJztcclxubGV0IGRlYmFnUGFuZWwgPSBuZXcgRGVidWdQYW5lbCh7XHJcblx0cm9vdEVsZW1lbnRJZDogJ2dhbWUtZmllbGQnXHJcbn0pO1xyXG5cclxuLy8gSW1wb3J0IFJlZHV4XHJcbmltcG9ydCB7Y3JlYXRlU3RvcmUsIGNvbWJpbmVSZWR1Y2Vyc30gZnJvbSAncmVkdXgnO1xyXG5cclxuLy8gSW1wb3J0IEFjdGlvbnNcclxuaW1wb3J0IHthcHBBY3Rpb25zfSBmcm9tICcuL2dhbWUvYWN0aW9ucy9hcHBBY3Rpb25zLmpzJztcclxuaW1wb3J0IHt1c2VyQWN0aW9uc30gZnJvbSAnLi9nYW1lL2FjdGlvbnMvdXNlckFjdGlvbnMuanMnO1xyXG5pbXBvcnQge2dhbWVBY3Rpb25zfSBmcm9tICcuL2dhbWUvYWN0aW9ucy9nYW1lQWN0aW9ucy5qcyc7XHJcbmltcG9ydCB7b3B0aW9uc0FjdGlvbnN9IGZyb20gJy4vZ2FtZS9hY3Rpb25zL29wdGlvbnNBY3Rpb25zLmpzJztcclxuXHJcbi8vIEltcG9ydCByZWR1Y2Vyc1xyXG5pbXBvcnQge2FwcH0gZnJvbSAnLi9nYW1lL3JlZHVjZXJzL2FwcC5qcyc7XHJcbmltcG9ydCB7dXNlcn0gZnJvbSAnLi9nYW1lL3JlZHVjZXJzL3VzZXIuanMnO1xyXG5pbXBvcnQge2dhbWV9IGZyb20gJy4vZ2FtZS9yZWR1Y2Vycy9nYW1lLmpzJztcclxuaW1wb3J0IHtvcHRpb25zfSBmcm9tICcuL2dhbWUvcmVkdWNlcnMvb3B0aW9ucy5qcyc7XHJcblxyXG4vLyBDcmVhdGVpbmcgU3RvcmVcclxud2luZG93LmFwcCA9IHtcclxuXHRnYW1lOiB7XHJcblx0XHRzdG9yZSA6IHt9XHJcblx0fVxyXG59O1xyXG5cclxud2luZG93LmFwcC5nYW1lLnN0b3JlID0gY3JlYXRlU3RvcmUoY29tYmluZVJlZHVjZXJzKHtcclxuICBcdFx0XHRcdGFwcCxcclxuICBcdFx0XHRcdHVzZXIsXHJcbiAgXHRcdFx0XHRnYW1lLFxyXG4gIFx0XHRcdFx0b3B0aW9uc1xyXG5cdFx0XHR9KVx0XHRcdFxyXG5cdFx0KVxyXG5cclxuXHJcblxyXG5cclxuXHJcbi8vIFN0YXRlIGNoYW5nZSAoY2FsbGJhY2s6IGxvZywgdXBkYXRlIHZpZXdzLCAuLi4pXHJcbmxldCB1bnN1YnNjcmliZSA9IHdpbmRvdy5hcHAuZ2FtZS5zdG9yZS5zdWJzY3JpYmUoKCkgPT4ge1xyXG5cdGlmIChkZWJhZ1BhbmVsKVx0ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2RlYnVnT3V0cHV0JykuaW5uZXJIVE1MID0gSlNPTi5zdHJpbmdpZnkod2luZG93LmFwcC5nYW1lLnN0b3JlLmdldFN0YXRlKCksIG51bGwsIDQpO1xyXG5cdGNvbnNvbGUubG9nKHdpbmRvdy5hcHAuZ2FtZS5zdG9yZS5nZXRTdGF0ZSgpKTtcclxufSk7XHJcblxyXG4vLyBEZW1vIHVzZWNhc2UgKGFycmF5IG9mIGFjdGlvbnMpXHJcbi8vIGxldCBkZW1vVXNlY2FzZSA9IHJlcXVpcmUoJy4vZGVtb1VzZWNhc2UuanMnKTtcclxuLy8gZGVtb1VzZWNhc2UoMTAwMCk7XHJcblxyXG5yZXF1aXJlKCcuL2luZGV4LmNzcycpO1xyXG5cclxuLy8gU3RvcCBsaXN0ZW5pbmcgY2hhbmdlcyBvZiBTdGF0ZVxyXG4vLyB1bnN1YnNjcmliZSgpO1xyXG5cclxuXHJcblxyXG5yZXF1aXJlKCcuL2xheW91dC5zY3NzJyk7XHJcbl9pc0d1ZXN0ID0gZmFsc2U7XHJcblxyXG53aW5kb3cuTG9naWNHYW1lLmluaXQoZnVuY3Rpb24oKSB7XHRcclxuXHRjb25zb2xlLmxvZygnTEcgaW5pdCcpO1xyXG5cdHJlcXVpcmUoJy4vY2xpZW50L2luaXQuanMnKTtcclxuXHJcbi8vXHRhcHAuZ2FtZS5zdG9yZS5kaXNwYXRjaCh7XHJcbi8vXHRcdHR5cGU6IGFwcEFjdGlvbnMuQVBQX0lOSVQsXHJcbi8vXHRcdGRhdGE6IF9jbGllbnRcclxuLy9cdH0pO1xyXG5cclxufSk7XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vZnJvbnRlbmQvc3JjL2luZGV4LmpzXG4gKiovIiwicmVxdWlyZSgnLi9kZWJ1Z1BhbmVsLmNzcycpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3Mge1xyXG5cdGNvbnN0cnVjdG9yIChvcHRpb25zKSB7XHJcbi8vXHRcdGFsZXJ0KCdkZWJ1ZyEhJyk7XHJcblx0XHR0aGlzLkRPTSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG5cdFx0dGhpcy5ET00uaWQgPSBcImRlYnVnUGFuZWxcIjtcclxuXHRcdHRoaXMuRE9NLmlubmVySFRNTCA9ICBgPGZvcm0gaWQ9XCJkZWJ1Z0lucHV0XCI+PHRleHRhcmVhIG5hbWU9XCJkZWJ1Z0lucHV0VGV4dGFyZWFcIiBpZD1cIlwiPlxyXG53aW5kb3cuX2NsaWVudC5nYW1lTWFuYWdlci5zZW5kVHVybih7XHJcblx0J25ld01vdmUnOiB7XHJcbiAgICBcdFx0ZnJvbTogJ2UyJyxcclxuICAgIFx0XHR0bzogJ2U0JyxcclxuICAgIFx0XHRwcm9tb3Rpb246ICdxJyxcclxuICAgIFx0fVxyXG59KTtcclxuPC90ZXh0YXJlYT48YnV0dG9uIGlkPVwiZGVidWdJbnB1dEJ1dHRvblwiPkFwcGx5PC9idXR0b24+PC9mb3JtPjxkaXYgaWQ9XCJkZWJ1Z091dHB1dFwiPkRFQlVHIE9VVFBVVDwvZGl2PmA7XHJcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZChvcHRpb25zLnJvb3RFbGVtZW50SWQpLmFwcGVuZENoaWxkKHRoaXMuRE9NKTtcclxuXHJcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZGVidWdJbnB1dCcpLmFkZEV2ZW50TGlzdGVuZXIoJ3N1Ym1pdCcsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICBcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBcdFx0XHRldmFsKGUudGFyZ2V0LmVsZW1lbnRzWydkZWJ1Z0lucHV0VGV4dGFyZWEnXS52YWx1ZSk7XHJcblx0XHR9KTtcclxuXHR9XHJcbn07XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vZnJvbnRlbmQvc3JjL2RlYnVnL2RlYnVnUGFuZWwuanNcbiAqKi8iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi4vZnJvbnRlbmQvc3JjL2RlYnVnL2RlYnVnUGFuZWwuY3NzXG4gKiogbW9kdWxlIGlkID0gMlxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyAnZGVmYXVsdCc6IG9iaiB9OyB9XG5cbnZhciBfY3JlYXRlU3RvcmUgPSByZXF1aXJlKCcuL2NyZWF0ZVN0b3JlJyk7XG5cbnZhciBfY3JlYXRlU3RvcmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlU3RvcmUpO1xuXG52YXIgX3V0aWxzQ29tYmluZVJlZHVjZXJzID0gcmVxdWlyZSgnLi91dGlscy9jb21iaW5lUmVkdWNlcnMnKTtcblxudmFyIF91dGlsc0NvbWJpbmVSZWR1Y2VyczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF91dGlsc0NvbWJpbmVSZWR1Y2Vycyk7XG5cbnZhciBfdXRpbHNCaW5kQWN0aW9uQ3JlYXRvcnMgPSByZXF1aXJlKCcuL3V0aWxzL2JpbmRBY3Rpb25DcmVhdG9ycycpO1xuXG52YXIgX3V0aWxzQmluZEFjdGlvbkNyZWF0b3JzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3V0aWxzQmluZEFjdGlvbkNyZWF0b3JzKTtcblxudmFyIF91dGlsc0FwcGx5TWlkZGxld2FyZSA9IHJlcXVpcmUoJy4vdXRpbHMvYXBwbHlNaWRkbGV3YXJlJyk7XG5cbnZhciBfdXRpbHNBcHBseU1pZGRsZXdhcmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdXRpbHNBcHBseU1pZGRsZXdhcmUpO1xuXG52YXIgX3V0aWxzQ29tcG9zZSA9IHJlcXVpcmUoJy4vdXRpbHMvY29tcG9zZScpO1xuXG52YXIgX3V0aWxzQ29tcG9zZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF91dGlsc0NvbXBvc2UpO1xuXG5leHBvcnRzLmNyZWF0ZVN0b3JlID0gX2NyZWF0ZVN0b3JlMlsnZGVmYXVsdCddO1xuZXhwb3J0cy5jb21iaW5lUmVkdWNlcnMgPSBfdXRpbHNDb21iaW5lUmVkdWNlcnMyWydkZWZhdWx0J107XG5leHBvcnRzLmJpbmRBY3Rpb25DcmVhdG9ycyA9IF91dGlsc0JpbmRBY3Rpb25DcmVhdG9yczJbJ2RlZmF1bHQnXTtcbmV4cG9ydHMuYXBwbHlNaWRkbGV3YXJlID0gX3V0aWxzQXBwbHlNaWRkbGV3YXJlMlsnZGVmYXVsdCddO1xuZXhwb3J0cy5jb21wb3NlID0gX3V0aWxzQ29tcG9zZTJbJ2RlZmF1bHQnXTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi9pbmRleC5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHNbJ2RlZmF1bHQnXSA9IGNyZWF0ZVN0b3JlO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyAnZGVmYXVsdCc6IG9iaiB9OyB9XG5cbnZhciBfdXRpbHNJc1BsYWluT2JqZWN0ID0gcmVxdWlyZSgnLi91dGlscy9pc1BsYWluT2JqZWN0Jyk7XG5cbnZhciBfdXRpbHNJc1BsYWluT2JqZWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3V0aWxzSXNQbGFpbk9iamVjdCk7XG5cbi8qKlxuICogVGhlc2UgYXJlIHByaXZhdGUgYWN0aW9uIHR5cGVzIHJlc2VydmVkIGJ5IFJlZHV4LlxuICogRm9yIGFueSB1bmtub3duIGFjdGlvbnMsIHlvdSBtdXN0IHJldHVybiB0aGUgY3VycmVudCBzdGF0ZS5cbiAqIElmIHRoZSBjdXJyZW50IHN0YXRlIGlzIHVuZGVmaW5lZCwgeW91IG11c3QgcmV0dXJuIHRoZSBpbml0aWFsIHN0YXRlLlxuICogRG8gbm90IHJlZmVyZW5jZSB0aGVzZSBhY3Rpb24gdHlwZXMgZGlyZWN0bHkgaW4geW91ciBjb2RlLlxuICovXG52YXIgQWN0aW9uVHlwZXMgPSB7XG4gIElOSVQ6ICdAQHJlZHV4L0lOSVQnXG59O1xuXG5leHBvcnRzLkFjdGlvblR5cGVzID0gQWN0aW9uVHlwZXM7XG4vKipcbiAqIENyZWF0ZXMgYSBSZWR1eCBzdG9yZSB0aGF0IGhvbGRzIHRoZSBzdGF0ZSB0cmVlLlxuICogVGhlIG9ubHkgd2F5IHRvIGNoYW5nZSB0aGUgZGF0YSBpbiB0aGUgc3RvcmUgaXMgdG8gY2FsbCBgZGlzcGF0Y2goKWAgb24gaXQuXG4gKlxuICogVGhlcmUgc2hvdWxkIG9ubHkgYmUgYSBzaW5nbGUgc3RvcmUgaW4geW91ciBhcHAuIFRvIHNwZWNpZnkgaG93IGRpZmZlcmVudFxuICogcGFydHMgb2YgdGhlIHN0YXRlIHRyZWUgcmVzcG9uZCB0byBhY3Rpb25zLCB5b3UgbWF5IGNvbWJpbmUgc2V2ZXJhbCByZWR1Y2Vyc1xuICogaW50byBhIHNpbmdsZSByZWR1Y2VyIGZ1bmN0aW9uIGJ5IHVzaW5nIGBjb21iaW5lUmVkdWNlcnNgLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlZHVjZXIgQSBmdW5jdGlvbiB0aGF0IHJldHVybnMgdGhlIG5leHQgc3RhdGUgdHJlZSwgZ2l2ZW5cbiAqIHRoZSBjdXJyZW50IHN0YXRlIHRyZWUgYW5kIHRoZSBhY3Rpb24gdG8gaGFuZGxlLlxuICpcbiAqIEBwYXJhbSB7YW55fSBbaW5pdGlhbFN0YXRlXSBUaGUgaW5pdGlhbCBzdGF0ZS4gWW91IG1heSBvcHRpb25hbGx5IHNwZWNpZnkgaXRcbiAqIHRvIGh5ZHJhdGUgdGhlIHN0YXRlIGZyb20gdGhlIHNlcnZlciBpbiB1bml2ZXJzYWwgYXBwcywgb3IgdG8gcmVzdG9yZSBhXG4gKiBwcmV2aW91c2x5IHNlcmlhbGl6ZWQgdXNlciBzZXNzaW9uLlxuICogSWYgeW91IHVzZSBgY29tYmluZVJlZHVjZXJzYCB0byBwcm9kdWNlIHRoZSByb290IHJlZHVjZXIgZnVuY3Rpb24sIHRoaXMgbXVzdCBiZVxuICogYW4gb2JqZWN0IHdpdGggdGhlIHNhbWUgc2hhcGUgYXMgYGNvbWJpbmVSZWR1Y2Vyc2Aga2V5cy5cbiAqXG4gKiBAcmV0dXJucyB7U3RvcmV9IEEgUmVkdXggc3RvcmUgdGhhdCBsZXRzIHlvdSByZWFkIHRoZSBzdGF0ZSwgZGlzcGF0Y2ggYWN0aW9uc1xuICogYW5kIHN1YnNjcmliZSB0byBjaGFuZ2VzLlxuICovXG5cbmZ1bmN0aW9uIGNyZWF0ZVN0b3JlKHJlZHVjZXIsIGluaXRpYWxTdGF0ZSkge1xuICBpZiAodHlwZW9mIHJlZHVjZXIgIT09ICdmdW5jdGlvbicpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0V4cGVjdGVkIHRoZSByZWR1Y2VyIHRvIGJlIGEgZnVuY3Rpb24uJyk7XG4gIH1cblxuICB2YXIgY3VycmVudFJlZHVjZXIgPSByZWR1Y2VyO1xuICB2YXIgY3VycmVudFN0YXRlID0gaW5pdGlhbFN0YXRlO1xuICB2YXIgbGlzdGVuZXJzID0gW107XG4gIHZhciBpc0Rpc3BhdGNoaW5nID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIFJlYWRzIHRoZSBzdGF0ZSB0cmVlIG1hbmFnZWQgYnkgdGhlIHN0b3JlLlxuICAgKlxuICAgKiBAcmV0dXJucyB7YW55fSBUaGUgY3VycmVudCBzdGF0ZSB0cmVlIG9mIHlvdXIgYXBwbGljYXRpb24uXG4gICAqL1xuICBmdW5jdGlvbiBnZXRTdGF0ZSgpIHtcbiAgICByZXR1cm4gY3VycmVudFN0YXRlO1xuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgYSBjaGFuZ2UgbGlzdGVuZXIuIEl0IHdpbGwgYmUgY2FsbGVkIGFueSB0aW1lIGFuIGFjdGlvbiBpcyBkaXNwYXRjaGVkLFxuICAgKiBhbmQgc29tZSBwYXJ0IG9mIHRoZSBzdGF0ZSB0cmVlIG1heSBwb3RlbnRpYWxseSBoYXZlIGNoYW5nZWQuIFlvdSBtYXkgdGhlblxuICAgKiBjYWxsIGBnZXRTdGF0ZSgpYCB0byByZWFkIHRoZSBjdXJyZW50IHN0YXRlIHRyZWUgaW5zaWRlIHRoZSBjYWxsYmFjay5cbiAgICpcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gbGlzdGVuZXIgQSBjYWxsYmFjayB0byBiZSBpbnZva2VkIG9uIGV2ZXJ5IGRpc3BhdGNoLlxuICAgKiBAcmV0dXJucyB7RnVuY3Rpb259IEEgZnVuY3Rpb24gdG8gcmVtb3ZlIHRoaXMgY2hhbmdlIGxpc3RlbmVyLlxuICAgKi9cbiAgZnVuY3Rpb24gc3Vic2NyaWJlKGxpc3RlbmVyKSB7XG4gICAgbGlzdGVuZXJzLnB1c2gobGlzdGVuZXIpO1xuICAgIHZhciBpc1N1YnNjcmliZWQgPSB0cnVlO1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIHVuc3Vic2NyaWJlKCkge1xuICAgICAgaWYgKCFpc1N1YnNjcmliZWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpc1N1YnNjcmliZWQgPSBmYWxzZTtcbiAgICAgIHZhciBpbmRleCA9IGxpc3RlbmVycy5pbmRleE9mKGxpc3RlbmVyKTtcbiAgICAgIGxpc3RlbmVycy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogRGlzcGF0Y2hlcyBhbiBhY3Rpb24uIEl0IGlzIHRoZSBvbmx5IHdheSB0byB0cmlnZ2VyIGEgc3RhdGUgY2hhbmdlLlxuICAgKlxuICAgKiBUaGUgYHJlZHVjZXJgIGZ1bmN0aW9uLCB1c2VkIHRvIGNyZWF0ZSB0aGUgc3RvcmUsIHdpbGwgYmUgY2FsbGVkIHdpdGggdGhlXG4gICAqIGN1cnJlbnQgc3RhdGUgdHJlZSBhbmQgdGhlIGdpdmVuIGBhY3Rpb25gLiBJdHMgcmV0dXJuIHZhbHVlIHdpbGxcbiAgICogYmUgY29uc2lkZXJlZCB0aGUgKipuZXh0Kiogc3RhdGUgb2YgdGhlIHRyZWUsIGFuZCB0aGUgY2hhbmdlIGxpc3RlbmVyc1xuICAgKiB3aWxsIGJlIG5vdGlmaWVkLlxuICAgKlxuICAgKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvbmx5IHN1cHBvcnRzIHBsYWluIG9iamVjdCBhY3Rpb25zLiBJZiB5b3Ugd2FudCB0b1xuICAgKiBkaXNwYXRjaCBhIFByb21pc2UsIGFuIE9ic2VydmFibGUsIGEgdGh1bmssIG9yIHNvbWV0aGluZyBlbHNlLCB5b3UgbmVlZCB0b1xuICAgKiB3cmFwIHlvdXIgc3RvcmUgY3JlYXRpbmcgZnVuY3Rpb24gaW50byB0aGUgY29ycmVzcG9uZGluZyBtaWRkbGV3YXJlLiBGb3JcbiAgICogZXhhbXBsZSwgc2VlIHRoZSBkb2N1bWVudGF0aW9uIGZvciB0aGUgYHJlZHV4LXRodW5rYCBwYWNrYWdlLiBFdmVuIHRoZVxuICAgKiBtaWRkbGV3YXJlIHdpbGwgZXZlbnR1YWxseSBkaXNwYXRjaCBwbGFpbiBvYmplY3QgYWN0aW9ucyB1c2luZyB0aGlzIG1ldGhvZC5cbiAgICpcbiAgICogQHBhcmFtIHtPYmplY3R9IGFjdGlvbiBBIHBsYWluIG9iamVjdCByZXByZXNlbnRpbmcg4oCcd2hhdCBjaGFuZ2Vk4oCdLiBJdCBpc1xuICAgKiBhIGdvb2QgaWRlYSB0byBrZWVwIGFjdGlvbnMgc2VyaWFsaXphYmxlIHNvIHlvdSBjYW4gcmVjb3JkIGFuZCByZXBsYXkgdXNlclxuICAgKiBzZXNzaW9ucywgb3IgdXNlIHRoZSB0aW1lIHRyYXZlbGxpbmcgYHJlZHV4LWRldnRvb2xzYC4gQW4gYWN0aW9uIG11c3QgaGF2ZVxuICAgKiBhIGB0eXBlYCBwcm9wZXJ0eSB3aGljaCBtYXkgbm90IGJlIGB1bmRlZmluZWRgLiBJdCBpcyBhIGdvb2QgaWRlYSB0byB1c2VcbiAgICogc3RyaW5nIGNvbnN0YW50cyBmb3IgYWN0aW9uIHR5cGVzLlxuICAgKlxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBGb3IgY29udmVuaWVuY2UsIHRoZSBzYW1lIGFjdGlvbiBvYmplY3QgeW91IGRpc3BhdGNoZWQuXG4gICAqXG4gICAqIE5vdGUgdGhhdCwgaWYgeW91IHVzZSBhIGN1c3RvbSBtaWRkbGV3YXJlLCBpdCBtYXkgd3JhcCBgZGlzcGF0Y2goKWAgdG9cbiAgICogcmV0dXJuIHNvbWV0aGluZyBlbHNlIChmb3IgZXhhbXBsZSwgYSBQcm9taXNlIHlvdSBjYW4gYXdhaXQpLlxuICAgKi9cbiAgZnVuY3Rpb24gZGlzcGF0Y2goYWN0aW9uKSB7XG4gICAgaWYgKCFfdXRpbHNJc1BsYWluT2JqZWN0MlsnZGVmYXVsdCddKGFjdGlvbikpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignQWN0aW9ucyBtdXN0IGJlIHBsYWluIG9iamVjdHMuICcgKyAnVXNlIGN1c3RvbSBtaWRkbGV3YXJlIGZvciBhc3luYyBhY3Rpb25zLicpO1xuICAgIH1cblxuICAgIGlmICh0eXBlb2YgYWN0aW9uLnR5cGUgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0FjdGlvbnMgbWF5IG5vdCBoYXZlIGFuIHVuZGVmaW5lZCBcInR5cGVcIiBwcm9wZXJ0eS4gJyArICdIYXZlIHlvdSBtaXNzcGVsbGVkIGEgY29uc3RhbnQ/Jyk7XG4gICAgfVxuXG4gICAgaWYgKGlzRGlzcGF0Y2hpbmcpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignUmVkdWNlcnMgbWF5IG5vdCBkaXNwYXRjaCBhY3Rpb25zLicpO1xuICAgIH1cblxuICAgIHRyeSB7XG4gICAgICBpc0Rpc3BhdGNoaW5nID0gdHJ1ZTtcbiAgICAgIGN1cnJlbnRTdGF0ZSA9IGN1cnJlbnRSZWR1Y2VyKGN1cnJlbnRTdGF0ZSwgYWN0aW9uKTtcbiAgICB9IGZpbmFsbHkge1xuICAgICAgaXNEaXNwYXRjaGluZyA9IGZhbHNlO1xuICAgIH1cblxuICAgIGxpc3RlbmVycy5zbGljZSgpLmZvckVhY2goZnVuY3Rpb24gKGxpc3RlbmVyKSB7XG4gICAgICByZXR1cm4gbGlzdGVuZXIoKTtcbiAgICB9KTtcbiAgICByZXR1cm4gYWN0aW9uO1xuICB9XG5cbiAgLyoqXG4gICAqIFJlcGxhY2VzIHRoZSByZWR1Y2VyIGN1cnJlbnRseSB1c2VkIGJ5IHRoZSBzdG9yZSB0byBjYWxjdWxhdGUgdGhlIHN0YXRlLlxuICAgKlxuICAgKiBZb3UgbWlnaHQgbmVlZCB0aGlzIGlmIHlvdXIgYXBwIGltcGxlbWVudHMgY29kZSBzcGxpdHRpbmcgYW5kIHlvdSB3YW50IHRvXG4gICAqIGxvYWQgc29tZSBvZiB0aGUgcmVkdWNlcnMgZHluYW1pY2FsbHkuIFlvdSBtaWdodCBhbHNvIG5lZWQgdGhpcyBpZiB5b3VcbiAgICogaW1wbGVtZW50IGEgaG90IHJlbG9hZGluZyBtZWNoYW5pc20gZm9yIFJlZHV4LlxuICAgKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBuZXh0UmVkdWNlciBUaGUgcmVkdWNlciBmb3IgdGhlIHN0b3JlIHRvIHVzZSBpbnN0ZWFkLlxuICAgKiBAcmV0dXJucyB7dm9pZH1cbiAgICovXG4gIGZ1bmN0aW9uIHJlcGxhY2VSZWR1Y2VyKG5leHRSZWR1Y2VyKSB7XG4gICAgY3VycmVudFJlZHVjZXIgPSBuZXh0UmVkdWNlcjtcbiAgICBkaXNwYXRjaCh7IHR5cGU6IEFjdGlvblR5cGVzLklOSVQgfSk7XG4gIH1cblxuICAvLyBXaGVuIGEgc3RvcmUgaXMgY3JlYXRlZCwgYW4gXCJJTklUXCIgYWN0aW9uIGlzIGRpc3BhdGNoZWQgc28gdGhhdCBldmVyeVxuICAvLyByZWR1Y2VyIHJldHVybnMgdGhlaXIgaW5pdGlhbCBzdGF0ZS4gVGhpcyBlZmZlY3RpdmVseSBwb3B1bGF0ZXNcbiAgLy8gdGhlIGluaXRpYWwgc3RhdGUgdHJlZS5cbiAgZGlzcGF0Y2goeyB0eXBlOiBBY3Rpb25UeXBlcy5JTklUIH0pO1xuXG4gIHJldHVybiB7XG4gICAgZGlzcGF0Y2g6IGRpc3BhdGNoLFxuICAgIHN1YnNjcmliZTogc3Vic2NyaWJlLFxuICAgIGdldFN0YXRlOiBnZXRTdGF0ZSxcbiAgICByZXBsYWNlUmVkdWNlcjogcmVwbGFjZVJlZHVjZXJcbiAgfTtcbn1cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi9jcmVhdGVTdG9yZS5qc1xuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHNbJ2RlZmF1bHQnXSA9IGlzUGxhaW5PYmplY3Q7XG52YXIgZm5Ub1N0cmluZyA9IGZ1bmN0aW9uIGZuVG9TdHJpbmcoZm4pIHtcbiAgcmV0dXJuIEZ1bmN0aW9uLnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGZuKTtcbn07XG52YXIgb2JqU3RyaW5nVmFsdWUgPSBmblRvU3RyaW5nKE9iamVjdCk7XG5cbi8qKlxuICogQHBhcmFtIHthbnl9IG9iaiBUaGUgb2JqZWN0IHRvIGluc3BlY3QuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGUgYXJndW1lbnQgYXBwZWFycyB0byBiZSBhIHBsYWluIG9iamVjdC5cbiAqL1xuXG5mdW5jdGlvbiBpc1BsYWluT2JqZWN0KG9iaikge1xuICBpZiAoIW9iaiB8fCB0eXBlb2Ygb2JqICE9PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHZhciBwcm90byA9IHR5cGVvZiBvYmouY29uc3RydWN0b3IgPT09ICdmdW5jdGlvbicgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqKSA6IE9iamVjdC5wcm90b3R5cGU7XG5cbiAgaWYgKHByb3RvID09PSBudWxsKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICB2YXIgY29uc3RydWN0b3IgPSBwcm90by5jb25zdHJ1Y3RvcjtcblxuICByZXR1cm4gdHlwZW9mIGNvbnN0cnVjdG9yID09PSAnZnVuY3Rpb24nICYmIGNvbnN0cnVjdG9yIGluc3RhbmNlb2YgY29uc3RydWN0b3IgJiYgZm5Ub1N0cmluZyhjb25zdHJ1Y3RvcikgPT09IG9ialN0cmluZ1ZhbHVlO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi91dGlscy9pc1BsYWluT2JqZWN0LmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0c1snZGVmYXVsdCddID0gY29tYmluZVJlZHVjZXJzO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyAnZGVmYXVsdCc6IG9iaiB9OyB9XG5cbnZhciBfY3JlYXRlU3RvcmUgPSByZXF1aXJlKCcuLi9jcmVhdGVTdG9yZScpO1xuXG52YXIgX2lzUGxhaW5PYmplY3QgPSByZXF1aXJlKCcuL2lzUGxhaW5PYmplY3QnKTtcblxudmFyIF9pc1BsYWluT2JqZWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUGxhaW5PYmplY3QpO1xuXG52YXIgX21hcFZhbHVlcyA9IHJlcXVpcmUoJy4vbWFwVmFsdWVzJyk7XG5cbnZhciBfbWFwVmFsdWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX21hcFZhbHVlcyk7XG5cbnZhciBfcGljayA9IHJlcXVpcmUoJy4vcGljaycpO1xuXG52YXIgX3BpY2syID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcGljayk7XG5cbi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnNvbGUgKi9cblxuZnVuY3Rpb24gZ2V0VW5kZWZpbmVkU3RhdGVFcnJvck1lc3NhZ2Uoa2V5LCBhY3Rpb24pIHtcbiAgdmFyIGFjdGlvblR5cGUgPSBhY3Rpb24gJiYgYWN0aW9uLnR5cGU7XG4gIHZhciBhY3Rpb25OYW1lID0gYWN0aW9uVHlwZSAmJiAnXCInICsgYWN0aW9uVHlwZS50b1N0cmluZygpICsgJ1wiJyB8fCAnYW4gYWN0aW9uJztcblxuICByZXR1cm4gJ1JlZHVjZXIgXCInICsga2V5ICsgJ1wiIHJldHVybmVkIHVuZGVmaW5lZCBoYW5kbGluZyAnICsgYWN0aW9uTmFtZSArICcuICcgKyAnVG8gaWdub3JlIGFuIGFjdGlvbiwgeW91IG11c3QgZXhwbGljaXRseSByZXR1cm4gdGhlIHByZXZpb3VzIHN0YXRlLic7XG59XG5cbmZ1bmN0aW9uIGdldFVuZXhwZWN0ZWRTdGF0ZUtleVdhcm5pbmdNZXNzYWdlKGlucHV0U3RhdGUsIG91dHB1dFN0YXRlLCBhY3Rpb24pIHtcbiAgdmFyIHJlZHVjZXJLZXlzID0gT2JqZWN0LmtleXMob3V0cHV0U3RhdGUpO1xuICB2YXIgYXJndW1lbnROYW1lID0gYWN0aW9uICYmIGFjdGlvbi50eXBlID09PSBfY3JlYXRlU3RvcmUuQWN0aW9uVHlwZXMuSU5JVCA/ICdpbml0aWFsU3RhdGUgYXJndW1lbnQgcGFzc2VkIHRvIGNyZWF0ZVN0b3JlJyA6ICdwcmV2aW91cyBzdGF0ZSByZWNlaXZlZCBieSB0aGUgcmVkdWNlcic7XG5cbiAgaWYgKHJlZHVjZXJLZXlzLmxlbmd0aCA9PT0gMCkge1xuICAgIHJldHVybiAnU3RvcmUgZG9lcyBub3QgaGF2ZSBhIHZhbGlkIHJlZHVjZXIuIE1ha2Ugc3VyZSB0aGUgYXJndW1lbnQgcGFzc2VkICcgKyAndG8gY29tYmluZVJlZHVjZXJzIGlzIGFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIHJlZHVjZXJzLic7XG4gIH1cblxuICBpZiAoIV9pc1BsYWluT2JqZWN0MlsnZGVmYXVsdCddKGlucHV0U3RhdGUpKSB7XG4gICAgcmV0dXJuICdUaGUgJyArIGFyZ3VtZW50TmFtZSArICcgaGFzIHVuZXhwZWN0ZWQgdHlwZSBvZiBcIicgKyAoe30pLnRvU3RyaW5nLmNhbGwoaW5wdXRTdGF0ZSkubWF0Y2goL1xccyhbYS16fEEtWl0rKS8pWzFdICsgJ1wiLiBFeHBlY3RlZCBhcmd1bWVudCB0byBiZSBhbiBvYmplY3Qgd2l0aCB0aGUgZm9sbG93aW5nICcgKyAoJ2tleXM6IFwiJyArIHJlZHVjZXJLZXlzLmpvaW4oJ1wiLCBcIicpICsgJ1wiJyk7XG4gIH1cblxuICB2YXIgdW5leHBlY3RlZEtleXMgPSBPYmplY3Qua2V5cyhpbnB1dFN0YXRlKS5maWx0ZXIoZnVuY3Rpb24gKGtleSkge1xuICAgIHJldHVybiByZWR1Y2VyS2V5cy5pbmRleE9mKGtleSkgPCAwO1xuICB9KTtcblxuICBpZiAodW5leHBlY3RlZEtleXMubGVuZ3RoID4gMCkge1xuICAgIHJldHVybiAnVW5leHBlY3RlZCAnICsgKHVuZXhwZWN0ZWRLZXlzLmxlbmd0aCA+IDEgPyAna2V5cycgOiAna2V5JykgKyAnICcgKyAoJ1wiJyArIHVuZXhwZWN0ZWRLZXlzLmpvaW4oJ1wiLCBcIicpICsgJ1wiIGZvdW5kIGluICcgKyBhcmd1bWVudE5hbWUgKyAnLiAnKSArICdFeHBlY3RlZCB0byBmaW5kIG9uZSBvZiB0aGUga25vd24gcmVkdWNlciBrZXlzIGluc3RlYWQ6ICcgKyAoJ1wiJyArIHJlZHVjZXJLZXlzLmpvaW4oJ1wiLCBcIicpICsgJ1wiLiBVbmV4cGVjdGVkIGtleXMgd2lsbCBiZSBpZ25vcmVkLicpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGFzc2VydFJlZHVjZXJTYW5pdHkocmVkdWNlcnMpIHtcbiAgT2JqZWN0LmtleXMocmVkdWNlcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIHZhciByZWR1Y2VyID0gcmVkdWNlcnNba2V5XTtcbiAgICB2YXIgaW5pdGlhbFN0YXRlID0gcmVkdWNlcih1bmRlZmluZWQsIHsgdHlwZTogX2NyZWF0ZVN0b3JlLkFjdGlvblR5cGVzLklOSVQgfSk7XG5cbiAgICBpZiAodHlwZW9mIGluaXRpYWxTdGF0ZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignUmVkdWNlciBcIicgKyBrZXkgKyAnXCIgcmV0dXJuZWQgdW5kZWZpbmVkIGR1cmluZyBpbml0aWFsaXphdGlvbi4gJyArICdJZiB0aGUgc3RhdGUgcGFzc2VkIHRvIHRoZSByZWR1Y2VyIGlzIHVuZGVmaW5lZCwgeW91IG11c3QgJyArICdleHBsaWNpdGx5IHJldHVybiB0aGUgaW5pdGlhbCBzdGF0ZS4gVGhlIGluaXRpYWwgc3RhdGUgbWF5ICcgKyAnbm90IGJlIHVuZGVmaW5lZC4nKTtcbiAgICB9XG5cbiAgICB2YXIgdHlwZSA9ICdAQHJlZHV4L1BST0JFX1VOS05PV05fQUNUSU9OXycgKyBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoNykuc3BsaXQoJycpLmpvaW4oJy4nKTtcbiAgICBpZiAodHlwZW9mIHJlZHVjZXIodW5kZWZpbmVkLCB7IHR5cGU6IHR5cGUgfSkgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JlZHVjZXIgXCInICsga2V5ICsgJ1wiIHJldHVybmVkIHVuZGVmaW5lZCB3aGVuIHByb2JlZCB3aXRoIGEgcmFuZG9tIHR5cGUuICcgKyAoJ0RvblxcJ3QgdHJ5IHRvIGhhbmRsZSAnICsgX2NyZWF0ZVN0b3JlLkFjdGlvblR5cGVzLklOSVQgKyAnIG9yIG90aGVyIGFjdGlvbnMgaW4gXCJyZWR1eC8qXCIgJykgKyAnbmFtZXNwYWNlLiBUaGV5IGFyZSBjb25zaWRlcmVkIHByaXZhdGUuIEluc3RlYWQsIHlvdSBtdXN0IHJldHVybiB0aGUgJyArICdjdXJyZW50IHN0YXRlIGZvciBhbnkgdW5rbm93biBhY3Rpb25zLCB1bmxlc3MgaXQgaXMgdW5kZWZpbmVkLCAnICsgJ2luIHdoaWNoIGNhc2UgeW91IG11c3QgcmV0dXJuIHRoZSBpbml0aWFsIHN0YXRlLCByZWdhcmRsZXNzIG9mIHRoZSAnICsgJ2FjdGlvbiB0eXBlLiBUaGUgaW5pdGlhbCBzdGF0ZSBtYXkgbm90IGJlIHVuZGVmaW5lZC4nKTtcbiAgICB9XG4gIH0pO1xufVxuXG4vKipcbiAqIFR1cm5zIGFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIGRpZmZlcmVudCByZWR1Y2VyIGZ1bmN0aW9ucywgaW50byBhIHNpbmdsZVxuICogcmVkdWNlciBmdW5jdGlvbi4gSXQgd2lsbCBjYWxsIGV2ZXJ5IGNoaWxkIHJlZHVjZXIsIGFuZCBnYXRoZXIgdGhlaXIgcmVzdWx0c1xuICogaW50byBhIHNpbmdsZSBzdGF0ZSBvYmplY3QsIHdob3NlIGtleXMgY29ycmVzcG9uZCB0byB0aGUga2V5cyBvZiB0aGUgcGFzc2VkXG4gKiByZWR1Y2VyIGZ1bmN0aW9ucy5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcmVkdWNlcnMgQW4gb2JqZWN0IHdob3NlIHZhbHVlcyBjb3JyZXNwb25kIHRvIGRpZmZlcmVudFxuICogcmVkdWNlciBmdW5jdGlvbnMgdGhhdCBuZWVkIHRvIGJlIGNvbWJpbmVkIGludG8gb25lLiBPbmUgaGFuZHkgd2F5IHRvIG9idGFpblxuICogaXQgaXMgdG8gdXNlIEVTNiBgaW1wb3J0ICogYXMgcmVkdWNlcnNgIHN5bnRheC4gVGhlIHJlZHVjZXJzIG1heSBuZXZlciByZXR1cm5cbiAqIHVuZGVmaW5lZCBmb3IgYW55IGFjdGlvbi4gSW5zdGVhZCwgdGhleSBzaG91bGQgcmV0dXJuIHRoZWlyIGluaXRpYWwgc3RhdGVcbiAqIGlmIHRoZSBzdGF0ZSBwYXNzZWQgdG8gdGhlbSB3YXMgdW5kZWZpbmVkLCBhbmQgdGhlIGN1cnJlbnQgc3RhdGUgZm9yIGFueVxuICogdW5yZWNvZ25pemVkIGFjdGlvbi5cbiAqXG4gKiBAcmV0dXJucyB7RnVuY3Rpb259IEEgcmVkdWNlciBmdW5jdGlvbiB0aGF0IGludm9rZXMgZXZlcnkgcmVkdWNlciBpbnNpZGUgdGhlXG4gKiBwYXNzZWQgb2JqZWN0LCBhbmQgYnVpbGRzIGEgc3RhdGUgb2JqZWN0IHdpdGggdGhlIHNhbWUgc2hhcGUuXG4gKi9cblxuZnVuY3Rpb24gY29tYmluZVJlZHVjZXJzKHJlZHVjZXJzKSB7XG4gIHZhciBmaW5hbFJlZHVjZXJzID0gX3BpY2syWydkZWZhdWx0J10ocmVkdWNlcnMsIGZ1bmN0aW9uICh2YWwpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ2Z1bmN0aW9uJztcbiAgfSk7XG4gIHZhciBzYW5pdHlFcnJvcjtcblxuICB0cnkge1xuICAgIGFzc2VydFJlZHVjZXJTYW5pdHkoZmluYWxSZWR1Y2Vycyk7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICBzYW5pdHlFcnJvciA9IGU7XG4gIH1cblxuICB2YXIgZGVmYXVsdFN0YXRlID0gX21hcFZhbHVlczJbJ2RlZmF1bHQnXShmaW5hbFJlZHVjZXJzLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfSk7XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIGNvbWJpbmF0aW9uKHN0YXRlLCBhY3Rpb24pIHtcbiAgICBpZiAoc3RhdGUgPT09IHVuZGVmaW5lZCkgc3RhdGUgPSBkZWZhdWx0U3RhdGU7XG5cbiAgICBpZiAoc2FuaXR5RXJyb3IpIHtcbiAgICAgIHRocm93IHNhbml0eUVycm9yO1xuICAgIH1cblxuICAgIHZhciBoYXNDaGFuZ2VkID0gZmFsc2U7XG4gICAgdmFyIGZpbmFsU3RhdGUgPSBfbWFwVmFsdWVzMlsnZGVmYXVsdCddKGZpbmFsUmVkdWNlcnMsIGZ1bmN0aW9uIChyZWR1Y2VyLCBrZXkpIHtcbiAgICAgIHZhciBwcmV2aW91c1N0YXRlRm9yS2V5ID0gc3RhdGVba2V5XTtcbiAgICAgIHZhciBuZXh0U3RhdGVGb3JLZXkgPSByZWR1Y2VyKHByZXZpb3VzU3RhdGVGb3JLZXksIGFjdGlvbik7XG4gICAgICBpZiAodHlwZW9mIG5leHRTdGF0ZUZvcktleSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdmFyIGVycm9yTWVzc2FnZSA9IGdldFVuZGVmaW5lZFN0YXRlRXJyb3JNZXNzYWdlKGtleSwgYWN0aW9uKTtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGVycm9yTWVzc2FnZSk7XG4gICAgICB9XG4gICAgICBoYXNDaGFuZ2VkID0gaGFzQ2hhbmdlZCB8fCBuZXh0U3RhdGVGb3JLZXkgIT09IHByZXZpb3VzU3RhdGVGb3JLZXk7XG4gICAgICByZXR1cm4gbmV4dFN0YXRlRm9yS2V5O1xuICAgIH0pO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHZhciB3YXJuaW5nTWVzc2FnZSA9IGdldFVuZXhwZWN0ZWRTdGF0ZUtleVdhcm5pbmdNZXNzYWdlKHN0YXRlLCBmaW5hbFN0YXRlLCBhY3Rpb24pO1xuICAgICAgaWYgKHdhcm5pbmdNZXNzYWdlKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3Iod2FybmluZ01lc3NhZ2UpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBoYXNDaGFuZ2VkID8gZmluYWxTdGF0ZSA6IHN0YXRlO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi91dGlscy9jb21iaW5lUmVkdWNlcnMuanNcbiAqKi8iLCIvLyBzaGltIGZvciB1c2luZyBwcm9jZXNzIGluIGJyb3dzZXJcblxudmFyIHByb2Nlc3MgPSBtb2R1bGUuZXhwb3J0cyA9IHt9O1xudmFyIHF1ZXVlID0gW107XG52YXIgZHJhaW5pbmcgPSBmYWxzZTtcbnZhciBjdXJyZW50UXVldWU7XG52YXIgcXVldWVJbmRleCA9IC0xO1xuXG5mdW5jdGlvbiBjbGVhblVwTmV4dFRpY2soKSB7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBpZiAoY3VycmVudFF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBxdWV1ZSA9IGN1cnJlbnRRdWV1ZS5jb25jYXQocXVldWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICB9XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCkge1xuICAgICAgICBkcmFpblF1ZXVlKCk7XG4gICAgfVxufVxuXG5mdW5jdGlvbiBkcmFpblF1ZXVlKCkge1xuICAgIGlmIChkcmFpbmluZykge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhciB0aW1lb3V0ID0gc2V0VGltZW91dChjbGVhblVwTmV4dFRpY2spO1xuICAgIGRyYWluaW5nID0gdHJ1ZTtcblxuICAgIHZhciBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgd2hpbGUobGVuKSB7XG4gICAgICAgIGN1cnJlbnRRdWV1ZSA9IHF1ZXVlO1xuICAgICAgICBxdWV1ZSA9IFtdO1xuICAgICAgICB3aGlsZSAoKytxdWV1ZUluZGV4IDwgbGVuKSB7XG4gICAgICAgICAgICBpZiAoY3VycmVudFF1ZXVlKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudFF1ZXVlW3F1ZXVlSW5kZXhdLnJ1bigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcbiAgICAgICAgbGVuID0gcXVldWUubGVuZ3RoO1xuICAgIH1cbiAgICBjdXJyZW50UXVldWUgPSBudWxsO1xuICAgIGRyYWluaW5nID0gZmFsc2U7XG4gICAgY2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xufVxuXG5wcm9jZXNzLm5leHRUaWNrID0gZnVuY3Rpb24gKGZ1bikge1xuICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGggLSAxKTtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGFyZ3NbaSAtIDFdID0gYXJndW1lbnRzW2ldO1xuICAgICAgICB9XG4gICAgfVxuICAgIHF1ZXVlLnB1c2gobmV3IEl0ZW0oZnVuLCBhcmdzKSk7XG4gICAgaWYgKHF1ZXVlLmxlbmd0aCA9PT0gMSAmJiAhZHJhaW5pbmcpIHtcbiAgICAgICAgc2V0VGltZW91dChkcmFpblF1ZXVlLCAwKTtcbiAgICB9XG59O1xuXG4vLyB2OCBsaWtlcyBwcmVkaWN0aWJsZSBvYmplY3RzXG5mdW5jdGlvbiBJdGVtKGZ1biwgYXJyYXkpIHtcbiAgICB0aGlzLmZ1biA9IGZ1bjtcbiAgICB0aGlzLmFycmF5ID0gYXJyYXk7XG59XG5JdGVtLnByb3RvdHlwZS5ydW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5mdW4uYXBwbHkobnVsbCwgdGhpcy5hcnJheSk7XG59O1xucHJvY2Vzcy50aXRsZSA9ICdicm93c2VyJztcbnByb2Nlc3MuYnJvd3NlciA9IHRydWU7XG5wcm9jZXNzLmVudiA9IHt9O1xucHJvY2Vzcy5hcmd2ID0gW107XG5wcm9jZXNzLnZlcnNpb24gPSAnJzsgLy8gZW1wdHkgc3RyaW5nIHRvIGF2b2lkIHJlZ2V4cCBpc3N1ZXNcbnByb2Nlc3MudmVyc2lvbnMgPSB7fTtcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbnByb2Nlc3Mub24gPSBub29wO1xucHJvY2Vzcy5hZGRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLm9uY2UgPSBub29wO1xucHJvY2Vzcy5vZmYgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUFsbExpc3RlbmVycyA9IG5vb3A7XG5wcm9jZXNzLmVtaXQgPSBub29wO1xuXG5wcm9jZXNzLmJpbmRpbmcgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5cbnByb2Nlc3MuY3dkID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gJy8nIH07XG5wcm9jZXNzLmNoZGlyID0gZnVuY3Rpb24gKGRpcikge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5jaGRpciBpcyBub3Qgc3VwcG9ydGVkJyk7XG59O1xucHJvY2Vzcy51bWFzayA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gMDsgfTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcHJvY2Vzcy9icm93c2VyLmpzXG4gKiovIiwiLyoqXG4gKiBBcHBsaWVzIGEgZnVuY3Rpb24gdG8gZXZlcnkga2V5LXZhbHVlIHBhaXIgaW5zaWRlIGFuIG9iamVjdC5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqIFRoZSBzb3VyY2Ugb2JqZWN0LlxuICogQHBhcmFtIHtGdW5jdGlvbn0gZm4gVGhlIG1hcHBlciBmdW5jdGlvbiB0aGF0IHJlY2VpdmVzIHRoZSB2YWx1ZSBhbmQgdGhlIGtleS5cbiAqIEByZXR1cm5zIHtPYmplY3R9IEEgbmV3IG9iamVjdCB0aGF0IGNvbnRhaW5zIHRoZSBtYXBwZWQgdmFsdWVzIGZvciB0aGUga2V5cy5cbiAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IG1hcFZhbHVlcztcblxuZnVuY3Rpb24gbWFwVmFsdWVzKG9iaiwgZm4pIHtcbiAgcmV0dXJuIE9iamVjdC5rZXlzKG9iaikucmVkdWNlKGZ1bmN0aW9uIChyZXN1bHQsIGtleSkge1xuICAgIHJlc3VsdFtrZXldID0gZm4ob2JqW2tleV0sIGtleSk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfSwge30pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbXCJkZWZhdWx0XCJdO1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcmVkdXgvbGliL3V0aWxzL21hcFZhbHVlcy5qc1xuICoqLyIsIi8qKlxuICogUGlja3Mga2V5LXZhbHVlIHBhaXJzIGZyb20gYW4gb2JqZWN0IHdoZXJlIHZhbHVlcyBzYXRpc2Z5IGEgcHJlZGljYXRlLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmogVGhlIG9iamVjdCB0byBwaWNrIGZyb20uXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBUaGUgcHJlZGljYXRlIHRoZSB2YWx1ZXMgbXVzdCBzYXRpc2Z5IHRvIGJlIGNvcGllZC5cbiAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBvYmplY3Qgd2l0aCB0aGUgdmFsdWVzIHRoYXQgc2F0aXNmaWVkIHRoZSBwcmVkaWNhdGUuXG4gKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBwaWNrO1xuXG5mdW5jdGlvbiBwaWNrKG9iaiwgZm4pIHtcbiAgcmV0dXJuIE9iamVjdC5rZXlzKG9iaikucmVkdWNlKGZ1bmN0aW9uIChyZXN1bHQsIGtleSkge1xuICAgIGlmIChmbihvYmpba2V5XSkpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gb2JqW2tleV07XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH0sIHt9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzW1wiZGVmYXVsdFwiXTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi91dGlscy9waWNrLmpzXG4gKiovIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0c1snZGVmYXVsdCddID0gYmluZEFjdGlvbkNyZWF0b3JzO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyAnZGVmYXVsdCc6IG9iaiB9OyB9XG5cbnZhciBfbWFwVmFsdWVzID0gcmVxdWlyZSgnLi9tYXBWYWx1ZXMnKTtcblxudmFyIF9tYXBWYWx1ZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfbWFwVmFsdWVzKTtcblxuZnVuY3Rpb24gYmluZEFjdGlvbkNyZWF0b3IoYWN0aW9uQ3JlYXRvciwgZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZGlzcGF0Y2goYWN0aW9uQ3JlYXRvci5hcHBseSh1bmRlZmluZWQsIGFyZ3VtZW50cykpO1xuICB9O1xufVxuXG4vKipcbiAqIFR1cm5zIGFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIGFjdGlvbiBjcmVhdG9ycywgaW50byBhbiBvYmplY3Qgd2l0aCB0aGVcbiAqIHNhbWUga2V5cywgYnV0IHdpdGggZXZlcnkgZnVuY3Rpb24gd3JhcHBlZCBpbnRvIGEgYGRpc3BhdGNoYCBjYWxsIHNvIHRoZXlcbiAqIG1heSBiZSBpbnZva2VkIGRpcmVjdGx5LiBUaGlzIGlzIGp1c3QgYSBjb252ZW5pZW5jZSBtZXRob2QsIGFzIHlvdSBjYW4gY2FsbFxuICogYHN0b3JlLmRpc3BhdGNoKE15QWN0aW9uQ3JlYXRvcnMuZG9Tb21ldGhpbmcoKSlgIHlvdXJzZWxmIGp1c3QgZmluZS5cbiAqXG4gKiBGb3IgY29udmVuaWVuY2UsIHlvdSBjYW4gYWxzbyBwYXNzIGEgc2luZ2xlIGZ1bmN0aW9uIGFzIHRoZSBmaXJzdCBhcmd1bWVudCxcbiAqIGFuZCBnZXQgYSBmdW5jdGlvbiBpbiByZXR1cm4uXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbnxPYmplY3R9IGFjdGlvbkNyZWF0b3JzIEFuIG9iamVjdCB3aG9zZSB2YWx1ZXMgYXJlIGFjdGlvblxuICogY3JlYXRvciBmdW5jdGlvbnMuIE9uZSBoYW5keSB3YXkgdG8gb2J0YWluIGl0IGlzIHRvIHVzZSBFUzYgYGltcG9ydCAqIGFzYFxuICogc3ludGF4LiBZb3UgbWF5IGFsc28gcGFzcyBhIHNpbmdsZSBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBkaXNwYXRjaCBUaGUgYGRpc3BhdGNoYCBmdW5jdGlvbiBhdmFpbGFibGUgb24geW91ciBSZWR1eFxuICogc3RvcmUuXG4gKlxuICogQHJldHVybnMge0Z1bmN0aW9ufE9iamVjdH0gVGhlIG9iamVjdCBtaW1pY2tpbmcgdGhlIG9yaWdpbmFsIG9iamVjdCwgYnV0IHdpdGhcbiAqIGV2ZXJ5IGFjdGlvbiBjcmVhdG9yIHdyYXBwZWQgaW50byB0aGUgYGRpc3BhdGNoYCBjYWxsLiBJZiB5b3UgcGFzc2VkIGFcbiAqIGZ1bmN0aW9uIGFzIGBhY3Rpb25DcmVhdG9yc2AsIHRoZSByZXR1cm4gdmFsdWUgd2lsbCBhbHNvIGJlIGEgc2luZ2xlXG4gKiBmdW5jdGlvbi5cbiAqL1xuXG5mdW5jdGlvbiBiaW5kQWN0aW9uQ3JlYXRvcnMoYWN0aW9uQ3JlYXRvcnMsIGRpc3BhdGNoKSB7XG4gIGlmICh0eXBlb2YgYWN0aW9uQ3JlYXRvcnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICByZXR1cm4gYmluZEFjdGlvbkNyZWF0b3IoYWN0aW9uQ3JlYXRvcnMsIGRpc3BhdGNoKTtcbiAgfVxuXG4gIGlmICh0eXBlb2YgYWN0aW9uQ3JlYXRvcnMgIT09ICdvYmplY3QnIHx8IGFjdGlvbkNyZWF0b3JzID09PSBudWxsIHx8IGFjdGlvbkNyZWF0b3JzID09PSB1bmRlZmluZWQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ2JpbmRBY3Rpb25DcmVhdG9ycyBleHBlY3RlZCBhbiBvYmplY3Qgb3IgYSBmdW5jdGlvbiwgaW5zdGVhZCByZWNlaXZlZCAnICsgKGFjdGlvbkNyZWF0b3JzID09PSBudWxsID8gJ251bGwnIDogdHlwZW9mIGFjdGlvbkNyZWF0b3JzKSArICcuICcgKyAnRGlkIHlvdSB3cml0ZSBcImltcG9ydCBBY3Rpb25DcmVhdG9ycyBmcm9tXCIgaW5zdGVhZCBvZiBcImltcG9ydCAqIGFzIEFjdGlvbkNyZWF0b3JzIGZyb21cIj8nKTtcbiAgfVxuXG4gIHJldHVybiBfbWFwVmFsdWVzMlsnZGVmYXVsdCddKGFjdGlvbkNyZWF0b3JzLCBmdW5jdGlvbiAoYWN0aW9uQ3JlYXRvcikge1xuICAgIHJldHVybiBiaW5kQWN0aW9uQ3JlYXRvcihhY3Rpb25DcmVhdG9yLCBkaXNwYXRjaCk7XG4gIH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi91dGlscy9iaW5kQWN0aW9uQ3JlYXRvcnMuanNcbiAqKi8iLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbmV4cG9ydHNbJ2RlZmF1bHQnXSA9IGFwcGx5TWlkZGxld2FyZTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgJ2RlZmF1bHQnOiBvYmogfTsgfVxuXG52YXIgX2NvbXBvc2UgPSByZXF1aXJlKCcuL2NvbXBvc2UnKTtcblxudmFyIF9jb21wb3NlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NvbXBvc2UpO1xuXG4vKipcbiAqIENyZWF0ZXMgYSBzdG9yZSBlbmhhbmNlciB0aGF0IGFwcGxpZXMgbWlkZGxld2FyZSB0byB0aGUgZGlzcGF0Y2ggbWV0aG9kXG4gKiBvZiB0aGUgUmVkdXggc3RvcmUuIFRoaXMgaXMgaGFuZHkgZm9yIGEgdmFyaWV0eSBvZiB0YXNrcywgc3VjaCBhcyBleHByZXNzaW5nXG4gKiBhc3luY2hyb25vdXMgYWN0aW9ucyBpbiBhIGNvbmNpc2UgbWFubmVyLCBvciBsb2dnaW5nIGV2ZXJ5IGFjdGlvbiBwYXlsb2FkLlxuICpcbiAqIFNlZSBgcmVkdXgtdGh1bmtgIHBhY2thZ2UgYXMgYW4gZXhhbXBsZSBvZiB0aGUgUmVkdXggbWlkZGxld2FyZS5cbiAqXG4gKiBCZWNhdXNlIG1pZGRsZXdhcmUgaXMgcG90ZW50aWFsbHkgYXN5bmNocm9ub3VzLCB0aGlzIHNob3VsZCBiZSB0aGUgZmlyc3RcbiAqIHN0b3JlIGVuaGFuY2VyIGluIHRoZSBjb21wb3NpdGlvbiBjaGFpbi5cbiAqXG4gKiBOb3RlIHRoYXQgZWFjaCBtaWRkbGV3YXJlIHdpbGwgYmUgZ2l2ZW4gdGhlIGBkaXNwYXRjaGAgYW5kIGBnZXRTdGF0ZWAgZnVuY3Rpb25zXG4gKiBhcyBuYW1lZCBhcmd1bWVudHMuXG4gKlxuICogQHBhcmFtIHsuLi5GdW5jdGlvbn0gbWlkZGxld2FyZXMgVGhlIG1pZGRsZXdhcmUgY2hhaW4gdG8gYmUgYXBwbGllZC5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gQSBzdG9yZSBlbmhhbmNlciBhcHBseWluZyB0aGUgbWlkZGxld2FyZS5cbiAqL1xuXG5mdW5jdGlvbiBhcHBseU1pZGRsZXdhcmUoKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBtaWRkbGV3YXJlcyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgIG1pZGRsZXdhcmVzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChuZXh0KSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChyZWR1Y2VyLCBpbml0aWFsU3RhdGUpIHtcbiAgICAgIHZhciBzdG9yZSA9IG5leHQocmVkdWNlciwgaW5pdGlhbFN0YXRlKTtcbiAgICAgIHZhciBfZGlzcGF0Y2ggPSBzdG9yZS5kaXNwYXRjaDtcbiAgICAgIHZhciBjaGFpbiA9IFtdO1xuXG4gICAgICB2YXIgbWlkZGxld2FyZUFQSSA9IHtcbiAgICAgICAgZ2V0U3RhdGU6IHN0b3JlLmdldFN0YXRlLFxuICAgICAgICBkaXNwYXRjaDogZnVuY3Rpb24gZGlzcGF0Y2goYWN0aW9uKSB7XG4gICAgICAgICAgcmV0dXJuIF9kaXNwYXRjaChhY3Rpb24pO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgY2hhaW4gPSBtaWRkbGV3YXJlcy5tYXAoZnVuY3Rpb24gKG1pZGRsZXdhcmUpIHtcbiAgICAgICAgcmV0dXJuIG1pZGRsZXdhcmUobWlkZGxld2FyZUFQSSk7XG4gICAgICB9KTtcbiAgICAgIF9kaXNwYXRjaCA9IF9jb21wb3NlMlsnZGVmYXVsdCddLmFwcGx5KHVuZGVmaW5lZCwgY2hhaW4pKHN0b3JlLmRpc3BhdGNoKTtcblxuICAgICAgcmV0dXJuIF9leHRlbmRzKHt9LCBzdG9yZSwge1xuICAgICAgICBkaXNwYXRjaDogX2Rpc3BhdGNoXG4gICAgICB9KTtcbiAgICB9O1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9+L3JlZHV4L2xpYi91dGlscy9hcHBseU1pZGRsZXdhcmUuanNcbiAqKi8iLCIvKipcbiAqIENvbXBvc2VzIHNpbmdsZS1hcmd1bWVudCBmdW5jdGlvbnMgZnJvbSByaWdodCB0byBsZWZ0LlxuICpcbiAqIEBwYXJhbSB7Li4uRnVuY3Rpb259IGZ1bmNzIFRoZSBmdW5jdGlvbnMgdG8gY29tcG9zZS5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gQSBmdW5jdGlvbiBvYnRhaW5lZCBieSBjb21wb3NpbmcgZnVuY3Rpb25zIGZyb20gcmlnaHQgdG9cbiAqIGxlZnQuIEZvciBleGFtcGxlLCBjb21wb3NlKGYsIGcsIGgpIGlzIGlkZW50aWNhbCB0byBhcmcgPT4gZihnKGgoYXJnKSkpLlxuICovXG5cInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gY29tcG9zZTtcblxuZnVuY3Rpb24gY29tcG9zZSgpIHtcbiAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGZ1bmNzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgZnVuY3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKGFyZykge1xuICAgIHJldHVybiBmdW5jcy5yZWR1Y2VSaWdodChmdW5jdGlvbiAoY29tcG9zZWQsIGYpIHtcbiAgICAgIHJldHVybiBmKGNvbXBvc2VkKTtcbiAgICB9LCBhcmcpO1xuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbXCJkZWZhdWx0XCJdO1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vcmVkdXgvbGliL3V0aWxzL2NvbXBvc2UuanNcbiAqKi8iLCJleHBvcnQgY29uc3QgYXBwQWN0aW9ucyA9IHtcclxuXHRBUFBfSU5JVDogJ0FQUF9JTklUJyxcclxuXHRBUFBfU1RBVFVTOiAnQVBQX1NUQVRVUycsXHJcblx0QVBQX0RFQlVHOiAnQVBQX0RFQlVHJyxcclxuXHRBUFBfTE9HOiAnQVBQX0xPRycsXHJcblx0QVBQX0VSUk9SOiAnQVBQX0VSUk9SJyxcclxuXHRNT0RFX1NXSVRDSDogJ01PREVfU1dJVENIJ1xyXG59O1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL2Zyb250ZW5kL3NyYy9nYW1lL2FjdGlvbnMvYXBwQWN0aW9ucy5qc1xuICoqLyIsImV4cG9ydCBjb25zdCB1c2VyQWN0aW9ucyA9IHtcclxuXHRVU0VSX0xPR0lOOiAnVVNFUl9MT0dJTicsXHJcblx0VVNFUl9MT0dPVVQ6ICdVU0VSX0xPR0lOJyxcclxuXHRVU0VSX09OTElORTogJ1VTRVJfT0ZGTElORScsXHJcblx0VVNFUl9PRkZMSU5FOiAnVVNFUl9PRkZMSU5FJyxcclxuXHRVU0VSX1VQREFURTogJ1VTRVJfVVBEQVRFJyxcclxuXHRVU0VSX1RVUk46ICdVU0VSX1RVUk4nLFxyXG5cdFVTRVJfSElTVE9SWV9OQVZJR0FURTogJ1VTRVJfSElTVE9SWV9OQVZJR0FURScsXHJcblx0VVNFUl9BRE1JVF9ERUZFQVQ6ICdVU0VSX0FETUlUX0RFRkVBVCcsXHJcblx0VVNFUl9EUkFXX09GRkVSOiAnVVNFUl9EUkFXX09GRkVSJyxcclxuXHRVU0VSX0RSQVdfQUNDRVBUOiAnVVNFUl9EUkFXX0FDQ0VQVCcsXHJcblx0VVNFUl9UQUtFQkFDS19PRkZFUjogJ1VTRVJfVEFLRUJBQ0tfT0ZGRVInLFxyXG5cdFVTRVJfVEFLRUJBQ0tfQUNDRVBUOiAnVVNFUl9UQUtFQkFDS19BQ0NFUFQnLFxyXG5cdFVTRVJfRk9DVVM6ICdVU0VSX0ZPQ1VTJ1xyXG59O1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL2Zyb250ZW5kL3NyYy9nYW1lL2FjdGlvbnMvdXNlckFjdGlvbnMuanNcbiAqKi8iLCJleHBvcnQgY29uc3QgZ2FtZUFjdGlvbnMgPSB7XHJcblx0R0FNRV9TVEFSVDogJ0dBTUVfU1RBUlQnLFxyXG5cdFJPVU5EX1NUQVJUOiAnUk9VTkRfU1RBUlQnLFxyXG5cdEdBTUVfTE9BRDogJ0dBTUVfTE9BRCcsXHJcblx0R0FNRV9ISVNUT1JZX0xPQUQ6ICdHQU1FX0hJU1RPUllfTE9BRCcsXHJcblx0R0FNRV9UVVJOOiAnR0FNRV9UVVJOJyxcclxuXHRHQU1FX1RJTUVUSUNLOiAnR0FNRV9USU1FVElDSycsXHJcblx0R0FNRV9USU1FT1VUOiAnR0FNRV9USU1FT1VUJyxcclxuXHRTV0lUQ0hfUExBWUVSOiAnU1dJVENIX1BMQVlFUicsXHJcblx0Uk9VTkRfRU5EOiAnUk9VTkRfRU5EJyxcclxuXHRHQU1FX0xFQVZFOiAnR0FNRV9MRUFWRScsXHJcblx0R0FNRV9FVkVOVDogJ0dBTUVfRVZFTlQnLFxyXG5cdEdBTUVfVEFLRUJBQ0s6ICdHQU1FX1RBS0VCQUNLJyxcclxuXHRHQU1FX0VSUk9SOiAnR0FNRV9FUlJPUidcclxufTtcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuLi9mcm9udGVuZC9zcmMvZ2FtZS9hY3Rpb25zL2dhbWVBY3Rpb25zLmpzXG4gKiovIiwiZXhwb3J0IGNvbnN0IG9wdGlvbnNBY3Rpb25zID0ge1xyXG5cdE9QVElPTlNfQ0hBTkdFRDogJ09QVElPTlNfQ0hBTkdFRCcsXHJcblx0T1BUSU9OU19TQVZFRDogJ09QVElPTlNfU0FWRUQnLFxyXG5cdE9QVElPTlNfUkVTRVQ6ICdPUFRJT05TX1JFU0VUJ1xyXG59O1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL2Zyb250ZW5kL3NyYy9nYW1lL2FjdGlvbnMvb3B0aW9uc0FjdGlvbnMuanNcbiAqKi8iLCIvLyBJbXBvcnQgYWN0aW9uc1xyXG5pbXBvcnQge2FwcEFjdGlvbnN9IGZyb20gJy4vLi4vYWN0aW9ucy9hcHBBY3Rpb25zJztcclxuaW1wb3J0IEFwcENsYXNzIGZyb20gJy4vLi4vY2xhc3Nlcy9hcHAuY2xhc3MnO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGFwcCAoc3RhdGUgPSBudWxsICwgYWN0aW9uKSB7XHJcblx0c3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG5cclxuXHRcdGNhc2UgYXBwQWN0aW9ucy5BUFBfSU5JVDpcclxuXHRcdFx0cmV0dXJuIG5ldyBBcHBDbGFzcyAoc3RhdGUsIGFjdGlvbikuZ2V0U3RhdGUoKTtcclxuXHJcblx0XHRjYXNlIGFwcEFjdGlvbnMuTU9ERV9TV0lUQ0g6XHJcblx0XHRcdHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xyXG5cdFx0XHRcdGN1cnJlbnRNb2RlOiBhY3Rpb24uZGF0YVxyXG5cdFx0XHR9KTtcclxuXHRcdGNhc2UgYXBwQWN0aW9ucy5BUFBfU1RBVFVTOiByZXR1cm4gc3RhdGU7XHJcblx0XHRjYXNlIGFwcEFjdGlvbnMuQVBQX0RFQlVHOiByZXR1cm4gc3RhdGU7XHJcblx0XHRjYXNlIGFwcEFjdGlvbnMuQVBQX0xPRzogcmV0dXJuIHN0YXRlO1xyXG5cclxuXHRcdGRlZmF1bHQ6IHJldHVybiBzdGF0ZTtcclxuXHR9XHJcbn07XHJcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL2Zyb250ZW5kL3NyYy9nYW1lL3JlZHVjZXJzL2FwcC5qc1xuICoqLyIsImNsYXNzIEFwcENsYXNzIHtcclxuXHRjb25zdHJ1Y3RvciAoc3RhdGUsIGFjdGlvbikge1xyXG5cdFx0dGhpcy5zdGF0ZSA9IHt9O1xyXG5cdFx0dGhpcy5zdGF0ZS5tb2RlcyA9IGFjdGlvbi5kYXRhLm1vZGVzIHx8IFtdO1xyXG5cdFx0dGhpcy5zdGF0ZS5jdXJyZW50TW9kZSA9IGFjdGlvbi5kYXRhLm1vZGVzIHx8IG51bGw7XHJcblx0XHR0aGlzLnN0YXRlLmlkID0gbnVsbDtcclxuXHRcdHRoaXMuc3RhdGUuc3RhdHVzID0gbnVsbDtcclxuXHRcdHRoaXMuc3RhdGUuZGVidWcgPSB0cnVlO1xyXG5cdFx0dGhpcy5zdGF0ZS5sb2cgPSB0cnVlO1xyXG5cdH1cclxuXHRtZXNzYWdlICgpIHtcclxuXHRcdGFsZXJ0KCdAQCcpO1xyXG5cdH1cclxuXHRnZXRTdGF0ZSAoKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5zdGF0ZTtcclxuXHR9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEFwcENsYXNzXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vZnJvbnRlbmQvc3JjL2dhbWUvY2xhc3Nlcy9hcHAuY2xhc3MuanNcbiAqKi8iLCIvLyBJbXBvcnQgYWN0aW9uc1xyXG5pbXBvcnQge3VzZXJBY3Rpb25zfSBmcm9tICcuLy4uL2FjdGlvbnMvdXNlckFjdGlvbnMnO1xyXG4vL2ltcG9ydCB7dXNlckNsYXNzfSBmcm9tICcuLy4uL2NsYXNzZXMvdXNlci5jbGFzcyc7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdXNlciAoc3RhdGUgPSBudWxsLCBhY3Rpb24pIHtcclxuXHRzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcblx0XHRjYXNlIHVzZXJBY3Rpb25zLlVTRVJfTE9HSU46IHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwgYWN0aW9uLmRhdGEpO1xyXG5cdFx0Y2FzZSB1c2VyQWN0aW9ucy5VU0VSX0xPR09VVDogcmV0dXJuIHN0YXRlO1xyXG5cdFx0Y2FzZSB1c2VyQWN0aW9ucy5VU0VSX09OTElORTogcmV0dXJuIHN0YXRlO1xyXG5cdFx0Y2FzZSB1c2VyQWN0aW9ucy5VU0VSX09GRkxJTkU6IHJldHVybiBzdGF0ZTtcclxuXHRcdGNhc2UgdXNlckFjdGlvbnMuVVNFUl9VUERBVEU6IHJldHVybiBzdGF0ZVxyXG5cdFx0ZGVmYXVsdDogcmV0dXJuIHN0YXRlO1xyXG5cdH1cclxufTtcclxuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vZnJvbnRlbmQvc3JjL2dhbWUvcmVkdWNlcnMvdXNlci5qc1xuICoqLyIsIi8vIEltcG9ydCBhY3Rpb25zXHJcbmltcG9ydCB7Z2FtZUFjdGlvbnN9IGZyb20gJy4vLi4vYWN0aW9ucy9nYW1lQWN0aW9ucyc7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2FtZSAoc3RhdGUgPSB7dHVybnM6IFtdLCBhY3RpdmVUdXJuOjB9LCBhY3Rpb24pIHtcclxuXHJcblx0aWYgKGFjdGlvbi50eXBlICE9IGdhbWVBY3Rpb25zLkdBTUVfVElNRVRJQ0spICBjb25zb2xlLmRlYnVnKGFjdGlvbik7XHJcblxyXG5cdHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcclxuXHRcdGNhc2UgZ2FtZUFjdGlvbnMuR0FNRV9TVEFSVDogIHJldHVybiBzdGF0ZVxyXG5cdFx0Y2FzZSBnYW1lQWN0aW9ucy5ST1VORF9TVEFSVDogIHJldHVybiBzdGF0ZVxyXG5cdFx0Y2FzZSBnYW1lQWN0aW9ucy5HQU1FX0xPQUQ6ICByZXR1cm4gc3RhdGVcclxuXHRcdGNhc2UgZ2FtZUFjdGlvbnMuR0FNRV9UVVJOOlxyXG5cdFx0XHRyZXR1cm4ge1xyXG5cdFx0XHRcdHR1cm5zOiBbLi4uc3RhdGUudHVybnMsIGFjdGlvbi5kYXRhXVxyXG5cdFx0XHR9XHJcblx0XHRjYXNlIGdhbWVBY3Rpb25zLkdBTUVfVElNRVRJQ0s6ICByZXR1cm4gc3RhdGVcclxuXHRcdGNhc2UgZ2FtZUFjdGlvbnMuR0FNRV9USU1FT1VUOiAgcmV0dXJuIHN0YXRlXHJcblx0XHRjYXNlIGdhbWVBY3Rpb25zLlNXSVRDSF9QTEFZRVI6ICByZXR1cm4gc3RhdGVcclxuXHRcdGNhc2UgZ2FtZUFjdGlvbnMuUk9VTkRfRU5EOiAgcmV0dXJuIHN0YXRlXHJcblx0XHRjYXNlIGdhbWVBY3Rpb25zLkdBTUVfTEVBVkU6ICByZXR1cm4gc3RhdGVcclxuXHRcdGNhc2UgZ2FtZUFjdGlvbnMuR0FNRV9FVkVOVDogIHJldHVybiBzdGF0ZVxyXG5cdFx0Y2FzZSBnYW1lQWN0aW9ucy5HQU1FX0VSUk9SOiAgcmV0dXJuIHN0YXRlXHJcblx0XHRkZWZhdWx0OiByZXR1cm4gc3RhdGU7XHJcblx0fVxyXG59O1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL2Zyb250ZW5kL3NyYy9nYW1lL3JlZHVjZXJzL2dhbWUuanNcbiAqKi8iLCIvLyBJbXBvcnQgYWN0aW9uc1xyXG5pbXBvcnQge29wdGlvbnNBY3Rpb25zfSBmcm9tICcuLy4uL2FjdGlvbnMvb3B0aW9uc0FjdGlvbnMnO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG9wdGlvbnMgKHN0YXRlID0gbnVsbCAsIGFjdGlvbikge1xyXG5cdFxyXG4vL1x0Y29uc29sZS5kZWJ1ZyhhY3Rpb24pO1xyXG5cclxuXHRzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcblxyXG5cdFx0Y2FzZSBvcHRpb25zQWN0aW9ucy5PUFRJT05TX1NBVkVEOiByZXR1cm4gc3RhdGU7XHJcblx0XHRjYXNlIG9wdGlvbnNBY3Rpb25zLk9QVElPTlNfQ0hFTkdFRDogcmV0dXJuIHN0YXRlO1xyXG5cdFx0Y2FzZSBvcHRpb25zQWN0aW9ucy5PUFRJT05TX1JFU0VUOiByZXR1cm4gc3RhdGVcclxuXHJcblx0XHRkZWZhdWx0OiByZXR1cm4gc3RhdGU7XHJcblx0fVxyXG59OyAgICAgICAgICAgICBcclxuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vZnJvbnRlbmQvc3JjL2dhbWUvcmVkdWNlcnMvb3B0aW9ucy5qc1xuICoqLyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuLi9mcm9udGVuZC9zcmMvaW5kZXguY3NzXG4gKiogbW9kdWxlIGlkID0gMjVcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuLi9mcm9udGVuZC9zcmMvbGF5b3V0LnNjc3NcbiAqKiBtb2R1bGUgaWQgPSAyN1xuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiLy8gSW1wb3J0IEFjdGlvbnNcclxuaW1wb3J0IHthcHBBY3Rpb25zfSBmcm9tICcuLy4uL2dhbWUvYWN0aW9ucy9hcHBBY3Rpb25zLmpzJztcclxuaW1wb3J0IHt1c2VyQWN0aW9uc30gZnJvbSAnLi8uLi9nYW1lL2FjdGlvbnMvdXNlckFjdGlvbnMuanMnO1xyXG5pbXBvcnQge2dhbWVBY3Rpb25zfSBmcm9tICcuLy4uL2dhbWUvYWN0aW9ucy9nYW1lQWN0aW9ucy5qcyc7XHJcbmltcG9ydCB7b3B0aW9uc0FjdGlvbnN9IGZyb20gJy4vLi4vZ2FtZS9hY3Rpb25zL29wdGlvbnNBY3Rpb25zLmpzJztcclxuXHJcbmNvbnNvbGUubG9nKCdhcHAgc3RhcnQnKTtcclxuY29uc29sZS5sb2coQ2xpZW50KTtcclxuLy92YXIgc2V0dGluZ3NUZW1wbGF0ZSA9ICc8ZGl2PjxwPtCm0LLQtdGCPC9wPiA8ZGl2PiA8bGFiZWw+PGlucHV0IHR5cGU9XCJyYWRpb1wiIG5hbWU9XCJjb2xvclwiIHZhbHVlPVwicmVkXCIgPtC60YDQsNGB0L3Ri9C5PC9sYWJlbD4gPGxhYmVsPjxpbnB1dCB0eXBlPVwicmFkaW9cIiBuYW1lPVwiY29sb3JcIiB2YWx1ZT1cImJsYWNrXCIgPtGH0LXRgNC90YvQuTwvbGFiZWw+IDwvZGl2PiA8L2Rpdj4gPHA+0J3QsNGB0YLRgNC+0LnQutC4INC40LPRgNGLPC9wPiA8ZGl2PiA8ZGl2IGNsYXNzPVwib3B0aW9uXCI+IDxsYWJlbD48aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgbmFtZT1cInNvdW5kc1wiPiDQktC60LvRjtGH0LjRgtGMINC30LLRg9C6PC9sYWJlbD4gPC9kaXY+IDxkaXYgY2xhc3M9XCJvcHRpb25cIj4gPGxhYmVsPjxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBuYW1lPVwiZGlzYWJsZUludml0ZVwiPiDQl9Cw0L/RgNC10YLQuNGC0Ywg0L/RgNC40LPQu9Cw0YjQsNGC0Ywg0LzQtdC90Y8g0LIg0LjQs9GA0YM8L2xhYmVsPiA8L2Rpdj48L2Rpdj4nO1xyXG52YXIgc2V0dGluZ3NUZW1wbGF0ZSA9ICc8ZGl2Plx0PHA+0KbQstC10YIg0YTQuNCz0YPRgDwvcD5cdDxkaXY+XHRcdDxkaXYgY2xhc3M9XCJvcHRpb25cIj5cdFx0XHQ8bGFiZWw+PGlucHV0IHR5cGU9XCJyYWRpb1wiIG5hbWU9XCJzaW5nbGVQbGF5ZXJDb2xvclwiIHZhbHVlPVwid2hpdGVcIiA+INCR0LXQu9GL0LU8L2xhYmVsPlx0XHQ8L2Rpdj5cdFx0PGRpdiBjbGFzcz1cIm9wdGlvblwiPlx0XHRcdDxsYWJlbD48aW5wdXQgdHlwZT1cInJhZGlvXCIgbmFtZT1cInNpbmdsZVBsYXllckNvbG9yXCIgdmFsdWU9XCJibGFja1wiID4g0KfQtdGA0L3Ri9C1PC9sYWJlbD5cdFx0PC9kaXY+XHQ8L2Rpdj5cdFx0PHA+0KPRgNC+0LLQtdC90Ywg0YHQu9C+0LbQvdC+0YHRgtC4PC9wPlx0PGRpdj5cdFx0PGRpdiBjbGFzcz1cIm9wdGlvblwiPlx0XHRcdDxsYWJlbD48aW5wdXQgdHlwZT1cInJhZGlvXCIgbmFtZT1cInNpbmdsZVBsYXllckVuZ2luZVNraWxsXCIgdmFsdWU9XCIwXCI+INCf0YDQvtGB0YLQvtC5PC9sYWJlbD5cdFx0PC9kaXY+XHRcdDxkaXYgY2xhc3M9XCJvcHRpb25cIj5cdFx0XHQ8bGFiZWw+PGlucHV0IHR5cGU9XCJyYWRpb1wiIG5hbWU9XCJzaW5nbGVQbGF5ZXJFbmdpbmVTa2lsbFwiIHZhbHVlPVwiMVwiPiDQodGA0LXQtNC90LjQuTwvbGFiZWw+XHRcdDwvZGl2Plx0XHQ8ZGl2IGNsYXNzPVwib3B0aW9uXCI+XHRcdFx0PGxhYmVsPjxpbnB1dCB0eXBlPVwicmFkaW9cIiBuYW1lPVwic2luZ2xlUGxheWVyRW5naW5lU2tpbGxcIiB2YWx1ZT1cIjJcIj4g0KHQu9C+0LbQvdGL0Lk8L2xhYmVsPlx0XHQ8L2Rpdj5cdFx0XHQ8L2Rpdj5cdCA8cD7QndCw0YHRgtGA0L7QudC60Lgg0LjQs9GA0Ys8L3A+XHQ8ZGl2Plx0XHQ8ZGl2IGNsYXNzPVwib3B0aW9uXCI+XHRcdFx0PGxhYmVsPjxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBuYW1lPVwic291bmRzXCI+INCS0LrQu9GO0YfQuNGC0Ywg0LfQstGD0Lo8L2xhYmVsPlx0XHQ8L2Rpdj5cdFx0PGRpdiBjbGFzcz1cIm9wdGlvblwiPlx0XHRcdDxsYWJlbD48aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgbmFtZT1cImRpc2FibGVJbnZpdGVcIj4g0JfQsNC/0YDQtdGC0LjRgtGMINC/0YDQuNCz0LvQsNGI0LDRgtGMINC80LXQvdGPINCyINC40LPRgNGDPC9sYWJlbD5cdFx0PC9kaXY+XHQ8L2Rpdj48L2Rpdj4nO1xyXG4vLyB2YXIgc2V0dGluZ3NUZW1wbGF0ZSA9ICdAQEBAJztcclxuXHJcbi8qIC0tLS0gVE9ETzogcmVtb3ZlIGZvciBwcm9kdWN0aW9uICAqL1xyXG5cclxuICAgIGlmIChuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ0ZpcmVmb3gvJykgPiAwKSB7XHJcbiAgICAgICAgd2luZG93Ll91c2VySWQgPSAnRmlyZWZveCc7XHJcbiAgICAgICAgd2luZG93Ll91c2VybmFtZSA9ICdGaXJlZm94JztcclxuICAgIH1cclxuICAgIGlmIChuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ0Nocm9tZS8nKSA+IDApIHtcclxuICAgICAgICB3aW5kb3cuX3VzZXJJZCA9ICdDaHJvbWUnO1xyXG4gICAgICAgIHdpbmRvdy5fdXNlcm5hbWUgPSAnQ2hyb21lJztcclxuICAgIH1cclxuICAgIGlmIChuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ09QUi8nKSA+IDApIHtcclxuICAgICAgICB3aW5kb3cuX3VzZXJJZCA9ICdPcGVyYSc7XHJcbiAgICAgICAgd2luZG93Ll91c2VybmFtZSA9ICdPcGVyYSc7XHJcbiAgICB9XHJcblxyXG4vKiAtLS0tICovXHJcbnZhciB1c2VyID0ge1xyXG5cdHVzZXJJZDogd2luZG93Ll91c2VySWQsXHJcblx0dXNlck5hbWU6IHdpbmRvdy5fdXNlcm5hbWUsXHJcblx0c2lnbjogd2luZG93Ll91c2VySWQgKyB3aW5kb3cuX3VzZXJuYW1lXHJcbn07XHJcblxyXG53aW5kb3cuX2NsaWVudCA9IG5ldyBDbGllbnQoe1xyXG5cdGdhbWU6ICd2NkNoZXNzR2FtZScsXHJcblx0cG9ydDogODEwMyxcclxuXHRyZXN1bHREaWFsb2dEZWxheTogMTAwMCxcclxuXHRyZWxvYWQ6IHRydWUsXHJcbi8vXHRodHRwczogdHJ1ZSxcclxuLy9cdGRvbWFpbjogJ2xvZ2ljLWdhbWVzLnNwYi5ydScsXHJcbi8vXHRkb21haW46ICd2NmNoZXNzLmxvY2FsJyxcclxuXHRhdXRvU2hvd1Byb2ZpbGU6IHRydWUsXHJcblx0bmV3R2FtZUZvcm1hdDogdHJ1ZSxcclxuXHRnZW5lcmF0ZUludml0ZVRleHQ6IGZ1bmN0aW9uIChpbnZpdGUpIHtcclxuLy9cdFx0Y29uc29sZS5sb2coJ0BAQEAgaW52aXRlJyk7XHJcbi8vXHRcdGNvbnNvbGUubG9nKGludml0ZSk7XHJcblx0XHR2YXIgIGludml0ZVN0cmluZ0NsYXNzaWMgPSAn0JLQsNGBINC/0YDQuNCz0LvQsNGB0LjQuyDQv9C+0LvRjNC30L7QstCw0YLQtdC70YwgJyArIGludml0ZS5mcm9tLnVzZXJOYW1lICsgJyAoJyArIGludml0ZS5mcm9tLmdldFJhbmsoaW52aXRlLmRhdGEubW9kZSkgKyAnINC80LXRgdGC0L4g0LIg0YDQtdC50YLQuNC90LPQtSknXHJcblx0XHRcdCsgJyDQsiAgPHN0cm9uZz7QutC70LDRgdGB0LjRh9C10YHQutC40LUg0YjQsNGF0LzQsNGC0Ys8L3N0cm9uZz4uJztcclxuXHRcdHZhciAgaW52aXRlU3RyaW5nQmxpdHogPSAn0JLQsNGBINC/0YDQuNCz0LvQsNGB0LjQuyDQv9C+0LvRjNC30L7QstCw0YLQtdC70YwgJyArIGludml0ZS5mcm9tLnVzZXJOYW1lICsgJyAoJyArIGludml0ZS5mcm9tLmdldFJhbmsoaW52aXRlLmRhdGEubW9kZSkgKyAnINC80LXRgdGC0L4g0LIg0YDQtdC50YLQuNC90LPQtSknXHJcblx0XHRcdCsgJyDQsiAgPHN0cm9uZz7QsdC70LjRhjwvc3Ryb25nPi4nO1xyXG5cclxuXHRcdGlmIChpbnZpdGUuZGF0YS5tb2RlID09ICdjbGFzc2ljJykge1xyXG5cdFx0XHRyZXR1cm4gaW52aXRlU3RyaW5nQ2xhc3NpYztcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHJldHVybiBpbnZpdGVTdHJpbmdCbGl0ejtcclxuXHRcdH1cclxuLypcclxuXHRcdHJldHVybiAn0JLQsNGBINC/0YDQuNCz0LvQsNGB0LjQuyDQv9C+0LvRjNC30L7QstCw0YLQtdC70YwgJyArIGludml0ZS5mcm9tLnVzZXJOYW1lICsgJygnICsgaW52aXRlLmZyb20uZ2V0UmFuayhpbnZpdGUuZGF0YS5tb2RlKSArICcg0LzQtdGB0YLQviDQsiDRgNC10LnRgtC40L3Qs9C1KSdcclxuXHRcdFx0KyAnINCyICAnK19jbGllbnQuZ2V0TW9kZUFsaWFzKGludml0ZS5kYXRhLm1vZGUpO1xyXG4qL1xyXG5cdH0sXHJcblx0YmxvY2tzOiB7XHJcblx0XHR1c2VyTGlzdElkOiAndXNlckxpc3REaXYnLFxyXG5cdFx0Y2hhdElkOiAnY2hhdERpdicsXHJcblx0XHRyYXRpbmdJZDogJ3JhdGluZ0RpdicsXHJcblx0XHRoaXN0b3J5SWQ6ICdyYXRpbmdEaXYnLFxyXG5cdFx0cHJvZmlsZUlkOiAncmF0aW5nRGl2J1xyXG5cdH0sXHJcblx0c2V0dGluZ3M6IHtcclxuXHRcdHNpbmdsZVBsYXllckNvbG9yOiAnd2hpdGUnLFxyXG5cdFx0c2luZ2xlUGxheWVyRW5naW5lU2tpbGw6IFwiMFwiLFxyXG5cdFx0Y2hlc3NDbG9ja1ZpZXc6IFwiMFwiLFxyXG5cdH0sXHJcblx0c2V0dGluZ3NUZW1wbGF0ZTogc2V0dGluZ3NUZW1wbGF0ZVxyXG59KS5pbml0KHVzZXIpO1xyXG5cclxudmFyIF9jbGllbnQgPSB3aW5kb3cuX2NsaWVudDtcclxuXHJcbl9jbGllbnQub24oJ2xvZ2luJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHRhcHAuZ2FtZS5zdG9yZS5kaXNwYXRjaCh7XHJcblx0XHR0eXBlOiBhcHBBY3Rpb25zLkFQUF9JTklULFxyXG5cdFx0ZGF0YToge1xyXG5cdFx0XHRtb2RlczogX2NsaWVudC5tb2RlcyxcclxuXHRcdFx0Y3VycmVudE1vZGU6IF9jbGllbnQuY3VycmVudE1vZGVcclxuXHRcdH1cclxuXHR9KTtcclxuXHJcblx0Y29uc3QgIGN1cnJlbnRVc2VyID0ge1xyXG5cdFx0aWQ6IGRhdGEudXNlcklkLFxyXG5cdFx0bmFtZTogZGF0YS51c2VyTmFtZSxcclxuXHRcdGlzR3Vlc3Q6IG51bGwsXHJcblx0XHRzdGF0dXM6IG51bGwsXHJcblx0XHR0YWdzOiBbXSxcclxuXHRcdGltZzogbnVsbCxcclxuXHR9O1xyXG5cdGZvciAobGV0IGdhbWVNb2RlIGluICBfY2xpZW50Lm1vZGVzKSB7XHJcblx0XHRjdXJyZW50VXNlcltfY2xpZW50Lm1vZGVzW2dhbWVNb2RlXV0gPSBkYXRhW19jbGllbnQubW9kZXNbZ2FtZU1vZGVdXVxyXG5cdH1cclxuXHJcblx0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG5cdFx0dHlwZTogdXNlckFjdGlvbnMuVVNFUl9MT0dJTixcclxuXHRcdGRhdGE6IGN1cnJlbnRVc2VyXHJcblx0fSk7XHJcblxyXG59KTtcclxuXHJcbl9jbGllbnQub24oJ21vZGVfc3dpdGNoJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHRhcHAuZ2FtZS5zdG9yZS5kaXNwYXRjaCh7XHJcblx0XHR0eXBlOiBhcHBBY3Rpb25zLk1PREVfU1dJVENILFxyXG5cdFx0ZGF0YTogZGF0YVxyXG5cdH0pO1xyXG59KTtcclxuXHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ2dhbWVfc3RhcnQnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLkdBTUVfU1RBUlQsXHJcblx0XHRkYXRhOiBkYXRhXHJcblx0fSk7XHJcbn0pO1xyXG5cclxuX2NsaWVudC5nYW1lTWFuYWdlci5vbigncm91bmRfc3RhcnQnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLlJPVU5EX1NUQVJULFxyXG5cdFx0ZGF0YTogZGF0YVxyXG5cdH0pO1xyXG59KTtcclxuXHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ2dhbWVfbG9hZCcsIGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG5cdFx0dHlwZTogZ2FtZUFjdGlvbnMuR0FNRV9MT0FELFxyXG5cdFx0ZGF0YTogZGF0YVxyXG5cdH0pO1xyXG59KTtcclxuXHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ3R1cm4nLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLkdBTUVfVFVSTixcclxuXHRcdGRhdGE6IHtcclxuXHRcdFx0ZnJvbTogZGF0YS50dXJuLm5ld01vdmUuZnJvbSxcclxuXHRcdFx0dG86IGRhdGEudHVybi5uZXdNb3ZlLnRvLFxyXG5cdFx0XHRjb3JyZWN0OiAgZGF0YS50dXJuLmlzQ29yZWN0XHJcblx0XHR9XHJcblx0fSk7XHJcbn0pO1xyXG5cclxuX2NsaWVudC5nYW1lTWFuYWdlci5vbignZXJyb3InLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLkdBTUVfRVJST1IsXHJcblx0XHRkYXRhOiBkYXRhXHJcblx0fSk7XHJcbn0pO1xyXG5cclxuX2NsaWVudC5nYW1lTWFuYWdlci5vbignc3dpdGNoX3BsYXllcicsIGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG5cdFx0dHlwZTogZ2FtZUFjdGlvbnMuU1dJVENIX1BMQVlFUixcclxuXHRcdGRhdGE6IGRhdGFcclxuXHR9KTtcclxufSk7XHJcblxyXG5fY2xpZW50LmdhbWVNYW5hZ2VyLm9uKCdldmVudCcsIGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG5cdFx0dHlwZTogZ2FtZUFjdGlvbnMuR0FNRV9FVkVOVCxcclxuXHRcdGRhdGE6IGRhdGFcclxuXHR9KTtcclxufSk7XHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ3RpbWVvdXQnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLkdBTUVfVElNRU9VVCxcclxuXHRcdGRhdGE6IGRhdGFcclxuXHR9KTtcclxufSk7XHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ3JvdW5kX2VuZCcsIGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG5cdFx0dHlwZTogZ2FtZUFjdGlvbnMuUk9VTkRfRU5ELFxyXG5cdFx0ZGF0YTogZGF0YVxyXG5cdH0pO1xyXG59KTtcclxuXHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ2dhbWVfbGVhdmUnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLkdBTUVfTEVBVkUsXHJcblx0XHRkYXRhOiBkYXRhXHJcblx0fSk7XHJcbn0pO1xyXG5cclxuX2NsaWVudC5nYW1lTWFuYWdlci5vbigndGFrZV9iYWNrJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHRhcHAuZ2FtZS5zdG9yZS5kaXNwYXRjaCh7XHJcblx0XHR0eXBlOiBnYW1lQWN0aW9ucy5HQU1FX1RBS0VCQUNLLFxyXG5cdFx0ZGF0YTogZGF0YVxyXG5cdH0pO1xyXG59KTtcclxuXHJcbl9jbGllbnQuZ2FtZU1hbmFnZXIub24oJ3RpbWUnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IGdhbWVBY3Rpb25zLkdBTUVfVElNRVRJQ0ssXHJcblx0XHRkYXRhOiBkYXRhXHJcblx0fSk7XHJcbn0pO1xyXG5cclxuX2NsaWVudC5nYW1lTWFuYWdlci5vbignZm9jdXMnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG4vL1x0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG4vL1x0XHR0eXBlOiB1c2VyQWN0aW9ucy5VU0VSX0ZPQ1VTLFxyXG4vL1x0XHRkYXRhOiBkYXRhXHJcbi8vXHR9KTtcclxufSk7XHJcblxyXG5fY2xpZW50Lm9uKCdzaG93X3Byb2ZpbGUnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG4vL1x0Y29uc29sZS5sb2coJ21haW47JywgJ3Nob3dfcHJvZmlsZSB1c2VyOicsIGRhdGEpO1xyXG59KTtcclxuXHJcbl9jbGllbnQub24oJ3NldHRpbmdzX2NoYW5nZWQnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdGFwcC5nYW1lLnN0b3JlLmRpc3BhdGNoKHtcclxuXHRcdHR5cGU6IG9wdGlvbnNBY3Rpb25zLk9QVElPTlNfQ0hFTkdFRCxcclxuXHRcdGRhdGE6IGRhdGFcclxuXHR9KTtcclxufSk7XHJcblxyXG5fY2xpZW50Lm9uKCdzZXR0aW5nc19zYXZlZCcsIGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0YXBwLmdhbWUuc3RvcmUuZGlzcGF0Y2goe1xyXG5cdFx0dHlwZTogb3B0aW9uc0FjdGlvbnMuT1BUSU9OU19TQVZFRCxcclxuXHRcdGRhdGE6IGRhdGFcclxuXHR9KTtcclxufSk7XHJcbi8vIHNlbmQgZXZlbnRzIGJ1dHRvbnMgZXhhbXBsZVxyXG5cclxuX2NsaWVudC5oaXN0b3J5TWFuYWdlci5vbignZ2FtZV9sb2FkJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHRhcHAuZ2FtZS5zdG9yZS5kaXNwYXRjaCh7XHJcblx0XHR0eXBlOiBnYW1lQWN0aW9ucy5HQU1FX0hJU1RPUllfTE9BRCxcclxuXHRcdGRhdGE6IGRhdGFcclxuXHR9KTtcclxufSk7XHJcblxyXG5cclxuX2dlbmVyYXRlRW5kR2FtZUJ0bigpO1xyXG5mdW5jdGlvbiBfZ2VuZXJhdGVFbmRHYW1lQnRuKCkge1xyXG5cdHZhciBiZGl2ID0gJCgnPGRpdj4nKTtcclxuXHRiZGl2LmFkZENsYXNzKCd2Ni1idXR0b25zJyk7XHJcblx0JCgnYm9keScpLmFwcGVuZChiZGl2KTtcclxuXHR2YXIgZGl2ID0gJCgnPGRpdj4nKTtcclxuXHRkaXYuYXR0cignaWQnLCAnZW5kR2FtZUJ1dHRvbicpO1xyXG5cdGRpdi5odG1sKCc8c3Bhbj7QktGL0LnRgtC4INC40Lcg0LjQs9GA0Ys8L3NwYW4+Jyk7XHJcblx0ZGl2Lm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHdpbmRvdy5fY2xpZW50LmdhbWVNYW5hZ2VyLmxlYXZlR2FtZSgpO1xyXG5cdH0pO1xyXG5cdGJkaXYuYXBwZW5kKGRpdik7XHJcblx0ZGl2ID0gJCgnPGRpdj4nKTtcclxuXHRkaXYuYXR0cignaWQnLCAnZHJhd0J1dHRvbicpO1xyXG5cdGRpdi5odG1sKCc8c3Bhbj7Qn9GA0LXQtNC70L7QttC40YLRjCDQvdC40YfRjNGOPC9zcGFuPicpO1xyXG5cdGRpdi5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHR3aW5kb3cuX2NsaWVudC5nYW1lTWFuYWdlci5zZW5kRHJhdygpO1xyXG5cdH0pO1xyXG5cdGJkaXYuYXBwZW5kKGRpdik7XHJcblx0ZGl2ID0gJCgnPGRpdj4nKTtcclxuXHRkaXYuYXR0cignaWQnLCAnd2luQnV0dG9uJyk7XHJcblx0ZGl2Lmh0bWwoJzxzcGFuPtCf0L7QsdC10LTQvdGL0Lkg0YXQvtC0PC9zcGFuPicpO1xyXG5cdGRpdi5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHR3aW5kb3cuX2NsaWVudC5nYW1lTWFuYWdlci5zZW5kVHVybih7cmVzdWx0OiAxfSk7XHJcblx0fSk7XHJcblx0YmRpdi5hcHBlbmQoZGl2KTtcclxuXHRkaXYgPSAkKCc8ZGl2PicpO1xyXG5cdGRpdi5hdHRyKCdpZCcsICdyYXRpbmdCdXR0b24nKTtcclxuXHRkaXYuaHRtbCgnPHNwYW4+0J/QvtC60LDQt9Cw0YLRjCDRgNC10LnRgtC40L3Qszwvc3Bhbj4nKTtcclxuXHRkaXYub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0d2luZG93Ll9jbGllbnQucmF0aW5nTWFuYWdlci5nZXRSYXRpbmdzKCk7XHJcblx0fSk7XHJcblx0YmRpdi5hcHBlbmQoZGl2KTtcclxuXHRkaXYgPSAkKCc8ZGl2PicpO1xyXG5cdGRpdi5hdHRyKCdpZCcsICdoaXN0b3J5QnV0dG9uJyk7XHJcblx0ZGl2Lmh0bWwoJzxzcGFuPtCf0L7QutCw0LfQsNGC0Ywg0LjRgdGC0L7RgNC40Y48L3NwYW4+Jyk7XHJcblx0ZGl2Lm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdHdpbmRvdy5fY2xpZW50Lmhpc3RvcnlNYW5hZ2VyLmdldEhpc3RvcnkoZmFsc2UsIGZhbHNlLCBmYWxzZSk7XHJcblx0fSk7XHJcblx0YmRpdi5hcHBlbmQoZGl2KTtcclxuXHRkaXYgPSAkKCc8ZGl2PicpO1xyXG5cdGRpdi5odG1sKCc8c3Bhbj7Qn9C10YDQtdC00LDRgtGMINGF0L7QtDwvc3Bhbj4nKTtcclxuXHRkaXYub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0d2luZG93Ll9jbGllbnQuZ2FtZU1hbmFnZXIuc2VuZFR1cm4oeydzd2l0Y2gnOiB0cnVlfSk7XHJcblx0fSk7XHJcblx0YmRpdi5hcHBlbmQoZGl2KTtcclxuXHRkaXYgPSAkKCc8ZGl2PicpO1xyXG5cdGRpdi5odG1sKCc8c3Bhbj7QodC00LXQu9Cw0YLRjCDRhdC+0LQ8L3NwYW4+Jyk7XHJcblx0ZGl2Lm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0dmFyIG5ld01vdmUgPSB7XHJcblx0XHRcdFx0J25ld01vdmUnOiB7XHJcblx0XHRcdCAgICBcdFx0ZnJvbTogJ2UyJyxcclxuXHRcdFx0ICAgIFx0XHR0bzogJ2U0JyxcclxuXHRcdFx0ICAgIFx0XHRwcm9tb3Rpb246ICdxJyxcclxuXHRcdFx0ICAgIFx0fSxcclxuXHRcdFx0ICAgIFx0ZmVuOiAnRkVOJ1xyXG5cdFx0XHR9XHJcblx0XHR3aW5kb3cuX2NsaWVudC5nYW1lTWFuYWdlci5zZW5kVHVybihuZXdNb3ZlKTtcclxuXHR9KTtcclxuXHRiZGl2LmFwcGVuZChkaXYpO1xyXG5cdGRpdiA9ICQoJzxkaXY+Jyk7XHJcblx0ZGl2Lmh0bWwoJzxzcGFuPtGF0L7QtCDQvdCw0LfQsNC0PC9zcGFuPicpO1xyXG5cdGRpdi5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHR3aW5kb3cuX2NsaWVudC5nYW1lTWFuYWdlci5zZW5kVGFrZUJhY2soKTtcclxuXHR9KTtcclxuXHRiZGl2LmFwcGVuZChkaXYpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRDb29raWUoY19uYW1lKSB7XHJcblx0aWYgKGRvY3VtZW50LmNvb2tpZS5sZW5ndGggPiAwKSB7XHJcblx0XHR2YXIgY19zdGFydCA9IGRvY3VtZW50LmNvb2tpZS5pbmRleE9mKGNfbmFtZSArIFwiPVwiKTtcclxuXHRcdGlmIChjX3N0YXJ0ICE9IC0xKSB7XHJcblx0XHRcdGNfc3RhcnQgPSBjX3N0YXJ0ICsgY19uYW1lLmxlbmd0aCArIDE7XHJcblx0XHRcdHZhciBjX2VuZCA9IGRvY3VtZW50LmNvb2tpZS5pbmRleE9mKFwiO1wiLCBjX3N0YXJ0KTtcclxuXHRcdFx0aWYgKGNfZW5kID09IC0xKSBjX2VuZCA9IGRvY3VtZW50LmNvb2tpZS5sZW5ndGg7XHJcblx0XHRcdHJldHVybiB1bmVzY2FwZShkb2N1bWVudC5jb29raWUuc3Vic3RyaW5nKGNfc3RhcnQsIGNfZW5kKSk7XHJcblx0XHR9XHJcblx0fVxyXG5cdHJldHVybiBcIlwiO1xyXG59XHJcblxyXG4vL1x0aWYgKHdpbmRvdy5fdXNlcm5hbWUgPT0gJ0Nocm9tZScpXHJcbi8vXHRcdHJlcXVpcmUoJy4vLi4vYm90L2JvdC5qcycpO1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL2Zyb250ZW5kL3NyYy9jbGllbnQvaW5pdC5qc1xuICoqLyJdLCJzb3VyY2VSb290IjoiIn0=