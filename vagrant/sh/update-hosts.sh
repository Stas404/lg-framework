echo 'Update hosts.'
vmip=192.168.33.10

for vhFile in /var/tmp/vagrant/hosts/*.conf
do
    sudo cp /var/tmp/vagrant/hosts/*.conf /etc/apache2/sites-available/ -R
    vhConf=${vhFile##*/}
    sudo a2ensite ${vhConf}
    vhost=${vhConf%.*}
    sudo sed -i "2i${vmip}    ${vhost}" /etc/hosts
done
#sudo chmod -R 755 /var/www
sudo service apache2 restart
echo '--------'