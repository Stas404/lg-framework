var webpack = require('webpack');
var path = require("path");
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	context: path.resolve("./src"),
	entry: {
		app: path.resolve('./frontend/src/index.js'),
	},
	output: {
		path: path.resolve("./frontend/www/build"),
//		publicPath: '../',
		filename: "[name].js",
		chunkFilename: '[name].js',
	},
//	devtool: "#inline-source-map",
	module: {
    	loaders: [
			{
				test: /\.js$/, loader: "babel-loader",
				query: {
					presets: ['es2015']
				}
			},
//    			{
//				test: /\.js$/,
//				loaders: ['babel?presets[]=es2015'],
//				exclude: /node_modules/,
//				include: __dirname
  //  			},
//			{
//				test: /\.js$/,
//				exclude: /node_modules/,
//				loaders: ['babel-loader'],
//			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract("style-loader", "css-loader")
			},
			{
				test: /\.(eot|woff|ttf|svg|png|jpg)$/,
				loader: 'url-loader?limit=10&name=[path][name].[ext]'
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract(
					'css?sourceMap!' +
					'sass?sourceMap'
				)
			}
		]
	},
	plugins: [
		new ExtractTextPlugin("[name].css", {}),
		new webpack.NoErrorsPlugin()
	],
};