var env = 'devel';
var path = require("path");
var webpack = require('webpack');
var config = require(path.resolve('./npm_scripts/webpack.config'));
console.log('Webpack ENV: ', env, '\n-------------------');

config.devtool = "inline-source-map";

var compiler = webpack(config);

compiler.watch({
	poll: true
}, function(err, stats) {
	console.log(stats.toString({
		assets: true,
		chunks: false,
		colors: true,
		hash: false,
		modules: false,
		reasons: false,
		source: false,
	}));
});