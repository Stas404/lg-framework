class EventManager {
	constructor (options) {
		_.extend(this, Backbone.Events);
		this.on('debug', function (args) {
			console.out('@EVENT MANAGER DEBUG: ');
			console.log(args);
		}, this);
	}
}

export default EventManager;